<?php
class Album_table implements DatabaseTables {
	private static $tableName  = "album";
	private static $fields = array(
								"id" => "id",
								"user_id" => "user_id",
								"is_public" => "is_public",
								"en_name" => "en_name",
								"ru_name" => "ru_name",
								"am_name" => "am_name",
								"creation_date" => "creation_date"
							 );
	
	
	public static function getTableName(){
		return self::$tableName;
	}
	
	public static function getTableFields(){
		return self::$fields;
	}
	
	private static function getTableKeyField(){
		return self::$fields[ "id" ];
	}
	
	public static function getAlbumList( $userId, $lang, $onlyPublic = true ){
		$db = new DBconnection();
		
		$whereData = array();
		$whereData[ "user_id" ] = $userId;
		if( $onlyPublic ){
			$whereData[ "is_public" ] = 1;
		}
		
		$selectFields = self::getTableFields();
		$langList = Config::getLanguages();
		for( $i = 0; $i < count( $langList ); $i++ ){
			unset( $selectFields[ ($langList[ $i ] . "_name") ] );
		}
		$getLangCol = self::getTranslatableCol('name', $lang);
        $query = "SELECT " . $db->getSelectPart( $selectFields ) . ", " . $getLangCol . " FROM " . self::getTableName() . " WHERE " . $db->getWherePart( self::getTableFields(), $whereData );
		$result = $db->selectQuery( $query, null );
	
		return $result;
	}
    
	public static function getTranslatableCol($col, $lang) {
      switch ($lang) {
        case 'en';
          $langList = array('en', 'ru', 'am');
          break;
        case 'ru';
          $langList = array('ru', 'en', 'am');
          break;
        case 'am';
          $langList = array('am', 'en', 'ru');
          break;
      }
      foreach ($langList as &$lang) {
        $lang = 'NULLIF(' . $lang . '_' . $col . ', "")';
      }
      return 'COALESCE(' . implode(',', $langList) . ') as ' . $col;
    }

  public static function getAlbum_allLang( $id ){
		$db = new DBconnection();
		
		$query = "SELECT * FROM " . self::getTableName() . " WHERE " . $db->getWherePart( self::getTableKeyField(), $id );
		$result = $db->selectQuery( $query, null );
	
		return $result[ 0 ];
	}
	
	public static function editAlbum( $id, $data ){
		$db = new DBconnection();
	
		$query = "UPDATE " . self::getTableName() . " SET " . $db->getUpdatePart(self::getTableFields(), $data) . " WHERE " . $db->getWherePart( self::getTableKeyField(), $id );
		$db->updateQuery( $query );
	}
	
	public static function removeAlbum( $id ){
		$db = new DBconnection();
	
		$query = "DELETE FROM " . self::getTableName() . " WHERE " . $db->getWherePart( self::getTableKeyField(), $id );
		$db->deleteQuery( $query );
		
		AlbumPicture_table::removePicturesByAlbumId( $id );
	}
	
	public static function createAlbum( $userId, $data ){
		$db = new DBconnection();
		
		$data[ "user_id" ] = $userId;
		$data[ "creation_date" ] = time();
		
		$query = "INSERT INTO " . self::getTableName() . " " . $db->getInsertPart( self::getTableFields(), $data );
		return $db->insertQuery( $query );
	}
}