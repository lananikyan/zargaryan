<?php
class ContactUs_table implements DatabaseTables {
	private static $tableName  = "contact_us";
	private static $fields = array(
								"id" => "id",
								"name" => "name",
								"email" => "email",
								"phone" => "phone",
								"header" => "header",
								"message" => "message",
								"creation_date" => "creation_date",
								"file" => "file",
								"is_new" => "is_new"
							 );
	
	
	
	public static function getTableName(){
		return self::$tableName;
	}
	
	public static function getTableFields(){
		return self::$fields;
	}
	
	private static function getTableKeyField(){
		return null;
	}
	
	public static function setMessage( $data ){
		$db = new DBconnection();
		$data[ "creation_date" ] = time();
		
		$query = "INSERT INTO " . self::getTableName() . " " . $db->getInsertPart( self::getTableFields(), $data );
		return $db->insertQuery( $query );
	}
	
	public static function setOld( $ids ){
		$db = new DBconnection();
		$query = "UPDATE " . self::getTableName() . " SET " . $db->getUpdatePart(self::getTableFields(), array( "is_new" => 0 )) . " WHERE " . self::$fields[ "id" ] . " IN (" . $db->getCommaSepListPart( $ids ) . ")";
		$db->updateQuery( $query );
	}
	
	public static function getMessages( $isNew = true ){
		$db = new DBconnection();		
		$query = "SELECT * FROM " . self::getTableName() . (is_null( $isNew ) ? "" : (" WHERE " . $db->getWherePart( self::getTableFields(), ($isNew ? array( "is_new" => "1" ) : array( "is_new" => "0" )) )));
		
		$result = $db->selectQuery( $query, self::getTableFields() );
		return $result;
	}
}