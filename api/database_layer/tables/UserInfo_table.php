<?php

require_once Config::getBaseDir() . 'database_layer/tables/CountryList_table.php';

class UserInfo_table implements DatabaseTables {

  private static $tableName = "user_info";
  private static $fields = array(
      "user_id" => "user_id",
      "first_name" => "first_name",
      "last_name" => "last_name",
      "birth_place" => "birth_place",
      "city" => "city",
      "profession" => "profession",
      "education" => "education",
      "biography" => "biography"
  );

  public static function getTableName($lang = "en") {
    return self::$tableName . "_" . $lang;
  }

  public static function getTableFields() {
    return self::$fields;
  }

  private static function getTableKeyField() {
    return self::$fields["user_id"];
  }

  public static function getUserData($userId, $langList = array("en")) {
    $db = new DBconnection();

    $countryF = CountryList_table::getTableFields();
    $userF = User_table::getTableFields();
    $selectUserF = $userF;
    //unset( $selectUserF[ "id" ] );
    unset($selectUserF["login"]);
    unset($selectUserF["pass"]);
    unset($selectUserF["is_active"]);

    if (count($langList) == 1) {
      $query = "
SELECT " . $db->getSelectPart(self::getTableFields(), "ui") . ", c." . $countryF["caption"] . " AS countryName, " . $db->getSelectPart($selectUserF, "u") . "
FROM 
		" . self::getTableName($langList[0]) . " AS ui
	INNER JOIN
		" . User_table::getTableName() . " AS u ON (u." . $userF["id"] . "= ui." . self::$fields["user_id"] . ")
	INNER JOIN
		" . CountryList_table::getTableName($langList[0]) . " AS c
	ON ui." . self::$fields["user_id"] . " = u." . $userF["id"] . " AND u." . $userF["country"] . " = c." . $countryF["id"] . "
WHERE " . $db->getWherePart(self::getTableFields(), array("user_id" => $userId), false, true, "ui");
      //$join = " " . User_table::getTableName() . " AS u  ON (u.".$userF[ "id" ].'= ui.'.self::$fields[ "user_id" ].')';
      $result = $db->selectQuery($query, null);
      self::extendUserInfo($userId, $result[0], $langList[0]);
    } else {
      $selectUserInfoF = self::$fields;
      unset($selectUserInfoF["user_id"]);

      $select = " " . $db->getSelectPart($selectUserF, "u") . " ";
      $join = " " . User_table::getTableName() . " AS u ";
      $on = " ON ";
      for ($i = 0; $i < count($langList); $i++) {
        $select .= ", " . $db->getSelectPart($selectUserInfoF, ("tUi_" . $langList[$i]), ($langList[$i] . "_")) . ", tC_" . $langList[$i] . "." . $countryF["caption"] . " AS " . $langList[$i] . "_countryName ";

        $join .= " LEFT JOIN " . self::getTableName($langList[$i]) . " AS tUi_" . $langList[$i] . " ON u." . $userF["id"] . " = tUi_" . $langList[$i] . "." . self::$fields["user_id"] . " LEFT OUTER JOIN " . CountryList_table::getTableName($langList[$i]) . " AS tC_" . $langList[$i] . " ON tC_" . $langList[$i] . "." . $countryF["id"] . " = u." . $userF["country"];

        //$on .= " u." .  $userF[ "id" ] . " = tUi_" . $langList[ $i ] . "." . self::$fields[ "user_id" ] . " AND tC_" . $langList[ $i ] . "." . $countryF[ "id" ] . " = u." . $userF[ "country" ] . " AND ";
      }
      $on = substr($on, 0, -4);
      $where = array();
      foreach($langList as $lang) {
        $where[] = "tUi_" . $lang . "." . "`user_id` = " . $userId;
      }
      $query = "SELECT $select FROM " . $join . " " . $on . " WHERE " . implode(' || ', $where);

      $result = $db->selectQuery($query, null);
    }
    
    return $result[0];
  }

  public static function extendUserInfo($user_id, &$result, $lang) {
    $fields = array(
      'first_name',
      'last_name',
      'city_name',
      'birth_place',
      'profession',
      'education',
      'biography',
    );
    foreach ($result as $key => &$value) {
      if(in_array($key, $fields) && empty($value)) {
        $value = self::getTranslatableCol($key, $user_id, $lang);
      }
    }
  }

  public static function getTranslatableCol($col, $user_id, $lang) {
    $db = new DBconnection();
    switch ($lang) {
      case 'en';
        $langList = array('en', 'ru', 'am');
        break;
      case 'ru';
        $langList = array('ru', 'en', 'am');
        break;
      case 'am';
        $langList = array('am', 'en', 'ru');
        break;
    }
    foreach ($langList as &$lang) {
      $query = "SELECT $col FROM user_info_$lang WHERE user_id = $user_id";
      $result = $db->selectQuery($query, null);
      
      if(empty($result[0][$col])) {
        continue;
      }
      else {
        return $result[0][$col];
      }
    }
  }
  public static function getRandomUserData($lang) {
    $db = new DBconnection();

    $countryT = CountryList_table::getTableName($lang);
    $countryF = CountryList_table::getTableFields();

    $fUserT = User_table::getTableName();
    $fUserF = User_table::getTableFields();


    $query = "
SELECT " . $db->getSelectPart(self::getTableFields(), "ui") . ", cl." . $countryF["caption"] . " AS countryName, " . $db->getSelectPart($fUserF, "fu") . "
FROM
		" . self::getTableName($lang) . " AS ui
	INNER JOIN
		" . $countryT . " AS cl
	INNER JOIN
		" . $fUserT . " AS fu
	ON fu." . $fUserF["country"] . " = cl." . $countryF["id"] . " AND ui." . self::$fields["user_id"] . " = fu." . $fUserF["id"] . "
WHERE " . $db->getWherePart($fUserF, array("approve_status" => User_table::$STATUS__APPROVE, "is_active" => "1"), false, true, "fu") . "
ORDER BY RAND() 
LIMIT 1";

    $result = $db->selectQuery($query, null);

    unset($result[0][$fUserF["pass"]]);

    return $result[0];
  }

  public static function updateUserData($id, $lang_data) {
    $db = new DBconnection();
    foreach ($lang_data AS $lang => $data) {
      $query = "UPDATE " . self::getTableName($lang) . " SET " . $db->getUpdatePart(self::getTableFields(), $data) . " WHERE " . $db->getWherePart(self::getTableKeyField(), $id);
      $db->updateQuery($query);
    }
  }

  public static function createUserData($userId, $lang_data) {
    $db = new DBconnection();

    foreach ($lang_data AS $lang => $data) {
      $data["user_id"] = $userId;

      $query = "INSERT INTO " . self::getTableName($lang) . " " . $db->getInsertPart(self::getTableFields(), $data);
      $db->insertQuery($query);
    }
  }

  public static function getUsersList($lang, $isActive = null, $whereStatuses = null) {
    $db = new DBconnection();
    $countryT = CountryList_table::getTableName($lang);
    $countryF = CountryList_table::getTableFields();

    $uField = User_table::getTableFields();
    $uTable = User_table::getTableName();
    $selectUserF = $uField;
    unset($selectUserF["id"]);
    unset($selectUserF["login"]);
    unset($selectUserF["pass"]);
    unset($selectUserF["role"]);
    unset($selectUserF["is_active"]);


    $query = "
SELECT " . $db->getSelectPart(self::getTableFields(), "ui") . ", cl." . $countryF["caption"] . " AS countryName, " . $db->getSelectPart($selectUserF, "u") . "
, pic.*, v.*

FROM 
		" . self::getTableName($lang) . " AS ui
	INNER JOIN
		" . $countryT . " AS cl
	INNER JOIN `" .
            User_table::getTableName() . "` AS u
	ON u." . $uField["country"] . " = cl." . $countryF["id"] . " AND u." . $uField["id"] . " = ui." . self::$fields["user_id"] . "
	
	LEFT JOIN 
	(SELECT al." . Album_table::getTableFields()["user_id"] . " AS al_user_id, sum(pic." . AlbumPicture_table::getTableFields()["file_size"] . ") AS UsedFileSize FROM " .
            Album_table::getTableName() . " AS al LEFT JOIN " . AlbumPicture_table::getTableName() . " AS pic ON 
			pic." . AlbumPicture_table::getTableFields()["album_id"] . "= al." . Album_table::getTableFields()["id"] . " 
			GROUP BY al." . Album_table::getTableFields()["user_id"] . ") as pic
	ON pic.al_user_id = u." . $uField["id"] . "		
	LEFT JOIN	(SELECT " . Videos_table::getTableFields()["user_id"] . " AS video_user_id, sum(" . Videos_table::getTableFields()["file_size"] . ") AS UsedVideoZise FROM " .
            Videos_table::getTableName() . " GROUP BY " . Videos_table::getTableFields()["user_id"] . " )	AS v 
	ON	v.video_user_id = 	u." . $uField["id"] . "
	

WHERE 1 " .
            (is_null($whereStatuses) ? "" : (" AND (" . $db->getWherePart($uField, array("approve_status" => $whereStatuses), true, true, "u") . ") ")) .
            //(is_null( $whereStatuses ) ? "" : (" AND (" . $db->getWherePart($uField, $whereStatuses, false, false, "u") . ") ")) .
            //(is_null( $isActive ) ? "" : (" AND " . $db->getWherePart($uField, array( "is_active" => ($isActive ? "1" : "0") ), false, true, "u")));
            (is_null($isActive) ? "" : (" AND " . $db->getWherePart($uField, $isActive, false, false, "u")))
            . "GROUP BY u." . $uField["id"];

    $result = $db->selectQuery($query, null);
    //echo $query;
    return $result;
  }

  public static function getTreeUsersList($lang) {
    $db = new DBconnection();

    $uTable = User_table::getTableName();
    $uField = User_table::getTableFields();
    $selectUserF = $uField;
    unset($selectUserF["login"]);
    unset($selectUserF["pass"]);
    unset($selectUserF["sex"]);
    unset($selectUserF["role"]);
    unset($selectUserF["create_date"]);
    unset($selectUserF["last_login_date"]);
    unset($selectUserF["approve_status"]);
    unset($selectUserF["is_active"]);
    unset($selectUserF["country"]);
    unset($selectUserF["email"]);
    unset($selectUserF["skype"]);
    unset($selectUserF["phone"]);
    unset($selectUserF["viber"]);
    unset($selectUserF["whatsapp"]);

    $selectUserInfoF = self::getTableFields();
    unset($selectUserInfoF["user_id"]);
    unset($selectUserInfoF["birth_place"]);
    unset($selectUserInfoF["city"]);
    unset($selectUserInfoF["profession"]);
    unset($selectUserInfoF["education"]);
    unset($selectUserInfoF["biography"]);

    $query = "
          SELECT " . $db->getSelectPart($selectUserInfoF, "ui") . ", " . $db->getSelectPart($selectUserF, "u") . "
          FROM
          " . self::getTableName($lang) . " AS ui
          INNER JOIN `" .
            User_table::getTableName() . "` AS u
          ON u." . $uField["id"] . " = ui." . self::$fields["user_id"] . "
          WHERE (" . $db->getWherePart($uField, array("approve_status" => User_table::$STATUS__APPROVE, "is_active" => "1"), true, true, "u")
            . ") AND " . $db->getWherePart($uField, array("role" => UserManager::$ROLE_VIEWER), false, false, "u")
    ;

    $result = $db->selectQuery($query, null);

    return $result;
  }

  public static function getUserSearchList($lang, $search = null, $member = null) {
    $db = new DBconnection();
    $uField = User_table::getTableFields();
    $uiFileds = UserInfo_table::getTableFields();
    $uiTable = UserInfo_table::getTableName($lang);

    $modi = html_entity_decode($search);
    $query = "SELECT a.user_id, a.first_name, a.birth_place, a.city, b.profile_picture, b.birth_date FROM user_info_" . $lang . " as a join user as b on a.user_id = b.id " . " Where a.first_name COLLATE utf8_general_ci LIKE '%" . $modi . "%'" .
            (is_null($member) ? "" : (" AND " . $db->getWherePart($uField, array("role" => UserManager::$ROLE_VIEWER), false, false, "b")));

    $result = $db->selectQuery($query, null);

    return $result;
  }

  public static function getVisitCount() {
    $db = new DBconnection();

    $query = "select params.Value from params where ID = 1";

    $result = $db->selectQuery($query, null);

    return $result[0]["Value"];
  }

  public static function incrementVisitCount() {
    $db = new DBconnection();

    $query = "Update params set Value = Value+1 where ID = 1";

    $result = $db->updateQuery($query);
    $query = "select params.Value from params where ID = 1";

    $result = $db->selectQuery($query, null);

    return $result[0]["Value"];
  }

  public static function getUserByEmail($lang, $search = null) {
    $db = new DBconnection();
    $uField = User_table::getTableFields();

    $modi = html_entity_decode($search);

    $query = "SELECT us.user_id, us.first_name, us.birth_place, us.city, u.profile_picture, u.phone, u.login, u.birth_date, u.role 
              FROM user_info_" . $lang . " as us 
              LEFT JOIN user as u on us.user_id = u.id 
              WHERE u.email = '" . $modi . "'".
      (" AND " . $db->getWherePart($uField, array("role" => UserManager::$ROLE_VIEWER), false, false, "u"));

    //echo $query;

    $result = $db->selectQuery($query, null);
    //var_dump($result);
    return $result;
  }

  public static function getUserByUnique($lang, $search = null) {
    $db = new DBconnection();

    $modi = html_entity_decode($search);
    $query = "SELECT a.user_id, a.first_name, a.birth_place, a.city, b.profile_picture, b.birth_date FROM user_info_" . $lang . " as a join user as b on a.user_id = b.id " . " Where b.unique_token  = '" . $modi . "'";
    $result = $db->selectQuery($query, null);

    return $result;
  }

  public static function getUsersByBirthday($lang, $search = null) {
    $db = new DBconnection();
    $tz = date_default_timezone_get();
    $dateTimeZone = new DateTimeZone($tz);
    $dateTimeZ = new DateTime("now", $dateTimeZone);
    $timeOffset = $dateTimeZone->getOffset($dateTimeZ) / 3600;
    if ($timeOffset > 0) {
      $timeOffset = '+' . $timeOffset;
    }

    $query = "SELECT a.user_id, a.first_name, a.last_name, b.birth_date FROM user_info_" . $lang . " AS a JOIN user AS b ON a.user_id = b.id " . " Where (DATE_FORMAT(FROM_UNIXTIME(b.birth_date),'%m-%d') = DATE_FORMAT(NOW(),'%m-%d') OR DATE_FORMAT(FROM_UNIXTIME(b.birth_date),'%m-%d') = DATE_FORMAT(NOW()+ INTERVAL 1 DAY,'%m-%d') OR DATE_FORMAT(FROM_UNIXTIME(b.birth_date),'%m-%d') = DATE_FORMAT(NOW()- INTERVAL 1 DAY,'%m-%d')) AND b.role<>'2'";
    //$query = "SELECT a.user_id, a.first_name, a.last_name, b.birth_date FROM user_info_".$lang." as a join user as b on a.user_id = b.id " . " Where DATE_FORMAT(FROM_UNIXTIME(b.birth_date),'%m-%d') = DATE_FORMAT(convert_tz(now(), '+00:00', '".$timeOffset.":00'),'%m-%d')";
    //echo $query;
    $result = $db->selectQuery($query, null);

    return $result;
  }

}
