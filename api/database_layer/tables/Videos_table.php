<?php
class Videos_table implements DatabaseTables {
	private static $tableName  = "videos";
	private static $fields = array(
								"id" => "id",
								"user_id" => "user_id",
								"path" => "path",
								"en_description" => "en_description",
								"ru_description" => "ru_description",
								"am_description" => "am_description",
								"type" => "type",
								"file_format" => "file_format",
								"embed_code" => "embed_code",
								"file_size" => "file_size",
								"creation_date" => "creation_date"
							 );
	

	
	public static function getTableName(){
		return self::$tableName;
	}
	
	public static function getTableFields(){
		return self::$fields;
	}
	
	private static function getTableKeyField(){
		return self::$fields[ "id" ];
	}

	public static $videoType_uploaded = 0;
	public static $videoType_embed = 1;

	public static function getVideoList( $userId, $lang = "en" ){
		$db = new DBconnection();
		
		$selectFields = self::getTableFields();
		$langList = Config::getLanguages();
		for( $i = 0; $i < count( $langList ); $i++ ){
			unset( $selectFields[ ($langList[ $i ] . "_description") ] );
		}
		$getLangCol = self::getTranslatableCol('description', $lang);
        $query = "SELECT " . $db->getSelectPart( $selectFields ) . ", " . $getLangCol . " FROM " . self::getTableName() . " WHERE " . $db->getWherePart( self::getTableFields(), array( "user_id" => $userId )  ) . "ORDER BY id DESC ";
		$result = $db->selectQuery( $query, null );
        return $result;
	}
    
    public static function getTranslatableCol($col, $lang) {
      switch ($lang) {
        case 'en';
          $langList = array('en', 'ru', 'am');
          break;
        case 'ru';
          $langList = array('ru', 'en', 'am');
          break;
        case 'am';
          $langList = array('am', 'en', 'ru');
          break;
      }
      foreach ($langList as &$lang) {
        $lang = 'NULLIF(' . $lang .'_' . $col . ', "")';
      }
      return 'COALESCE(' . implode(',', $langList) . ') as ' . $col;
    }

    public static function getVideos_allLang( $userId ){
		$db = new DBconnection();
	
		$query = "SELECT " . $db->getSelectPart( self::getTableFields() ) . " FROM " . self::getTableName() . " WHERE " . $db->getWherePart( self::getTableKeyField(), $userId );
		$result = $db->selectQuery( $query, null );
	
		return $result[ 0 ];
	}
	

	public static function editVideo( $id, $data ){
		$db = new DBconnection();
		
		$query = "UPDATE " . self::getTableName() . " SET " . $db->getUpdatePart(self::getTableFields(), $data) . " WHERE " . $db->getWherePart( self::getTableKeyField(), $id );

		$db->updateQuery( $query );
	}
	
	public static function removeVideo( $id ){
		$db = new DBconnection();
	
		$query = "DELETE FROM " . self::getTableName() . " WHERE " . $db->getWherePart( self::getTableKeyField(), $id );
		$db->deleteQuery( $query );
	}
	
	public static function createVideo( $userId, $data ){
		$db = new DBconnection();
		
		$data[ "user_id" ] = $userId;
		$data[ "creation_date" ] = time();
		//$data[ "embed_code" ] = (isset($data[ "embed_code" ]) ? strip_tags($data[ "embed_code" ], "<iframe>"): '');
		
		$query = "INSERT INTO " . self::getTableName() . " " . $db->getInsertPart( self::getTableFields(), $data );
		return $db->insertQuery( $query );
	}
}