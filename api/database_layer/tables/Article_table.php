<?php
class Article_table implements DatabaseTables {
	private static $tableName  = "article";
	private static $fields = array(
								"id" => "id",
								"author_id" => "author_id",
								"creation_date" => "creation_date",
								"approve_status" => "approve_status",
								"am_title" => "am_title",
								"am_content" => "am_content",
								"ru_title" => "ru_title",
								"ru_content" => "ru_content",
								"en_title" => "en_title",
								"en_content" => "en_content"
							 );
	
	public static $STATUS__WAIT_APPROVEMENT = 0;
	public static $STATUS__APPROVE = 1;
	public static $STATUS__DISAPPROVE = 2;
	
	public static function getTableName(){
		return self::$tableName;
	}
	
	public static function getTableFields(){
		return self::$fields;
	}
	
	private static function getTableKeyField(){
		return self::$fields[ "id" ];
	}
	
	public static function getArticles( $langList = array( "en" ), $approveStatus = null, $search = null ){
		$db = new DBconnection();
		
		if( count( $langList ) == 1 ){
			$where = "";
			if( !is_null( $approveStatus ) ){
				$where .= $db->getWherePart( self::getTableFields(), array( "approve_status" => $approveStatus ) );
			}
			if( !is_null( $search ) ){
				if( $where != "" ){
					$where .= " AND ";
				}
				
				$where .= " MATCH ( " . self::$fields[ "title" ] . ", " . self::$fields[ "content" ] . " ) AGAINST ( " . $db->addVar( $search ) . " IN BOOLEAN MODE )";
			}
			if( $where != "" ){
				$where = " WHERE " . $where;
			}
			
			$query = "SELECT * FROM " . self::getTableName( $langList[ 0 ] ) . $where;
			
			//var_dump( $query );
			
			$result = $db->selectQuery( $query, null );
		}else{
			$simpleFields = self::getTableFields();
			unset( $simpleFields[ "title" ] );
			unset( $simpleFields[ "content" ] );
				
			$langFields = self::getTableFields();
			unset( $langFields[ "id" ] );
			unset( $langFields[ "author_id" ] );
			unset( $langFields[ "creation_date" ] );
			unset( $langFields[ "approve_status" ] );
			
			$where = "";
			if( !is_null( $approveStatus ) ){
				$where .= $db->getWherePart( self::getTableFields(), array( "approve_status" => $approveStatus ), false, true, ("t_" . $langList[ 0 ]) );
			}
			if( !is_null( $search ) ){
				if( $where != "" ){
					$where .= " AND ";
				}
			
				$where .= " MATCH ( t_" . $langList[ 0 ] . "." . self::$fields[ "title" ] . ", t_" . $langList[ 0 ] . "." . self::$fields[ "content" ] . " ) AGAINST ( " . $db->addVar( $search ) . " IN BOOLEAN MODE)";
			}
			if( $where != "" ){
				$where = " WHERE " . $where;
			}
			
			$select = $db->getSelectPart( $simpleFields, ("t_" . $langList[ 0 ]) );
			$join = "";
			$on = " ON ";
			for( $i = 0; $i < count( $langList ); $i++ ){
				$select .= ", " . $db->getSelectPart( $langFields, ("t_" . $langList[ $i ]), ($langList[ $i ] . "_") );
				
				$join .= " " . self::getTableName( $langList[ $i ] ) . " AS t_" . $langList[ $i ] . " INNER JOIN ";
				if( $i > 0 ){
					$on .= " t_" . $langList[ $i ] . "." . self::$fields[ "id" ] . " = t_" . $langList[ 0 ] . "." . self::$fields[ "id" ] . " AND ";
				}
			}
			$join = substr($join, 0, -11);
			$on = substr($on, 0, -4);
			
			$query = "SELECT $select FROM " . $join . " " . $on . $where;
			
			$result = $db->selectQuery( $query, null );
		}
		
	
		return $result;
	}
	
	
	public static function getArticlesShortList( $lang, $search = null, $selectContent = false, $whereStatuses = null ){
		$db = new DBconnection();
		
		$uiFileds = UserInfo_table::getTableFields();
		$uiTable = UserInfo_table::getTableName( $lang );
		
		$selectUiFileds = $uiFileds;
		unset( $selectUiFileds[ "user_id" ] );
		unset( $selectUiFileds[ "birth_place" ] );
		unset( $selectUiFileds[ "city" ] );
		unset( $selectUiFileds[ "profession" ] );
		unset( $selectUiFileds[ "education" ] );
		unset( $selectUiFileds[ "biography" ] );
		
		$selectAlFields = self::getTableFields();
		if( is_null( $whereStatuses ) ){
			unset( $selectAlFields[ "approve_status" ] );
		}
		$uList = Config::getLanguages();
		for( $i = 0; $i < count( $uList ); $i++ ){
			unset( $selectAlFields[ ($uList[ $i ] . "_title") ] );
			unset( $selectAlFields[ ($uList[ $i ] . "_content")  ] );
		}
		
		
		
		$query = "
SELECT " . 
	$db->getSelectPart( $selectAlFields, "al" ) . ", 
	al." . self::$fields[ ($lang . "_title") ] . " AS title, " . ($selectContent ? (" al." . self::$fields[ ($lang . "_content") ] . " AS content, ") : "") . 
	$db->getSelectPart( $selectUiFileds, "ui" ) . "
FROM 
		" . self::getTableName() . " AS al
	INNER JOIN	
		" . $uiTable . " AS ui
	ON al." . self::$fields[ "author_id" ] . " = ui." . $uiFileds[ "user_id" ] . "
WHERE "  . 
	(!is_null( $whereStatuses ) ? (" (" . $db->getWherePart(self::getTableFields(), array( "approve_status" => $whereStatuses ), true, true, "al") . ") ") : " al." . self::$fields[ "approve_status" ] . " = '" . self::$STATUS__APPROVE . "' ") .  
	(!is_null( $search ) ? (" AND MATCH ( " . self::$fields[ ($lang . "_title") ] . ", " . self::$fields[ ($lang . "_content") ] . " ) AGAINST ( " . $db->addVar( $search ) . " IN BOOLEAN MODE )") : "");


		$result = $db->selectQuery( $query, null );
		
		return $result;
	}
	
	
	public static function getArtcleDetail( $id, $lang, $currentLang = null ){
		$db = new DBconnection();
		
		$uiFileds = UserInfo_table::getTableFields();
		
		$uFileds = User_table::getTableFields();
		$uTable = User_table::getTableName();
		
		$selectUiFileds = $uiFileds;
		unset( $selectUiFileds[ "user_id" ] );
		unset( $selectUiFileds[ "birth_place" ] );
		unset( $selectUiFileds[ "city" ] );
		unset( $selectUiFileds[ "profession" ] );
		unset( $selectUiFileds[ "education" ] );
		unset( $selectUiFileds[ "biography" ] );
		
		//SELECT ****************************************************************
		$selectAlFields = self::getTableFields();
		$uList = Config::getLanguages();
		if( count( $lang ) == 1 ){
			for( $i = 0; $i < count( $uList ); $i++ ){
				unset( $selectAlFields[ ($uList[ $i ] . "_title") ] );
				unset( $selectAlFields[ ($uList[ $i ] . "_content")  ] );
			}
		}
		
		
		$select = 
			$db->getSelectPart( $selectAlFields, "al" ) . ", 
			u." . $uFileds[ "profile_picture" ] . " AS profile_picture ";
		if( count( $lang ) == 1 ){
			$select .= ", 
				al." . self::$fields[ ($lang[ 0 ] . "_title") ] . " AS title, 
				al." . self::$fields[ ($lang[ 0 ] . "_content") ] . " AS content, " . 
				$db->getSelectPart( $selectUiFileds, "ui" );
		}else{
			for( $i = 0; $i < count( $lang ); $i++ ){
				/* $asFields = array();
				foreach( $selectUiFileds as $key => $val ){
					$asFields[ $key ] = $lang[ $i ] . "_" . $key;
				}
				$select .= ", " . $db->getSelectPart( $selectUiFileds, ("ui_" . $lang[ $i ]), $asFields ); */
				$select .= ", " . $db->getSelectPart( $selectUiFileds, "ui" );
			}
		}

		
		//JOIN ****************************************************************
		$join = 
				self::getTableName() . " AS al
			INNER JOIN
				" . $uTable . " AS u ";
		if( count( $lang ) == 1 ){
			$join .= " INNER JOIN " . UserInfo_table::getTableName( $lang[ 0 ] ) . " AS ui ";
		}else{
			$join .= " INNER JOIN " . UserInfo_table::getTableName( $currentLang ) . " AS ui ";
			/* for( $i = 0; $i < count( $lang ); $i++ ){
				$join .= " INNER JOIN " . UserInfo_table::getTableName( $lang[ $i ] ) . " AS ui_" . $lang[ $i ] . " ";
			} */
		}
				
		//ON ****************************************************************
		$on = "al." . self::$fields[ "author_id" ] . " = u." . $uFileds[ "id" ];
/*		if( count( $lang ) == 1 ){
			$on .= " AND al." . self::$fields[ "author_id" ] . " = ui." . $uiFileds[ "user_id" ];
		}else{
			for( $i = 0; $i < count( $lang ); $i++ ){
				$on .= " AND al." . self::$fields[ "author_id" ] . " = ui_" . $lang[ $i ] . "." . $uiFileds[ "user_id" ];
			}
		} */
		$on .= " AND al." . self::$fields[ "author_id" ] . " = ui." . $uiFileds[ "user_id" ];
	
	
		
		$query = "
SELECT " . $select . "
FROM " . $join . "
ON " . $on . "
WHERE al." . self::$fields[ "id" ] . " = '" . $id . "'";
		
		$result = $db->selectQuery( $query, null );
		
		
		return $result[ 0 ];
	}
	

	/*
	public static function updateArticle( $id, $simpleFields, $langFields ){
		$db = new DBconnection();
		foreach( $langFields AS $lang => $data ){
			$data = array_merge( $data, $simpleFields );
			$query = "UPDATE " . self::getTableName( $lang ) . " SET " . $db->getUpdatePart(self::getTableFields(), $data) . " WHERE " . $db->getWherePart( self::getTableKeyField(), $id );
			$db->updateQuery( $query );
		}
	}
	*/
	
	public static function createArticle( $userId, $data ){
		$db = new DBconnection();
		
		$data[ "author_id" ] = $userId;
		$data[ "creation_date" ] = time();
		
		$query = "INSERT INTO " . self::getTableName() . " " . $db->getInsertPart( self::getTableFields(), $data );
		return $db->insertQuery( $query );
	}
	
	
	public static function setApprovement( $ids, $approveStatus = 1 ){
		$db = new DBconnection();
		$query = "UPDATE " . self::getTableName() . " SET " . $db->getUpdatePart(self::getTableFields(), array( "approve_status" => $approveStatus )) . " WHERE " . self::$fields[ "id" ] . " IN (" . $db->getCommaSepListPart( $ids ) . ")";
		$db->updateQuery( $query );
	}
	
	public static function getArticlesCount( $where ){
		$db = new DBconnection();
		$query = "SELECT COUNT( " . self::$fields[ "id" ] . " ) AS rowCount FROM " . self::getTableName() . " WHERE " . $db->getWherePart( self::getTableFields(), $where );
					
		$result = $db->selectQuery( $query, null );
		
		return $result[ 0 ][ "rowCount" ];
	}
}