<?php
class ForumComment_table implements DatabaseTables {
	private static $tableName  = "forum_comment";
	private static $fields = array(
								"id" => "id",
								"creation_date" => "creation_date",
								"approve_status" => "approve_status",
								"comment" => "comment",
								"author_id" => "author_id",
								"forum_id" => "forum_id"
							 );
	
	public static function getTableName(){
		return self::$tableName;
	}
	
	public static function getTableFields(){
		return self::$fields;
	}
	
	private static function getTableKeyField(){
		return self::$fields[ "id" ];
	}

	public static function approveComment($id){
		$db = new DBconnection();

		$query = "update forum_comment set approve_status=1 where id=".$id;
		$db->updateQuery($query);
	}

	public static function rejectComment($id){
		$db = new DBconnection();

		$query = "delete from forum_comment where id=".$id;
		$db->deleteQuery($query);
	}


	public static function getForumComment($id, $lang){
		$db = new DBconnection();
		$usertable = "user_info_".$lang;

    	/*$query = "SELECT fc.id, fc.creation_date, fc.comment, fc.author_id, fu.login, u.profile_picture FROM forum_comment AS fc
    			left join framework_user as fu on fc.author_id = fu.id
    			inner join user as u on fc.author_id=u.id
    			where fc.approve_status=1 and forum_id=".$id."  ORDER BY fc.id desc";*/
		$query = "SELECT fc.id, fc.creation_date, fc.comment, fc.author_id, ut.first_name, ut.last_name, u.profile_picture FROM forum_comment AS fc
    			inner join ".$usertable." as ut on fc.author_id = ut.user_id
    			inner join user as u on fc.author_id=u.id
    			where fc.approve_status=1 and forum_id=".$id."  ORDER BY fc.id desc";
		$result = $db->selectQuery( $query, null );
		return $result;
	}
	
	public static function getForumCommentAdmin(){
		$db = new DBconnection();

		$query = "SELECT fc.id, fc.creation_date, fc.comment, fc.author_id, fu.login, fc.forum_id, f.title FROM forum_comment AS fc inner join framework_user as fu on fc.author_id = fu.id inner join forum as f on fc.forum_id = f.id where fc.approve_status=0";
 		$result = $db->selectQuery( $query, null );
		return $result;
	}

	public static function forumCommentCountAdmin() {
		$db = new DBconnection();

		$query = "SELECT count(*) as count FROM forum_comment AS fc inner join framework_user as fu on fc.author_id = fu.id inner join forum as f on fc.forum_id = f.id where fc.approve_status=0";
 		$result = $db->selectQuery( $query, null );
		return $result;

	}
	public static function createComment($data){
		$db = new DBconnection();

		$query = "INSERT INTO forum_comment (`id`, `creation_date`, `approve_status`, `comment`, `author_id`, `forum_id`) VALUES (NULL, '". $data->creation_date ."', '".$data->approve_status."', '".$data->comment."', '".$data->author_id."', '".$data->forum_id."')";
 		$result = $db->insertQuery( $query);
		return $result;
	}




}