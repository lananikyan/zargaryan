<?php
class UserRelation_table implements DatabaseTables {
	private static $tableName  = "user_relation";
	private static $fields = array(
								"user_id" => "user_id",
								"rel_user_id" => "rel_user_id",
								"rel_type" => "rel_type" //father //mother //couple
							 );
	
	public static function getTableName(){
		return self::$tableName;
	}
	
	public static function getTableFields(){
		return self::$fields;
	}
	
	private static function getTableKeyField(){
		return self::$fields["id"];
	}
	
	
	public static $relationshipType_father = "father";
	public static $relationshipType_couple = "couple";
	public static $relationshipType_child = "child";
	
	public static function getTorNer($userId) {
		$query = "select tor.user_id as id from user_relation as tor
			left outer join user_relation as erexa on tor.rel_user_id = erexa.user_id
			where erexa.rel_user_id = ".$userId;

		$db = new DBconnection();
		$result = $db->selectQuery($query, null);
		return $result;
	}

	public static function getTree( $userId, $upCount, $downCount, $lang ){
		$db = new DBconnection();
		
		///////////////// PARENTS
		$select = "";
		$join = "";
		$on = "";
		$where = $db->getWherePart( self::getTableFields(), array( "user_id" => $userId ), false, true, "lev_1" );
		for( $i = 0; $i < $upCount; $i++ ){
			$num = $i + 1;
			$select .= ", lev_" . $num . "." . self::$fields[ "rel_user_id" ] . " AS user_" . $num . " ";
			$join .= " LEFT OUTER JOIN " . self::getTableName() . " AS lev_" . $num . " ";
			if( $i < ($upCount - 1) ){
				$on .= " AND lev_" . $num . "." . self::$fields[ "rel_user_id" ] . " = lev_" . ($num + 1) . "." . self::$fields[ "user_id" ] . " ";
			}
			if( $upCount != 1 ){
				$on .= " AND " . $db->getWherePart( self::getTableFields(), array( "rel_type" => self::$relationshipType_father ), false, true, ("lev_" . $num) );
			}else{
				$where .= " AND " . $db->getWherePart( self::getTableFields(), array( "rel_type" => self::$relationshipType_father ), false, true, ("lev_" . $num) );
			}
		}
		$join = substr($join, 16);
		if( $on != "" ){
			$on = " ON " . substr($on, 4);
		}
		$select = substr($select, 1);
		
		$query = "SELECT $select FROM $join $on WHERE " . $where;

		$parentsIds = $db->selectQuery( $query, null );
				
		$parentsUserListStr = implode(",", self::getIdsFromResult( $parentsIds ));

		///////////////// CHILDES + PARENTS childes
		$select = "";
		$join = "";
		$on = "";
		$where = $db->getWherePart( self::getTableFields(), array( "rel_user_id" => $userId ), false, true, "lev_1" );
		for( $i = 0; $i < $downCount; $i++ ){
			$num = $i + 1;
			$select .= ", lev_" . $num . "." . self::$fields[ "user_id" ] . " AS user_" . $num . " ";
			$join .= " LEFT OUTER JOIN " . self::getTableName() . " AS lev_" . $num . " ";
			if( $i < ($downCount - 2) ){
				$on .= " AND lev_" . $num . "." . self::$fields[ "user_id" ] . " = lev_" . ($num + 1) . "." . self::$fields[ "rel_user_id" ] . " ";
			}else
			if( $i < ($downCount - 1) ){
				$on .= " AND (lev_" . $num . "." . self::$fields[ "user_id" ] . " = lev_" . ($num + 1) . "." . self::$fields[ "rel_user_id" ] . " " . (count( $parentsIds ) > 0 ? (" OR lev_" . ($num + 1) . "." . self::$fields[ "rel_user_id" ] . " IN ( $parentsUserListStr )") : "") . ")";
			}
			if( $downCount != 1 ){
				//$on .= " AND " . $db->getWherePart( self::getTableFields(), array( "rel_type" => "father" ), false, false, ("lev_" . $num) );
			}else{
				//$where .= " AND " . $db->getWherePart( self::getTableFields(), array( "rel_type" => "father" ), false, false, ("lev_" . $num) );
			}
		}
		$join = substr($join, 16);
		if( $on != "" ){
			$on = " ON " . substr($on, 4);
		}
		$select = substr($select, 1);
		
		$query = "SELECT $select FROM $join $on WHERE " . $where;
		$childsIds = $db->selectQuery( $query, null );
				
		//@stex miguce petqa mi hat saxi CUPL-nerin havaqel irar glux
		$childsIds[ 0 ][ "user_me" ] = $userId;
		
		$allUsersIds = array_merge( $childsIds, $parentsIds );
		
		//JOIN WITH USER INFO
		$userF = User_table::getTableFields();
		$userInfoF = UserInfo_table::getTableFields();
		
		$selectUser = User_table::getTableFields();
		unset( $selectUser[ "login" ] );
		unset( $selectUser[ "pass" ] );
		unset( $selectUser[ "role" ] );
		unset( $selectUser[ "approve_status" ] );
		unset( $selectUser[ "last_login_date" ] );
		unset( $selectUser[ "is_active" ] );
		unset( $selectUser[ "country" ] );
		unset( $selectUser[ "email" ] );
		unset( $selectUser[ "skype" ] );
		unset( $selectUser[ "phone" ] );
		unset( $selectUser[ "whatsapp" ] );
		unset( $selectUser[ "viber" ] );
		
		$selectUserInfo = UserInfo_table::getTableFields( $lang );
		unset( $selectUserInfo[ "user_id" ] );
		unset( $selectUserInfo[ "birth_place" ] );
		unset( $selectUserInfo[ "city" ] );
		//unset( $selectUserInfo[ "profession" ] );
		unset( $selectUserInfo[ "education" ] );
		
		$query = "
SELECT " . $db->getSelectPart( $selectUser, "u" ) . ", " . $db->getSelectPart( $selectUserInfo, "ui" ) . ", " . $db->getSelectPart( self::getTableFields(), "ur" ) . "
FROM
	" . UserInfo_table::getTableName( $lang ) . " AS ui 
	INNER JOIN " . User_table::getTableName() . " AS u ON ui." . $userInfoF[ "user_id" ] . " = u." . $userF[ "id" ] . "			
	LEFT OUTER JOIN	" . self::getTableName() . " AS ur ON (ur." . self::$fields[ "user_id" ] . " = u." . $userF[ "id" ] . ")
WHERE u." . $userF[ "id" ] . " IN (" . implode(",", self::getIdsFromResult( $allUsersIds )) . ") AND u.role<>2";
		$res = $db->selectQuery( $query, null );
        foreach($res as &$row) {
          UserInfo_table::extendUserInfo($row['id'], $row, $lang );
        }
		return $res;
	}

/**
 * Return All users list from relation table
 * @param $lang
 * @return Result
 */
public static function getAllFatherUsersList( $lang ){
	$db = new DBconnection();

	$query = "SELECT `" . self::$fields[ "user_id" ] ."` FROM " . self::getTableName() .
						" WHERE `rel_type`='father'";

	$allUsersIds = $db->selectQuery( $query, null );


	//JOIN WITH USER INFO
	$userF = User_table::getTableFields();
	$userInfoF = UserInfo_table::getTableFields();

	$selectUser = User_table::getTableFields();
	unset( $selectUser[ "login" ] );
	unset( $selectUser[ "pass" ] );
	unset( $selectUser[ "role" ] );
	unset( $selectUser[ "approve_status" ] );
	unset( $selectUser[ "last_login_date" ] );
	unset( $selectUser[ "is_active" ] );

	unset( $selectUser[ "country" ] );
	unset( $selectUser[ "email" ] );
	unset( $selectUser[ "skype" ] );
	unset( $selectUser[ "phone" ] );
	unset( $selectUser[ "whatsapp" ] );
	unset( $selectUser[ "viber" ] );

	$selectUserInfo = UserInfo_table::getTableFields( $lang );
	unset( $selectUserInfo[ "user_id" ] );
	unset( $selectUserInfo[ "birth_place" ] );
	unset( $selectUserInfo[ "city" ] );
	//unset( $selectUserInfo[ "profession" ] );
	unset( $selectUserInfo[ "education" ] );

	$query = "
	SELECT " . $db->getSelectPart( $selectUser, "u" ) . ", " . $db->getSelectPart( $selectUserInfo, "ui" ) . ", " . $db->getSelectPart( self::getTableFields(), "ur" ) . "
	FROM
	" . UserInfo_table::getTableName( $lang ) . " AS ui 
	INNER JOIN " . User_table::getTableName() . " AS u ON ui." . $userInfoF[ "user_id" ] . " = u." . $userF[ "id" ] . "			
	LEFT OUTER JOIN	" . self::getTableName() . " AS ur ON (ur." . self::$fields[ "user_id" ] . " = u." . $userF[ "id" ] . ")
	WHERE u." . $userF[ "id" ] . " IN (" . implode(",", self::getIdsFromResult( $allUsersIds )) . ")
	AND ur." . self::$fields[ "rel_type" ] . "='" . self::$relationshipType_father . "'";

	$result = $db->selectQuery( $query, null );

	return $result;

}

	/**
	 * Check if user id exists in user_id column of relations table or not
	 * @param $userId Integer
	 * @param $type String User rel type
	 * @return bool
	 */
	public static function is_user_exists( $userId, $type = "father" ){
		$db = new DBconnection();

		return $result = $db->isValueExist( self::getTableName(), array(self::$fields[ "user_id" ], self::$fields["rel_type"]),  array($userId, $type));
	}

	/**
	 * Check if user has children
	 * @param $userId Integer
	 * @return bool
	 */
	public static function is_user_has_children( $userId ){
		$db = new DBconnection();

		return $result = $db->isValueExist( self::getTableName(), self::$fields[ "rel_user_id" ],  $userId);
	}

	public static function getRelatives( $userId, $lang, $rel_type ){
		$db = new DBconnection();

		//JOIN WITH USER INFO
		$userF = User_table::getTableFields();
		$userInfoF = UserInfo_table::getTableFields();

		//JOIN WITH COUNTRY INFO
		$countryT = CountryList_table::getTableName( $lang );
		$countryF = CountryList_table::getTableFields();
		//unset( $countryF[ "id" ] );

		$selectUser = User_table::getTableFields();
		unset( $selectUser[ "login" ] );
		unset( $selectUser[ "pass" ] );
		unset( $selectUser[ "role" ] );
		unset( $selectUser[ "approve_status" ] );
		unset( $selectUser[ "last_login_date" ] );
		unset( $selectUser[ "is_active" ] );

		//unset( $selectUser[ "country" ] );
		//unset( $selectUser[ "email" ] );
		//unset( $selectUser[ "skype" ] );
		//unset( $selectUser[ "phone" ] );
		//unset( $selectUser[ "whatsapp" ] );
		//unset( $selectUser[ "viber" ] );

		$selectUserInfo = UserInfo_table::getTableFields( $lang );
		unset( $selectUserInfo[ "user_id" ] );
		//unset( $selectUserInfo[ "birth_place" ] );
		//unset( $selectUserInfo[ "city" ] );
		//unset( $selectUserInfo[ "profession" ] );
		//unset( $selectUserInfo[ "education" ] );

		$query = "
SELECT " . $db->getSelectPart( $selectUser, "u" ) . ", " . $db->getSelectPart( $selectUserInfo, "ui" ) . ", " . $db->getSelectPart( self::getTableFields(), "ur" ) . ", " .$db->getSelectPart( $countryF, "uc" ) ."
FROM
	" . UserInfo_table::getTableName( $lang ) . " AS ui 
	INNER JOIN " . User_table::getTableName() . " AS u ON ui." . $userInfoF[ "user_id" ] . " = u." . $userF[ "id" ] . "			
	LEFT OUTER JOIN	" . self::getTableName() . " AS ur ON ur." . self::$fields[ "rel_user_id" ] . " = u." . $userF[ "id" ] . "
	LEFT OUTER JOIN " . CountryList_table::getTableName($lang) . " AS uc ON u." . $userF[ "country" ] . " = uc." . $countryF[ "id" ] . "
	WHERE ur." . self::$fields[ "user_id" ] . " = " . $userId . " AND ur." . self::$fields[ "rel_type" ] . " = '" .$rel_type ."'";
		$res = $db->selectQuery( $query, null );
		return $res;
	}

	private static function getIdsFromResult( $list ){
		$idsList = array();
		foreach( $list AS $item ){
			foreach( $item AS $id ){
				if( !is_null( $id ) ){
					$idsList[ $id ] = true;
				}
			}
		}
		
		$result = array();
		foreach( $idsList AS $id => $data ){
			$result[] = $id;
		}
		
		return $result;
	}
	
	
	public static function setUserRelation( $userId, $relUserId, $type ){
		$db = new DBconnection();
		
		$data = array(
					"user_id" => $userId,
					"rel_user_id" => $relUserId,
					"rel_type" => $type
				 );
		
		$query = "INSERT INTO " . self::getTableName() . " " . $db->getInsertPart( self::getTableFields(), $data ) . "
					ON DUPLICATE KEY
					UPDATE " . $db->getUpdatePart(self::getTableFields(), array( "rel_type" => $type ));
		
		$db->insertQuery( $query );
		//echo $query;
	}
	
	
	public static function removeUserRelation( $userId, $relUserId = null, $type = null ){
		$db = new DBconnection();
		
		$where1 = array( "user_id" => $userId, "rel_user_id" => $relUserId );
		$where2 = array( "rel_user_id" => $userId, "user_id" => $relUserId );
		if( is_null( $relUserId ) ){
			$where1 = array( "user_id" => $userId );
			$where2 = array( "rel_user_id" => $userId );
		}
		if(!is_null( $type ) ){
			$where1["rel_type"] = $type;
			$where2["rel_type"] = $type;
		}
		
		$query = "DELETE FROM " . self::getTableName() . " WHERE " . $db->getWherePart( self::getTableFields(), $where1 );

		$db->deleteQuery( $query );
		
		$query = "DELETE FROM " . self::getTableName() . " WHERE " . $db->getWherePart( self::getTableFields(), $where2 );
		$db->deleteQuery( $query );
	}

	public static function mergeUsers($userId, $relUserId){
		$db = new DBconnection();
		$where1 = array( "user_id" => $relUserId );
		$where2 = array( "rel_user_id" => $relUserId );
		$query = "UPDATE " . self::getTableName() . " SET user_id=" . $userId ." WHERE " . $db->getWherePart( self::getTableFields(), $where1 );
		$db->updateQuery($query);

		$query = "UPDATE " . self::getTableName() . " SET rel_user_id=" . $userId ." WHERE " . $db->getWherePart( self::getTableFields(), $where2 );
		$db->updateQuery($query);
	}
}