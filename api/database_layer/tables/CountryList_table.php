<?php
class CountryList_table implements DatabaseTables {
	private static $tableName  = "country_list";
	private static $fields = array(
								"id" => "id",
								"caption" => "caption"
							 );
	
	//   	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	
	
	
	public static function getTableName( $lang = "en" ){
		return self::$tableName . "_" . $lang;
	}
	
	public static function getTableFields(){
		return self::$fields;
	}
	
	private static function getTableKeyField(){
		return self::$fields[ "id" ];
	}
	
	public static function getCountryData( $langList = array( "en" ) ){
		$db = new DBconnection();
		
		if( count( $langList ) == 1 ){
			$query = "SELECT * FROM " . self::getTableName( $langList[ 0 ] );
			$result = $db->selectQuery( $query, self::getTableFields() );
		}else{
			$select = "t_" . $langList[ 0 ] . "." . self::$fields[ "id" ] . " AS id";
			$join = "";
			$on = " ON ";
			for( $i = 0; $i < count( $langList ); $i++ ){
				$select .= ", t_" . $langList[ $i ] . "." . self::$fields[ "caption" ] . " AS " . ($langList[ $i ] . "_" . self::$fields[ "caption" ]);
				
				$join .= " " . self::getTableName( $langList[ $i ] ) . " AS t_" . $langList[ $i ] . " INNER JOIN ";
				if( $i > 0 ){
					$on .= " t_" . $langList[ $i ] . "." . self::$fields[ "id" ] . " = t_" . $langList[ 0 ] . "." . self::$fields[ "id" ] . " AND ";
				}
			}
			$join = substr($join, 0, -11);
			$on = substr($on, 0, -4);
			//$select = substr($select, 0, -2);
			
			$query = "SELECT $select FROM " . $join . " " . $on;
			
			$result = $db->selectQuery( $query, null );
		}
		
	
		return $result;
	}
}