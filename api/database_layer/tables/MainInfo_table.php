<?php
class MainInfo_table implements DatabaseTables {
	private static $tableName  = "main_info";
	private static $fields = array(
								"id" => "id",
								"title" => "title",
								"short_content" => "short_content",
								"content" => "content"
							 );
	 	 	 	 	 	 	 	 	 	 	 	 	
	
	
	public static function getTableName( $lang = "en" ){
		return self::$tableName . "_" . $lang;
	}
	
	public static function getTableFields(){
		return self::$fields;
	}
	
	private static function getTableKeyField(){
		return self::$fields[ "id" ];
	}
	
	public static function getInfo( $langList = array( "en" ), $id = null ){
		$db = new DBconnection();
		
		if( count( $langList ) == 1 ){
			$query = "SELECT * FROM " . self::getTableName( $langList[ 0 ] ) . (!is_null( $id ) ? (" WHERE " . $db->getWherePart(self::getTableFields(), array( "id" => $id ))) : "");
			$result = $db->selectQuery( $query, null );
		}else{
			$simpleFields = self::getTableFields();
			unset( $simpleFields[ "title" ] );
			unset( $simpleFields[ "short_content" ] );
			unset( $simpleFields[ "content" ] );
				
			$langFields = self::getTableFields();
			unset( $langFields[ "id" ] );
			
			$select = $db->getSelectPart( $simpleFields, ("t_" . $langList[ 0 ]) );
			$join = "";
			$on = " ON ";
			for( $i = 0; $i < count( $langList ); $i++ ){
				$select .= ", " . $db->getSelectPart( $langFields, ("t_" . $langList[ $i ]), ($langList[ $i ] . "_") );
				
				$join .= " " . self::getTableName( $langList[ $i ] ) . " AS t_" . $langList[ $i ] . " INNER JOIN ";
				if( $i > 0 ){
					$on .= " t_" . $langList[ $i ] . "." . self::$fields[ "id" ] . " = t_" . $langList[ 0 ] . "." . self::$fields[ "id" ] . " AND ";
				}
			}
			$join = substr($join, 0, -11);
			$on = substr($on, 0, -4);
			//$select = substr($select, 0, -2);
			
			$query = "SELECT $select FROM " . $join . " " . $on;
			$query .= (!is_null( $id ) ? (" WHERE " . $db->getWherePart(self::getTableFields(), array( "id" => $id ), false, true, ("t_" . $langList[ 0 ]))) : "");
			$result = $db->selectQuery( $query, null );
		}
		
	
		return $result;
	}
	
	
	public static function updateInfo( $id, $langFields ){
		$db = new DBconnection();
		foreach( $langFields AS $lang => $data ){
			$query = "UPDATE " . self::getTableName( $lang ) . " SET " . $db->getUpdatePart(self::getTableFields(), $data) . " WHERE " . $db->getWherePart( self::getTableKeyField(), $id );
			$db->updateQuery( $query );
		}
	}
}