<?php
class AlbumPicture_table implements DatabaseTables {
	private static $tableName  = "album_picture";
	private static $fields = array(
								"id" => "id",
								"album_id" => "album_id",
								"path" => "path",
								"en_description" => "en_description",
								"ru_description" => "ru_description",
								"am_description" => "am_description",
		 						"file_size" => "file_size",
								"creation_date" => "creation_date"
							 );
	

	
	public static function getTableName(){
		return self::$tableName;
	}
	
	public static function getTableFields(){
		return self::$fields;
	}
	
	private static function getTableKeyField(){
		return self::$fields[ "id" ];
	}
	
	public static function getPictureList( $albumId, $lang = "en" ){
		$db = new DBconnection();
		
		$selectFields = self::getTableFields();
		$langList = Config::getLanguages();
		for( $i = 0; $i < count( $langList ); $i++ ){
			unset( $selectFields[ ($langList[ $i ] . "_description") ] );
		}
		$getLangCol = self::getTranslatableCol('description', $lang);
        $query = "SELECT " . $db->getSelectPart( $selectFields ) . ", " . $getLangCol . " FROM " . self::getTableName() . " WHERE " . $db->getWherePart( self::getTableFields(), array( "album_id" => $albumId ) ) . "ORDER BY " . self::$fields[ "id" ] . " DESC";
		$result = $db->selectQuery( $query, null );

		return $result;
	}
	
    public static function getTranslatableCol($col, $lang) {
      switch ($lang) {
        case 'en';
          $langList = array('en', 'ru', 'am');
          break;
        case 'ru';
          $langList = array('ru', 'en', 'am');
          break;
        case 'am';
          $langList = array('am', 'en', 'ru');
          break;
      }
      foreach ($langList as &$lang) {
        $lang = 'NULLIF(' . $lang . '_' . $col . ', "")';
      }
      return 'COALESCE(' . implode(',', $langList) . ') as ' . $col;
    }

  public static function getPictures_allLang( $id ){
		$db = new DBconnection();
	
		$query = "SELECT " . $db->getSelectPart( self::getTableFields() ) . " FROM " . self::getTableName() . " WHERE " . $db->getWherePart( self::getTableKeyField(), $id );
		$result = $db->selectQuery( $query, null );
	
		return $result[ 0 ];
	}
	

	public static function editPicture( $id, $data ){
		$db = new DBconnection();
		
		$query = "UPDATE " . self::getTableName() . " SET " . $db->getUpdatePart(self::getTableFields(), $data) . " WHERE " . $db->getWherePart( self::getTableKeyField(), $id );
		$db->updateQuery( $query );
	}
	
	public static function removePicture( $id ){
		$db = new DBconnection();
	
		$query = "DELETE FROM " . self::getTableName() . " WHERE " . $db->getWherePart( self::getTableKeyField(), $id );
		$db->deleteQuery( $query );
	}
	
	public static function removePicturesByAlbumId( $albumId ){
		$db = new DBconnection();
	
		$query = "DELETE FROM " . self::getTableName() . " WHERE " . $db->getWherePart( self::getTableFields(), array( "album_id" => $albumId ) );
		$db->deleteQuery( $query );
	}
	
	public static function createPicture( $albumId, $data ){
		$db = new DBconnection();
		
		$data[ "album_id" ] = $albumId;
		$data[ "creation_date" ] = time();
		
		$query = "INSERT INTO " . self::getTableName() . " " . $db->getInsertPart( self::getTableFields(), $data );
		return $db->insertQuery( $query );
	}
}