<?php
class Forum_table implements DatabaseTables {
	private static $tableName  = "forum";
	private static $fields = array(
								"id" => "id",
								"creation_date" => "creation_date",
								"approve_status" => "approve_status",
								"title" => "title",
								"lang" => "lang"
							 );
	
	public static function getTableName(){
		return self::$tableName;
	}
	
	public static function getTableFields(){
		return self::$fields;
	}
	
	private static function getTableKeyField(){
		return self::$fields[ "id" ];
	}
	
	public static function getForum( ){
		$db = new DBconnection();
    	$query = "SELECT * FROM forum where approve_status=1 ORDER BY id desc";
		$result = $db->selectQuery( $query, null );
		return $result;
	}
	
	public static function getForumAdmin(){
		$db = new DBconnection();

    	$query = "SELECT * FROM forum";
		$result = $db->selectQuery( $query, null );
		return $result;
	}

	public static function forumCountAdmin(){
		$db = new DBconnection();

    	$query = "SELECT count(*) as count FROM forum where approve_status=0";
		$result = $db->selectQuery( $query, null );
		return $result;
	}




	public static function approveForum($id){
		$db = new DBconnection();

		$query = "update forum set approve_status=1 where id=".$id;
		$db->updateQuery($query);
	}

	public static function rejectForum($id){
		$db = new DBconnection();

		$query = "delete from forum where id=".$id;
		$db->deleteQuery($query);
	}

	public static function updateForumTitle($id, $title){
    		$db = new DBconnection();
   			$query = "UPDATE forum set title='".$title."' where id=".$id;
   			$db->updateQuery( $query );
   	}

	
	public static function createForum($data ){
		$db = new DBconnection();
		$query = "INSERT INTO `forum` (`id`, `creation_date`, `approve_status`, `title`, `lang`) VALUES (NULL, '".time()."', '".$data->approve_status."', '".$data->title."', '".$data->lang."')";
		return $db->insertQuery( $query );
	}
	
}