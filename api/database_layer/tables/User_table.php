<?php
class User_table implements DatabaseTables {
	private static $tableName  = "user";
	private static $fields = array(
								"id" => "id",
								"login" => "login",
								"pass" => "pass",
								"sex" => "sex",
								"role" => "role",
								"create_date" => "create_date",
								"last_login_date" => "last_login_date",
								"approve_status" => "approve_status",
								"is_active" => "is_active",
								"birth_date" => "birth_date",
								"profile_picture" => "profile_picture",
								"country" => "country",
								"email" => "email",
								"skype" => "skype",
								"phone" => "phone",
								"viber" => "viber",
								"whatsapp" => "whatsapp",
								"dead_date" => "dead_date",
								"unique_token" => "unique_token"
							 );
	
	public static $STATUS__WAIT_APPROVEMENT = 0;
	public static $STATUS__APPROVE = 1;
	public static $STATUS__DISAPPROVE = 2;
	
	public static $STATUS_NOT_ACTIVE = 2;
	
	
	public static function getTableName(){
		return self::$tableName;
	}
	
	public static function getTableFields(){
		return self::$fields;
	}
	
	private static function getTableKeyField(){
		return self::$fields["id"];
	}
	
	public static function login( $login, $pass ){
		$values = array();
		$values[ "login" ] = $login;
		$values[ "pass" ] = md5( $pass );
		
		/*
		 * 		$countryT = CountryList_table::getTableName( $langList[ 0 ] );
		$countryF = CountryList_table::getTableFields();
		 */
		
		$db = new DBconnection();
		$query = "SELECT * FROM " . self::getTableName() . " WHERE " . $db->getWherePart( self::getTableFields(), $values );
		$result = $db->selectQuery( $query, self::getTableFields() );
				
		
		if( isset( $result[0] ) ){
			if( $result[ 0 ][ "is_active" ] == "0" ){
				return self::$STATUS_NOT_ACTIVE;
			}
			if( $result[ 0 ][ "approve_status" ] != self::$STATUS__APPROVE ){
				return $result[ 0 ][ "approve_status" ];
			}
			
			$query = "UPDATE " . self::getTableName() . " SET " . $db->getUpdatePart(self::getTableFields(), array( "last_login_date" => time() )) . " WHERE " . $db->getWherePart( self::getTableFields(), $values );
			$db->updateQuery( $query );
						
			//unset( $result[0][ "pass" ] );
			return $result[0];
		}
		
		return null;
	}
    public static function userSessionStart($user_id) {
      $timestamp = (int) $_SERVER['REQUEST_TIME'];
      $sid = self::http_base64_encode(session_id(). $user_id);
      $db = new DBconnection();
      $conn = $db->getConnection();
      $sql = "INSERT INTO user_sessions(user_id, sid, timestamp)
            VALUES (:user_id, :sid, :timestamp)
            ON DUPLICATE KEY UPDATE timestamp = :timestamp";
      $stmt = $conn->prepare($sql);
      $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
      $stmt->bindParam(':sid', $sid, PDO::PARAM_STR);
      $stmt->bindParam(':timestamp', $timestamp, PDO::PARAM_INT);
      if($stmt->execute()) {
        return $sid;
      }
      return null;
    }
    
    public static function userSessionDestroy($user_id) {
      $sid = self::http_base64_encode(session_id() . $user_id);
      $db = new DBconnection();
      $conn = $db->getConnection();
      $sql = "DELETE FROM user_sessions WHERE sid = :sid";
      $stmt = $conn->prepare($sql);
      $stmt->bindParam(':sid', $sid, PDO::PARAM_STR);
      if($stmt->execute()) {
        return true;
      }
      return false;
    }
    
    /**
    * Returns a URL-safe, base64 encoded version of the supplied string.
    */
    public static function http_base64_encode($string) {
      $data = base64_encode($string);
      // Modify the output so it's safe to use in URLs.
      return strtr($data, array('+' => '-', '/' => '_', '=' => ''));
    }
	
	public static function update( $id, $data ){
		if( isset( $data[ "pass" ] ) ){
			$data[ "pass" ] = md5( $data[ "pass" ] );
		}
		
		$db = new DBconnection();
		$query = "UPDATE " . self::getTableName() . " SET " . $db->getUpdatePart(self::getTableFields(), $data) . " WHERE " . $db->getWherePart( self::getTableKeyField(), $id );
		$db->updateQuery( $query );
	}
	
	public static function isLoginExist( $login ){
		$db = new DBconnection();
		$query = "SELECT " . self::$fields[ "login" ] . " FROM " . self::getTableName() . " WHERE " . $db->getWherePart( self::getTableFields(), array( "login" => $login ) );
		$result = $db->selectQuery( $query, null );
		
		if( count( $result ) == 0 ){
			return false;
		}
		
		return true;
	}
	
	public static function create( $data ){
		$data[ "create_date" ] = time();
		$data[ "pass" ] = md5( $data[ "pass" ] );

		$db = new DBconnection();
		$query = "INSERT INTO " . self::getTableName() . " " . $db->getInsertPart( self::getTableFields(), $data );

		return $db->insertQuery( $query );
	}
	
	public static function getUsersCount( $where, $otherCondition = null ){
		$db = new DBconnection();
		$query = "SELECT COUNT( " . self::$fields[ "id" ] . " ) AS co FROM " . self::getTableName() . " WHERE " . $db->getWherePart( self::getTableFields(), $where );
		$result = $db->selectQuery( $query, null );
		
		return $result[ 0 ][ "co" ];
	}

	public static function removeUserRelationInfo( $relUserId, $allLangs) {
		$db = new DBconnection();

		$query = "DELETE FROM " . self::getTableName() . " WHERE " . $db->getWherePart(self::getTableKeyField(), $relUserId);
		$db->deleteQuery( $query );

		$selectUserInfoF = UserInfo_table::getTableFields();
		foreach ($allLangs as $lang) {
			$selectUserInfoT = UserInfo_table::getTableName($lang );
			$query = "DELETE FROM " . $selectUserInfoT . " WHERE " . $db->getWherePart($selectUserInfoF['user_id'], $relUserId);
			$db->deleteQuery( $query );
		}
	}

	public static function getUserRole( $userId) {
		$db = new DBconnection();
		$selectUser = self::getTableFields();
		unset( $selectUser[ "login" ] );
		unset( $selectUser[ "pass" ] );
		unset( $selectUser[ "approve_status" ] );
		unset( $selectUser[ "last_login_date" ] );
		unset( $selectUser[ "is_active" ] );
		unset( $selectUser[ "sex" ] );
		unset( $selectUser[ "birth_date" ] );
		unset( $selectUser[ "country" ] );
		unset( $selectUser[ "email" ] );
		unset( $selectUser[ "create_date"]);
		unset( $selectUser[ "profile_picture"]);
		unset( $selectUser[ "country"]);
		unset( $selectUser[ "skype"]);
		unset( $selectUser[ "phone"]);
		unset( $selectUser[ "viber"]);
		unset( $selectUser[ "whatsapp"]);
		unset( $selectUser[ "dead_date"]);
		unset( $selectUser[ "unique_token"]);

		$selectUserRelF = UserRelation_table::getTableFields();
		$selectUserRelT = UserRelation_table::getTableName();
		$query = "
SELECT " . $db->getSelectPart( $selectUser, "u" ) . ", " . $db->getSelectPart( $selectUserRelF, "ur" ) . "
FROM
	" . self::getTableName() . " AS u 
	INNER JOIN " . $selectUserRelT . " AS ur ON ur." . $selectUserRelF[ "rel_user_id" ] . " = u." . $selectUser[ "id" ] . "
		WHERE u." . self::$fields[ "id" ] . " = " . $userId ;
		//$query = "SELECT " . self::$fields[ "role" ] . " FROM " . self::getTableName() . " WHERE " . $db->getWherePart( self::$fields[ "id" ], $userId );
		$result = $db->selectQuery( $query, null );

		return $result;
	}

	public static function getUsersByRole($roleId, $withInfo = false) {
		$db = new DBconnection();
		$query = "SELECT *  FROM " . self::getTableName() . " WHERE " . $db->getWherePart( self::getTableFields(), array("role" => $roleId), false, true );
		return $result = $db->selectQuery( $query, null );
	}

	public static function getUserPicFileUsed($id) {
		$db = new DBconnection();
		$albumPicture = AlbumPicture_table::getTableFields();
		$album = Album_table::getTableFields();

		$query = "SELECT SUM( pic." . $albumPicture[ "file_size" ] . " ) AS FileSum 
		FROM " . AlbumPicture_table::getTableName() . " AS pic
		INNER JOIN
		" . Album_table::getTableName() . " AS al ON al. " . $album[ "id" ] . " = pic. " . $albumPicture[ "album_id" ] . "
		INNER JOIN
		" . self::$tableName . " AS u ON al. " . $album[ "user_id" ] . " = u. " . self::$fields[ "id" ] . "
		
		WHERE u." . self::$fields[ "id" ] ."=" .$id;

		$result = $db->selectQuery( $query, null );

		return $result[ 0 ][ "FileSum" ];
	}

	public static function getUserVideoFileUsed($id) {
		$db = new DBconnection();
		$videosF = Videos_table::getTableFields();

		$query = "SELECT SUM( v." . $videosF[ "file_size" ] . " ) AS FileSum 
		FROM " . Videos_table::getTableName() . " AS v
		INNER JOIN
		" . self::$tableName . " AS u ON v. " . $videosF[ "user_id" ] . " = u. " . self::$fields[ "id" ] . "
		WHERE u." . self::$fields[ "id" ] ."=" .$id;

		$result = $db->selectQuery( $query, null );

		return $result[ 0 ][ "FileSum" ];
	}
}