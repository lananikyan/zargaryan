<?php
class PhpbbUsers_table implements DatabaseTables {
	private static $tableName  = "phpbb_users";
	private static $fields = array( //here are only fields which I will use
								"user_id" => "user_id",
								"username" => "username",
								"username_clean" => "username_clean",
								"user_avatar" => "user_avatar",
								"user_avatar_type" => "user_avatar_type",
								"user_avatar_width" => "user_avatar_width",
								"user_avatar_height" => "user_avatar_height"
							 );
	
	public static function getTableName(){
		return self::$tableName;
	}
	
	public static function getTableFields(){
		return self::$fields;
	}
	
	private static function getTableKeyField(){
		return self::$fields[ "user_id" ];
	}
	
	public static function updateUserData( $id, $data ){
		$db = new DBconnection();

		$query = "UPDATE " . self::getTableName() . " SET " . $db->getUpdatePart(self::getTableFields(), $data) . " WHERE " . $db->getWherePart( self::getTableKeyField(), $id );
		$db->updateQuery( $query );
	}
	
	public static function getData( $where ){
		$db = new DBconnection();
	
		$query = "SELECT * FROM " . self::getTableName() . " WHERE " . $db->getWherePart( self::getTableFields(), $where );
		$result = $db->selectQuery( $query, null );
	
		return $result;
	}
}


