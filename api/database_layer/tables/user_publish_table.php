<?php
class UserPublish_table implements DatabaseTables {
	private static $tableName  = "user_publish";
	private static $fields = array(
								"user_id" => "user_id",
								"way_id" => "way_id",
								"to_user_id" => "to_user_id",
								"way_type" => "way_type",
								"sweepstake_id" => "sweepstake_id",
								"publish_date" => "publish_date",
								"point_publish" => "point_publish"
							 );
	
	
	
	public static function getTableName(){
		return self::$tableName;
	}
	
	public static function getTableFields(){
		return self::$fields;
	}
	
	private static function getTableKeyField(){
		return null;
	}
	
	public static function setPublish( $sweepstakeId, $userId, $wayId, $wayType = 1, $toUser = "", $pointPublish = false ){
		$db = new DBconnection();
		$data = array();
		$data[ "publish_date" ] = time();
		$data[ "sweepstake_id" ] = $sweepstakeId;
		$data[ "user_id" ] = $userId;
		$data[ "way_id" ] = $wayId;
		$data[ "way_type" ] = $wayType;
		$data[ "to_user_id" ] = $toUser;
		if( $pointPublish ){
			$data[ "point_publish" ] = 1;
		}else{
			$data[ "point_publish" ] = 0;
		}
		
		$query = "INSERT INTO " . self::getTableName() . " " . $db->getInsertPart( self::getTableFields(), $data );
		$db->insertQuery( $query );
	}
	
	public static function getLastPointDate( $sweepstakeId, $userId, $wayType ){
		$db = new DBconnection();
		$data = array();
		$data[ "sweepstake_id" ] = $sweepstakeId;
		$data[ "user_id" ] = $userId;
		$data[ "way_type" ] = $wayType;
		$data[ "point_publish" ] = 1;
		
		$query = "SELECT MAX( `" . self::$fields[ "publish_date" ] . "` ) AS publish_date FROM " . self::getTableName() . " WHERE " . $db->getWherePart( self::getTableFields(), $data );
		
		$result = $db->selectQuery( $query, self::getTableFields() );
		if( count( $result ) == 0 ){
			return 0;
		}
		return $result[ 0 ][ "publish_date" ];
	}
}