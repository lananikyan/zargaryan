<?php
class PhpbbConfig_table implements DatabaseTables {
	private static $tableName  = "phpbb_config";
	private static $fields = array(
			"config_name" => "config_name",
			"config_value" => "config_value",
			"is_dynamic" => "is_dynamic"
	);

	public static function getTableName(){
		return self::$tableName;
	}

	public static function getTableFields(){
		return self::$fields;
	}

	private static function getTableKeyField(){
		return null;
	}

	public static function getValues( $where ){
		$db = new DBconnection();

		$query = "SELECT * FROM " . self::getTableName() . " WHERE " . $db->getWherePart( self::getTableFields(), $where, true );
		$result = $db->selectQuery( $query, null );
		
		$retResult = array();
		for( $i = 0; $i < count( $result ); $i++ ){
			$retResult[ $result[ $i ][ "config_name" ] ] = $result[ $i ][ "config_value" ];
		}

		return $retResult;
	}
}