<?php
class Config {
	private static $dbHost;
	private static $dbUsername;
	private static $dbPassword;
	private static $dbDatabaseName;
	private static $baseURL;
    private static $nodejsHost;
    private static $nodejsKey;
  	private static $dataDir = "_data_/";
	private static $dataFilesDir = "_data_files_/";
	private static $uploadFilesDir = "_data_/upload_file/";
	private static $contactUsFilesDir = "_data_/contact/";
	private static $userPhotoFilesDir = "_data_/user_photo/";
  	private static $userPhotoDirName = "user_photo";

  	private static $PhotoStylesDir = "_data_/photo_styles/";
  	private static $PhotoStylesDirName = "photo_styles";

  	private static $userVideoFilesDir = "_data_/user_video/";

	private static $exRootDir = "";
	private static $directCall = true;
	private static $angularJS = true;
	private static $languages = array( "ru", "am", "en" ); // null IS no translation (one basic language)
	private static $languagesDir = "lang/";
  	private static $userFileUploadLimit = 300; //in MB
	
	private static $adminEmail = "admin@admin.com";

	/**
	 * @return the $adminEmail
	 */
	public static function getAdminEmail() {
		return Config::$adminEmail;
	}

	/**
	 * @return the $languagesDir
	 */
	public static function getLanguagesDir() {
		return (self::getBaseDir() . self::$languagesDir);
	}

	/**
	 * @return the $languages
	 */
	public static function getLanguages() {
		return Config::$languages;
	}

	public static function getDBhost(){
		return self::$dbHost;
	}
	
	public static function getDBusername(){
		return self::$dbUsername;
	}
	
	public static function getDBpassword(){
		return self::$dbPassword;
	}
	
	public static function getDBdatabaseName(){
		return self::$dbDatabaseName;
	}

	public static function useAlgularJs(){
	  return self::$angularJS;
	}
	
	public static function getBaseURL(){
		return self::$baseURL;
	}
    
    public static function getNodejsHost(){
		return self::$nodejsHost;
	}
    
    public static function getNodejsKey(){
		return self::$nodejsKey;
	}
	
	public static function setDBhost($host){
		self::$dbHost = $host;
	}
	
	public static function setDBusername($username){
		self::$dbUsername = $username;
	}
	
	public static function setDBpassword($password){
		self::$dbPassword = $password;
	}
	
	public static function setDBdatabaseName($db_name){
		self::$dbDatabaseName = $db_name;
	}
	
	public static function setBaseURL($base_url){
		self::$baseURL = $base_url;
	}
    
    public static function setNodejsHost($nodejs_host){
		self::$nodejsHost = $nodejs_host;
	}
    
    public static function setNodejsKey($nodejs_key){
		self::$nodejsKey = $nodejs_key;
	}
    
	public static function getBaseDir(){
		return dirname(__FILE__) . "/";
	}
	public static function getDataBaseDir(){
		return dirname( dirname(__FILE__) ) . "/";
	}
	
	
	public static function getUploadFileDir(){
		return (dirname( dirname(__FILE__) ) . "/" . self::$uploadFilesDir);
	}
	public static function getUploadFilePath(){
		return self::$uploadFilesDir;
	}
	public static function getDataFileDir(){
		return (self::getBaseDir() . self::$dataFilesDir);
	}
	public static function getDataFileUrl(){
		return (self::getBaseURL() . self::$dataFilesDir);
	}
	public static function getContactUsFilesPath(){
		return self::$contactUsFilesDir;
	}
	public static function getContactUsFilesDir(){
		return (dirname( dirname(__FILE__) ) . "/" . self::$contactUsFilesDir);
	}
	public static function getContactUsFilesUrl(){
		return (self::getBaseURL() . self::$contactUsFilesDir);
	}
	
	public static function getUserPhotoFilesPath(){
		return self::$userPhotoFilesDir;
	}
	public static function getUserPhotoFilesDir(){
		return (dirname( dirname(__FILE__) ) . "/" . self::$userPhotoFilesDir);
	}

	public static function getDataDir(){
	  return (dirname( dirname(__FILE__) ) . "/" . self::$dataDir);
	}
	public static function getDataDirUrl(){
	  return (self::getBaseURL() . self::$dataDir);
	}

	public static function getPhotoStylesDir(){
	  return (dirname( dirname(__FILE__) ) . "/" . self::$PhotoStylesDir);
	}
	public static function getUserPhotoFilesUrl(){
		return (self::getBaseURL() . self::$userPhotoFilesDir);
	}

	public static function getUserPhotoDirName(){
	  return self::$userPhotoDirName;
	}
	public static function getPhotoStylesDirName(){
	  return self::$PhotoStylesDirName;
	}
	public static function getUserVideoFilesPath(){
	  return self::$userVideoFilesDir;
	}
	public static function getUserVideoFilesDir(){
	  return (dirname( dirname(__FILE__) ) . "/" . self::$userVideoFilesDir);
	}
	public static function getUserVideoFilesUrl(){
	  return (self::getBaseURL() . self::$userVideoFilesDir);
	}


	public static function getUserFileUploadLimit() {
	  return Config::$userFileUploadLimit;
	}
	
	public static function setNonDirectCall(){
		self::$directCall = false;
	}
	
	public static function getModulesDir(){
		return (self::getBaseDir() . self::$modulesDir);
	}
	public static function getModulesUrl(){
		return (self::getBaseURL() . self::$modulesDir);
	}
	
	public static function getTemplatesDir(){
		return (self::getBaseDir() . self::$templatesDir);
	}
	public static function getTemplatesUrl(){
		return (self::getBaseURL() . self::$templatesDir);
	}
	
//***************************************************************************************************************************************************************************
}