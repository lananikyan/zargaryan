<?php

require_once Config::getBaseDir() . 'database_layer/tables/Album_table.php';
require_once Config::getBaseDir() . 'database_layer/tables/AlbumPicture_table.php';

class AlbumManager {

	public function createAlbum( $data ){
		$userManager = new UserManager();
		$userData = $userManager->getCurrentUser();
		
		$data = FormatManager::UItoDB( $data, true );
		$id = Album_table::createAlbum($userData[ "id" ], $data);
		
		return $id;
	}
	
	public function editAlbum( $id, $data ){
		$data = FormatManager::UItoDB( $data, true );
		Album_table::editAlbum($id, $data);
	}
	
	public function removeAlbum( $id ){
		$list = AlbumPicture_table::getPictureList( $id );
		for( $i = 0; $i < count( $list ); $i++ ){
			$this->removeAlbumPictureFile( $list[ $i ][ "path" ] );
		}
		
		Album_table::removeAlbum( $id );
	}
	
	public function getAlbum_allLang( $id ){
		return Album_table::getAlbum_allLang( $id );
	}
	
	public function getAlbumList( $userId ){
		$langManager = new LanguageManager();
		$userManager = new UserManager();
		$userData = $userManager->getCurrentUser();
		
		if( is_null( $userId ) ){
			$userId = $userData[ "id" ];
		}
		$isPublic = true;
		if( $userId == $userData[ "id" ] || $userData[ "role" ] == UserManager::$ROLE_ADMIN || $userData[ "role" ] == UserManager::$ROLE_GOD ){
			$isPublic = false;
		}
		
		
		$list = Album_table::getAlbumList($userId, $langManager->getCurrentLanguage(), ($userId == $userData[ "id" ]));
		return $list;
	}
	
	public function createPicture( $albumId, $data ){
		$upM = new UploadManager();

		$fileName = $upM->saveUploadedFile( Config::getUserPhotoFilesDir(), $upM->generateRandomString() );

		//Delete also all styles for current user if exists
		//Need to include imageManager class
		$imageManager = new \Image\ImageManager(Config::getDataDir(), Config::getUserPhotoDirName(), Config::getPhotoStylesDirName());
		$imageManager->deleteAllStyleImagesByName($fileName);

		if( is_null( $fileName ) ){
			return false;
		}

		$fileSize = $upM->getFileSize();
		$data[ "path" ] = Config::getUserPhotoFilesPath() . $fileName;
		$data[ "file_size" ] = $fileSize;
				
		$data = FormatManager::UItoDB( $data, true );
		$id = AlbumPicture_table::createPicture( $albumId, $data );
		
		return $id;
	}
	
	public function editPicture( $id, $data ){
		$data = FormatManager::UItoDB( $data, true );
		AlbumPicture_table::editPicture($id, $data);
	}
	
	public function removePicture( $id ){
		$data = AlbumPicture_table::getPictures_allLang( $id );
		
		$this->removeAlbumPictureFile( $data[ "path" ] );
		
		AlbumPicture_table::removePicture( $id );
	}
	
	private function removeAlbumPictureFile( $path ){
		if( file_exists( (Config::getDataBaseDir() . $path) ) ){
			unlink( (Config::getDataBaseDir() . $path) );
		}
	}
	
	public function getPicture_allLang( $id ){
		return AlbumPicture_table::getPictures_allLang( $id );
	}
	
	public function getPictureList( $albumId ){
		$langManager = new LanguageManager();
		
		$list = AlbumPicture_table::getPictureList( $albumId, $langManager->getCurrentLanguage() );
		return $list;
	}
}