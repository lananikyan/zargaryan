<?php

require_once Config::getBaseDir() . 'database_layer/tables/Videos_table.php';

class VideoManager {
	
	public function getVideoList( $userId ){
		$langManager = new LanguageManager();
		$userManager = new UserManager();
		$userData = $userManager->getCurrentUser();
		
		if( is_null( $userId ) ){
			$userId = $userData[ "id" ];
		}
		$isPublic = true;
		if( $userId == $userData[ "id" ] || $userData[ "role" ] == UserManager::$ROLE_ADMIN || $userData[ "role" ] == UserManager::$ROLE_GOD ){
			$isPublic = false;
		}

		$list = Videos_table::getVideoList($userId, $langManager->getCurrentLanguage(), ($userId == $userData[ "id" ]));
		return $list;
	}
	
	public function createVideo( $userId, $data ){
		$upM = new UploadManager();

		if(!is_dir(Config::getUserVideoFilesDir())) {
			mkdir(Config::getUserVideoFilesDir() , 0777);
		}

		$fileExistsForUpload = $upM->isFileExistForUpload();

		if ($fileExistsForUpload == 1) {
			$fileName = $upM->saveUploadedFile( Config::getUserVideoFilesDir(), $upM->generateRandomString() );
			if( is_null( $fileName ) ){
				return false;
			}
			$fileSize = $upM->getFileSize();
			$data[ "path" ] = Config::getUserVideoFilesPath() . $fileName;
			$data[ "file_size" ] = $fileSize;
		}
		
		$data = FormatManager::UItoDB( $data, true );
		$id = Videos_table::createVideo( $userId, $data );
		
		return $id;
	}
	
	public function editVideo( $id, $data ){
		$data = FormatManager::UItoDB( $data, true );
		Videos_table::editVideo($id, $data);
	}
	
	public function removeVideoItem( $id ){
		$data = Videos_table::getVideos_allLang( $id );

		if ($data['type'] == 0) {
			$this->removeVIdeoFile( $data[ "path" ] );
		}

		Videos_table::removeVideo( $id );
	}
	
	private function removeVIdeoFile( $path ){
		if( file_exists( (Config::getDataBaseDir() . $path) ) ){
			unlink( (Config::getDataBaseDir() . $path) );
		}
	}
	
	public function getVideo_allLang( $id ){
		return Videos_table::getVideos_allLang( $id );
	}

}