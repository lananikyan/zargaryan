<?php

namespace Image;

require_once 'ImageConfigs.php';
require_once 'ImageResize.php';
require_once 'ImageException.php';

/**
 * Class ImageManager
 * @package Image
 */ 
class ImageManager {

  /**
   * @var ImageConfig class
   *
   */
  public $config;

  /**
   * ImageManager constructor.
   * @param string $root_dir. Path to root directory of images folder
   * @param string $originalDirName. Original images folder name.
   * @param string $stylesRootDirName. Styles folders root directory, defult will be used root directory
   */
  public function __construct($root_dir, $originalDirName, $stylesRootDirName = null){
    $this->config = new ImageConfig();
    $this->config->rootDir = rtrim($root_dir, "/");
    $this->config->originalDirName = $originalDirName;
    if (!is_null($stylesRootDirName)) {
      $this->config->stylesRootDirName = $stylesRootDirName;
    }
  }

  /**
   * Generate and returrn Image irl by given style name, if image olready exists and $forceGenerate is false then return without existing style image
   * @param $fileName Image name
   * @param $style_name Style name
   * @param bool $forceGenerate force generate or not
   * @return string Image url
   * @throws ImageException
   */
  public function generateAndGetImageByStyle($fileName, $style_name, $forceGenerate = false, $crop = false ){

    if(empty($this->config->settings[$style_name])){
      throw new ImageException("Given image style name is not configured in image settings");
    }
    if (!file_exists($this->config->rootDir . "/" . $this->config->originalDirName . "/" . $fileName)) {
      throw new ImageException("Image with given imagename does not exists in specified url -  " . $this->config->rootDir . "/" . $this->config->originalDirName . "/" . $fileName);
    }

    $styleUrl = $this->config->rootDir . "/";
    if(!empty($this->config->stylesRootDirName)){
      $styleUrl .= $this->config->stylesRootDirName . "/";
    }

    if(file_exists($styleUrl . $this->config->settings[$style_name]["folder"] . "/" . $fileName) && !$forceGenerate){
      return (!empty($this->config->stylesRootDirName) ? $this->config->stylesRootDirName . "/" : "") . $this->config->settings[$style_name]["folder"] . "/" . $fileName;
    }
    else {
      if($crop) {
        $this->cropImage($fileName, $style_name);
      }
      else {
        $this->resizeImage($fileName, $style_name);
      }
      
      return (!empty($this->config->stylesRootDirName) ? $this->config->stylesRootDirName . "/" : "") . $this->config->settings[$style_name]["folder"] . "/" . $fileName;
    }
  }

  /**
   * Resize image gy given style properties ans save new style image or update old one
   * @param $fileName file name
   * @param $style_name style name
   * @throws ImageException
   */
  public function resizeImage($fileName, $style_name){
    if(empty($this->config->settings[$style_name])){
      throw new ImageException("Given image style name is not configured in image settings");
    }
    if (!file_exists($this->config->rootDir . "/" . $this->config->originalDirName . "/" . $fileName)) {
      throw new ImageException("Image with given imagename does not exists in specified url -  " . $this->config->rootDir . "/" . $this->config->originalDirName . "/" . $fileName);
    }

    $styleUrl = $this->config->rootDir . "/";
    if(!empty($this->config->stylesRootDirName)){
      $styleUrl .= $this->config->stylesRootDirName . "/";
    }
    if (!is_dir($styleUrl . $this->config->settings[$style_name]["folder"])) {
      if(!is_dir($styleUrl) && !is_null($this->config->stylesRootDirName)){
        mkdir($styleUrl, 0777, true);
      }
      mkdir($styleUrl . $this->config->settings[$style_name]["folder"], 0777, true);
    }

    $imageResize = new ImageResize($this->config->rootDir . "/" . $this->config->originalDirName . "/" . $fileName);

    $style = $this->config->settings[$style_name];
    if(!empty($style['resize']['width']) && !empty($style['resize']['height'])){
      $imageResize->resize($style['resize']['width'], $style['resize']['height']);
    }
    elseif(!empty($style['resize']['width'])){
      $imageResize->resizeToWidth($style['resize']['width']);
    }
    elseif(!empty($style['resize']['height'])){
      $imageResize->resizeToHeight($style['resize']['height']);
    }
    $imageResize->save( $styleUrl . $this->config->settings[$style_name]["folder"] . "/" . $fileName);
  }
  
    /**
   * Crop image by given style properties ans save new style image or update old one
   * @param $fileName file name
   * @param $style_name style name
   * @throws ImageException
   */
  public function cropImage($fileName, $style_name) {
    if (empty($this->config->settings[$style_name])) {
      throw new ImageException("Given image style name is not configured in image settings");
    }
    if (!file_exists($this->config->rootDir . "/" . $this->config->originalDirName . "/" . $fileName)) {
      throw new ImageException("Image with given imagename does not exists in specified url -  " . $this->config->rootDir . "/" . $this->config->originalDirName . "/" . $fileName);
    }

    $styleUrl = $this->config->rootDir . "/";
    if (!empty($this->config->stylesRootDirName)) {
      $styleUrl .= $this->config->stylesRootDirName . "/";
    }
    if (!is_dir($styleUrl . $this->config->settings[$style_name]["folder"])) {
      if (!is_dir($styleUrl) && !is_null($this->config->stylesRootDirName)) {
        mkdir($styleUrl, 0777, true);
      }
      mkdir($styleUrl . $this->config->settings[$style_name]["folder"], 0777, true);
    }

    $imageResize = new ImageResize($this->config->rootDir . "/" . $this->config->originalDirName . "/" . $fileName);

    $style = $this->config->settings[$style_name];
    if (!empty($style['resize']['width']) && !empty($style['resize']['height'])) {
      $imageResize->crop($style['resize']['width'], $style['resize']['height'], true);
    }
    $imageResize->save($styleUrl . $this->config->settings[$style_name]["folder"] . "/" . $fileName);
  }

  /**
   * Delete all given image styled images.
   * @param $fileName deleting file name
   */
  public function deleteAllStyleImagesByName($fileName){
    if(!empty($this->config->settings) && is_array($this->config->settings)){
      $styleUrl = $this->config->rootDir . "/";
      if(!empty($this->config->stylesRootDirName)){
        $styleUrl .= $this->config->stylesRootDirName . "/";
      }

      foreach($this->config->settings as $key=> $setting){
        if( file_exists($styleUrl . $setting["folder"] . "/" . $fileName) ){
          unlink( $styleUrl . $setting["folder"] . "/" . $fileName );
        }
      }
    }
  }
}