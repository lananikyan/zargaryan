<?php

namespace Image;

/**
 * Class ImageConfig
 * @package Image
 */
class ImageConfig {
  /**
   * @var string url of root directory for images folders
   */
  public $rootDir = null;

  /**
   * @var string, Original images folder name
   */
  public $originalDirName = null;

  /**
   * @var string Style folders root name. If left empty then styles root directory will be used $rootDir variable
   */
  public $stylesRootDirName = null;

  /**
   * @var array(), style settings array, key = is style name, folder = style images folder name, resize = array()
   * resize => width, height reszie settings,
   * if set only width, then image will be resized by Width only,
   * if set only height, then image will be resized by height only,
   * if set width,height, then image will be resized by width and height,
   */
  public $settings = array (
       "large" => array (
          'folder' => 'large',
          'resize' => array (
            'width' => '960',
            //'height' => ''
            )
       ),
      "medium" => array (
        'folder' => 'medium',
        'resize' => array (
          'width' => '480',
          //'height' => ''
        )
      ),
      'small' => array (
        'folder' => 'small',
        'resize' => array (
          'width' => '220',
          //'height' => ''
        )
      ),
      "thumbnail" => array (
        'folder' => 'thumbnail',
        'resize' => array (
          'width' => '100',
          //'height' => '50'
        )
      ),
      "thumbnail_crop" => array(
        'folder' => 'thumbnail_crop',
        'resize' => array(
          'width' => '100',
          'height' => '100'
        )
      ),
     "medium_height" => array (
        'folder' => 'medium_height',
        'resize' => array (
          //'width' => '480',
          'height' => '650'
        )
      )
  );
}