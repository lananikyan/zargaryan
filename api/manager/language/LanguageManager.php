<?php
class LanguageManager {
	public function getCurrentLanguage(){
//var_dump( "getCurrentLanguage", $_SESSION, $_COOKIE );
		if( isset( $_SESSION[ "lang" ] ) ){
			return $_SESSION[ "lang" ];
		}
		if( isset( $_COOKIE[ "lang" ] ) ){
			return $_COOKIE[ "lang" ];
		}
		$languages = Config::getLanguages();
		if( !is_null( $languages ) ){
			return $languages[ 0 ];
		}

		return null;
	}
	
	public function setCurrentLanguage( $lang ){
//var_dump( "PRE setCurrentLanguage", $_SESSION, $_COOKIE, "*****", $lang, "*****" );
		if( !is_null( Config::getLanguages() ) || in_array($lang, Config::getLanguages()) ){
			$_SESSION[ "lang" ] = $lang;
			$_COOKIE[ "lang" ] = $lang;
			setcookie( 'lang', $lang, (time() + 60*60*24*30));
	
//var_dump( "setCurrentLanguage", $_SESSION, $_COOKIE, "&&&&&&", $lang, "&&&&&&" );

			return $lang;
		}
	
		return null;
	}
	
	private function fileToObject( $filePath ){
		$lines = @file( $filePath, FILE_IGNORE_NEW_LINES );
		if( $lines === false ){
			return null;
		}
	
		$retObj = array();
		foreach ( $lines AS $line ){
			if( strpos($line, "=") !== false ){
				$value = substr($line, (strpos($line, "=") + 1));
				$key = substr($line, 0, strpos($line, "="));
				$retObj[ addslashes( $key ) ] = str_replace('"', '\"', $value );
			}
		}
	
		return (count( $retObj ) > 0 ? $retObj : null);
	}
	
	public function getLangObject(){
		if( is_null( $this->getCurrentLanguage() ) ){
			return null;
		}
	
		return self::fileToObject( Config::getLanguagesDir() . "lang_" . $this->getCurrentLanguage() . ".txt" );
	}
	
	public function getLanguageList(){
		return Config::getLanguages();
	}
}