<?php

require_once Config::getBaseDir() . 'database_layer/tables/Article_table.php';

class ArticleManager {
	public function getWaitApproveArticleCount(){
		return Article_table::getArticlesCount( array( "approve_status" => Article_table::$STATUS__WAIT_APPROVEMENT ) );
	}
	
	public function createArticle( $data ){
		$userManager = new UserManager();
		$userData = $userManager->getCurrentUser();
		
		$data = FormatManager::UItoDB( $data, true );
		$data[ "approve_status" ] = Article_table::$STATUS__WAIT_APPROVEMENT;
		$id = Article_table::createArticle($userData[ "id" ], $data);
		
		return $id;
	}
	
	public function rejectArticle( $id ){
		Article_table::setApprovement( $id, Article_table::$STATUS__DISAPPROVE );
	}
	
	public function approveArticle( $id ){
		Article_table::setApprovement( $id, Article_table::$STATUS__APPROVE );
	}
	
	public function getArticlesShortList( $search = null ){
		$langManager = new LanguageManager();
		
		return Article_table::getArticlesShortList( $langManager->getCurrentLanguage(), $search );
	}
	
	public function getArticlesShortListForAdmin( $search = null ){
		$langManager = new LanguageManager();
	
		return Article_table::getArticlesShortList( $langManager->getCurrentLanguage(), $search, true, array(Article_table::$STATUS__APPROVE, Article_table::$STATUS__WAIT_APPROVEMENT) );
	}
	
	public function getArtcleDetail( $id ){
		$langManager = new LanguageManager();
		
		return Article_table::getArtcleDetail( $id, array( $langManager->getCurrentLanguage() ) );
	}
	
	public function getArtcleDetail_allLang( $id ){
		$langManager = new LanguageManager();
	
		return Article_table::getArtcleDetail( $id, Config::getLanguages(), $langManager->getCurrentLanguage() );
	}
}