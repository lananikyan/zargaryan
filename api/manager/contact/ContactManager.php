<?php

require_once Config::getBaseDir() . 'database_layer/tables/ContactUs_table.php';

class ContactManager {
	public function getNewMessagesCount(){
		return count( ContactUs_table::getMessages( true ) );
	}
	
	public function getNewMessages(){
		return ContactUs_table::getMessages( true );
	}
	
	public function createMessage( $data ){
		$mail             = new PHPMailer();
		$body             = eregi_replace("[\]", '', ($data[ "message" ] . " \r\n Phone: " . $data[ "phone" ]));
		//$mail->IsSMTP();
		//$mail->SMTPSecure = "ssl";
		//$mail->SMTPDebug  = 0;
		//$mail->SMTPAuth   = true;
		//$mail->Host       = "";
		//$mail->Port       = 465;
		//$mail->Username   = "";
		//$mail->Password   = '';
		$mail->CharSet  = 'UTF-8';

		$mail->SetFrom( $data[ "email" ], $data[ "name" ] );
		$mail->Subject = ( $data[ "header" ] );
		//$mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
		//$mail->MsgHTML( $body );
		$mail->Body = $body;

		$mail->AddAddress( Config::getAdminEmail() );

		$keys = array_keys( $_FILES );
		if( count( $keys ) == 1 ){
			$upM = new UploadManager();
			$fileName = $upM->saveUploadedFile( Config::getContactUsFilesDir(), $upM->generateRandomString() );
			$data[ "file" ] = Config::getContactUsFilesPath() . $fileName;

			$mail->AddAttachment( (Config::getContactUsFilesDir() . $fileName) , $_FILES[ $keys[ 0 ] ]["name"] );
		}
		ContactUs_table::setMessage( $data );

		$mail->Send();
	}

	
	public function setMessageOld( $id ){
		ContactUs_table::setOld( $id );
	}
}