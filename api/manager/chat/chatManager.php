<?php

class chatManager {
  
  private static $conn;
  
  public function __construct($conn){
    self::$conn = $conn;
  }
  
  public static function createChatroom($user_id, $type) {
    $sql = "INSERT INTO chat_rooms(author_id, type) VALUES (:user_id, :type)";
    $stmt = self::$conn->prepare($sql);
    $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
    $stmt->bindParam(':type', $type);
    $stmt->execute();
    return self::$conn->lastInsertId();
  }
  
  public static function removeChatroom($room) {
    $sql_room = "DELETE FROM chat_rooms WHERE id = :room";
    $stmt_room = self::$conn->prepare($sql_room);
    $stmt_room->bindParam(':room', $room, PDO::PARAM_INT);
    $stmt_room->execute();
    return true;
  }
  
  public static function getChatroomAuthor($room_id) {
    $sql = "SELECT author_id FROM chat_rooms WHERE id = :room_id";
    $stmt = self::$conn->prepare($sql);
    $stmt->bindParam(':room_id', $room_id, PDO::PARAM_INT);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    return $result['author_id'];
  }
  
  public static function addUsersToChatroom($room_id, $users) {
    $sql = "INSERT IGNORE INTO chat_room_users(room_id, user_id) VALUES (:room_id, :user_id)";
    $stmt = self::$conn->prepare($sql);
    $stmt->bindParam(':room_id', $room_id, PDO::PARAM_INT);
    foreach ($users as $user_id) {
      $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
      $stmt->execute();
    }
    return TRUE;
  }
  public static function removeUsersFromChatroom($room_id, $users) {
    $sql = "DELETE FROM chat_room_users WHERE room_id = :room_id AND user_id = :user_id";
    $stmt = self::$conn->prepare($sql);
    $stmt->bindParam(':room_id', $room_id, PDO::PARAM_INT);
    foreach ($users as $user_id) {
      $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
      $stmt->execute();
    }
    return TRUE;
  }

  public static function getUserChatrooms($user_id) {
    $chatrooms = array();
    $sql_rooms = "SELECT chat_rooms.* FROM chat_rooms
            LEFT JOIN chat_room_users ON chat_room_users.room_id = chat_rooms.id
            WHERE chat_room_users.user_id = :user_id";
    $stmt_rooms = self::$conn->prepare($sql_rooms);
    $stmt_rooms->bindParam(':user_id', $user_id, PDO::PARAM_INT);
    $stmt_rooms->execute();
    if($stmt_rooms->rowCount() == 0) {
      return $chatrooms;
    }
    $rooms = $stmt_rooms->fetchAll(PDO::FETCH_ASSOC);
    foreach ($rooms as $room) {
      if($room['type'] == 'user') {
        $group_user = self::getChatroomUsers($room['id'], array($user_id));
        if(empty($group_user)) {
          continue;
        }
        $user_info = self::getUserChatInfo($group_user[0]);
        $chatrooms[] = array(
          'id'   => $room['id'],
          'type' => $room['type'],
          'user_id' => $user_info['id'],
          'name' => array(
            'en' => $user_info['first_name_en'] . ' ' . $user_info['last_name_en'],
            'ru' => $user_info['first_name_ru'] . ' ' . $user_info['last_name_ru'],
            'am' => $user_info['first_name_am'] . ' ' . $user_info['last_name_am'],
          ),
          'picture' => $user_info['profile_picture'],
          'last_message' => self::getRoomLastMessage($room['id']),
          'unseen_messages' => self::getRoomUnseenMessages($user_id, $room['id']),
          'author' => $room['author_id'],
        );
      }
      else if($room['type'] == 'group') {
        $chatrooms[] = array(
          'id'   => $room['id'],
          'type' => $room['type'],
          'name' => self::getGroupChatName($room['id'], $user_id),
          'picture' => null,
          'last_message' => self::getRoomLastMessage($room['id']),
          'unseen_messages' => self::getRoomUnseenMessages($user_id, $room['id']),
          'author' => $room['author_id'],
        );
      }
    }
    
    return $chatrooms;
  }
  
  public static function getGroupChatName($room_id, $user_id) {
    $group_name = array(
      'en' => '',
      'ru' => '',
      'am' => '',
    );
    $sql = "SELECT name FROM chat_rooms
            WHERE id = :room_id";
    $stmt = self::$conn->prepare($sql);
    $stmt->bindParam(':room_id', $room_id, PDO::PARAM_INT);
    $stmt->execute();
    $room = $stmt->fetch(PDO::FETCH_ASSOC);
    if(!is_null($room['name'])) {
      $group_name = array(
        'en' => $room['name'], 
        'ru' => $room['name'], 
        'am' => $room['name']
      );
      return $group_name;
    }
    $group_users = self::getChatroomUsers($room_id, array($user_id), array('offset' => 0, 'limit' => 4));
    if(empty($group_users)) {
      $group_name = 'only_you';
      return $group_name;
    }
    $user_count = count($group_users);
    for($i = 0; $i < $user_count; $i++) {
      if ($i > 2) {
        break;
      }
      if ($i !== 0) {
        $group_name['en'] .= ', ';
        $group_name['ru'] .= ', ';
        $group_name['am'] .= ', ';
      }
      $user_info = self::getUserChatInfo($group_users[$i]);
      $group_name['en'] .= $user_info['first_name_en'] . ' ' . $user_info['last_name_en'];
      $group_name['ru'] .= $user_info['first_name_ru'] . ' ' . $user_info['last_name_ru'];
      $group_name['am'] .= $user_info['first_name_am'] . ' ' . $user_info['last_name_am'];
    }
    if(count($group_users) > 2) {
      $group_name['en'] .= '...';
      $group_name['ru'] .= '...';
      $group_name['am'] .= '...';
    }
    return $group_name;
  }
  
  public static function setGroupChatName($room_id, $name) {
    $sql = "UPDATE chat_rooms
            SET name = :name
            WHERE id = :room_id";
    $stmt = self::$conn->prepare($sql);
    $stmt->bindParam(':room_id', $room_id, PDO::PARAM_INT);
    $stmt->bindParam(':name', $name);
    $stmt->execute();
    return $name;
  }
  
  public static function getUserChannels($user_id) {
    $chatrooms = array();
    $sql_rooms = "SELECT room_id FROM chat_room_users
                  WHERE user_id = :user_id";
    $stmt_rooms = self::$conn->prepare($sql_rooms);
    $stmt_rooms->bindParam(':user_id', $user_id, PDO::PARAM_INT);
    $stmt_rooms->execute();
    while($row = $stmt_rooms->fetch(PDO::FETCH_ASSOC)) {
      $chatrooms[] = 'room_' . $row['room_id'];
    }
    return $chatrooms;
  }
  
  public static function getUserChatInfo($user_id) {
    $sql = "SELECT user.id, user.profile_picture,
            user_info_am.first_name AS first_name_am, user_info_am.last_name AS last_name_am,
            user_info_ru.first_name AS first_name_ru, user_info_ru.last_name AS last_name_ru,
            user_info_en.first_name AS first_name_en, user_info_en.last_name AS last_name_en
            FROM user
            LEFT JOIN user_info_am ON user_info_am.user_id = user.id
            LEFT JOIN user_info_ru ON user_info_ru.user_id = user.id
            LEFT JOIN user_info_en ON user_info_en.user_id = user.id
            WHERE user.id = :user_id
            GROUP BY user.id";
    $stmt = self::$conn->prepare($sql);
    $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    return $result;
  }
  
  public static function getRoomType($room_id) {
    $sql = 'SELECT type
            FROM chat_rooms
            WHERE id = :room_id';
    $stmt = self::$conn->prepare($sql);
    $stmt->bindParam(':room_id', $room_id, PDO::PARAM_INT);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    return $result['type'];
  }
  
  public static function getRoomLastMessage($room_id) {
    $sql = 'SELECT DISTINCT message, timestamp
            FROM chat_msg
            WHERE room_id = :room_id
            ORDER BY timestamp DESC';
    $stmt = self::$conn->prepare($sql);
    $stmt->bindParam(':room_id', $room_id, PDO::PARAM_INT);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    return $result;
  }
  
  public static function setRoomMessagesSeen($user_id, $room_id) {
    $sql = 'UPDATE chat_msg_seen
            LEFT JOIN chat_msg ON chat_msg.id = chat_msg_seen.msg_id
            SET chat_msg_seen.seen = 1
            WHERE chat_msg_seen.user_id = :user_id AND chat_msg.room_id = :room_id';
    $stmt = self::$conn->prepare($sql);
    $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
    $stmt->bindParam(':room_id', $room_id, PDO::PARAM_INT);
    $stmt->execute();
    $result = $stmt->rowCount();
    return $result;
  }
  
  public static function setMessageSeen($user_id, $message_id) {
    $sql = 'UPDATE chat_msg_seen
            SET seen = 1
            WHERE user_id = :user_id AND msg_id = :message_id';
    $stmt = self::$conn->prepare($sql);
    $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
    $stmt->bindParam(':message_id', $message_id, PDO::PARAM_INT);
    $stmt->execute();
    $result = $stmt->rowCount();
    return $result;
  }
  
  public static function getRoomUnseenMessages($user_id, $room_id) {
    $sql = 'SELECT chat_msg.id
            FROM chat_msg_seen AS msg_seen
            LEFT JOIN chat_msg ON chat_msg.id = msg_seen.msg_id
            WHERE msg_seen.user_id = :user_id AND chat_msg.room_id = :room_id AND msg_seen.seen = 0';
    $stmt = self::$conn->prepare($sql);
    $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
    $stmt->bindParam(':room_id', $room_id, PDO::PARAM_INT);
    $stmt->execute();
    $result = $stmt->rowCount();
    return $result;
  }
  
  public static function getUserUnseenMessages($user_id) {
    $sql = 'SELECT chat_msg.id
            FROM chat_msg_seen AS msg_seen
            LEFT JOIN chat_msg ON chat_msg.id = msg_seen.msg_id
            WHERE msg_seen.user_id = :user_id AND msg_seen.seen = 0';
    $stmt = self::$conn->prepare($sql);
    $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
    $stmt->execute();
    $result = $stmt->rowCount();
    return $result;
  }
  
  public static function getChatroomUsers($room_id, $exclude_users = array(), $limit = array()) {
    $result = array();
    $sql = "SELECT user_id FROM chat_room_users WHERE room_id = :room_id";
    if(!empty($exclude_users)) {
      $sql .=' AND user_id NOT IN('. implode($exclude_users) .')';
    }
    if(!empty($limit)) {
      $sql .=' LIMIT :offset, :limit';
    }
    $stmt = self::$conn->prepare($sql);
    $stmt->bindParam(':room_id', $room_id, PDO::PARAM_INT);
    if(!empty($limit)) {
      $stmt->bindParam(':offset', $limit['offset'], PDO::PARAM_INT);
      $stmt->bindParam(':limit', $limit['limit'], PDO::PARAM_INT);
    }
    $stmt->execute();
    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $result[] = $row['user_id'];
    }
    return $result;
  }
  
  public static function getChatGroupMembers($room_id, $exclude_users = array()) {
    $result = array();
    $sql = "SELECT chu.user_id,
            user_info_am.first_name AS first_name_am, user_info_am.last_name AS last_name_am,
            user_info_ru.first_name AS first_name_ru, user_info_ru.last_name AS last_name_ru,
            user_info_en.first_name AS first_name_en, user_info_en.last_name AS last_name_en
            FROM chat_room_users AS chu
            LEFT JOIN user_info_am ON user_info_am.user_id = chu.user_id
            LEFT JOIN user_info_ru ON user_info_ru.user_id = chu.user_id
            LEFT JOIN user_info_en ON user_info_en.user_id = chu.user_id
            WHERE chu.room_id = :room_id";
    if(!empty($exclude_users)) {
      $sql .=' AND chu.user_id NOT IN('. implode($exclude_users) .')';
    }
    $stmt = self::$conn->prepare($sql);
    $stmt->bindParam(':room_id', $room_id, PDO::PARAM_INT);
    $stmt->execute();
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $result[] = array(
        'id' => $row['user_id'],
        'name' => array(
          'en' => $row['first_name_en'] . ' ' . $row['last_name_en'],
          'ru' => $row['first_name_ru'] . ' ' . $row['last_name_ru'],
          'am' => $row['first_name_am'] . ' ' . $row['last_name_am'],
        ),
      );
    }
    return $result;
  }
  
  public static function getChatroomHistory($room_id, $offset, $limit) {
    $sql = "SELECT chat_msg.*, msg_seen.seen
            FROM chat_msg
            LEFT JOIN chat_msg_seen AS msg_seen ON chat_msg.id = msg_seen.msg_id
            WHERE chat_msg.room_id = :room_id
            GROUP BY chat_msg.id
            ORDER BY chat_msg.timestamp DESC
            LIMIT :offset, :limit";
    $stmt = self::$conn->prepare($sql);
    $stmt->bindParam(':room_id', $room_id, PDO::PARAM_INT);
    $stmt->bindParam(':offset', $offset, PDO::PARAM_INT);
    $stmt->bindParam(':limit', $limit, PDO::PARAM_INT);
    $stmt->execute();
    $result = $stmt->fetchALL(PDO::FETCH_ASSOC);
    foreach($result as &$message) {
      $user_info = self::getUserChatInfo($message['user_id']);
      $message['user_info'] = array(
        'name' => array(
          'en' => $user_info['first_name_en'] . ' ' . $user_info['last_name_en'],
          'ru' => $user_info['first_name_ru'] . ' ' . $user_info['last_name_ru'],
          'am' => $user_info['first_name_am'] . ' ' . $user_info['last_name_am'],
        ),
        'picture' => $user_info['profile_picture'],
      );
    }
    return  array_reverse($result);
  }
  
  public static function saveMessage($user_id, $room, $message, $timestamp) {
    $sql = "INSERT INTO chat_msg(user_id, room_id, message, timestamp)
            VALUES (:user_id, :room_id, :message, :timestamp)";
    $stmt = self::$conn->prepare($sql);
    $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
    $stmt->bindParam(':room_id', $room, PDO::PARAM_INT);
    $stmt->bindParam(':message', $message, PDO::PARAM_STR);
    $stmt->bindParam(':timestamp', $timestamp, PDO::PARAM_STR);
    $stmt->execute();
    return self::$conn->lastInsertId();
  }
  
  public static function markMessageUnseen($message_id, $room_id, $sender) {
    $sql = "INSERT IGNORE INTO chat_msg_seen(msg_id, user_id, seen) VALUES (:msg_id, :user_id, 0)";
    $stmt = self::$conn->prepare($sql);
    $stmt->bindParam(':msg_id', $message_id, PDO::PARAM_INT);
    $group_users = self::getChatroomUsers($room_id, array($sender));
    foreach ($group_users as $user) {
      $stmt->bindParam(':user_id', $user, PDO::PARAM_INT);
      $stmt->execute();
    }
    return TRUE;
  }
  
  public static function sendMessage($sender, $room, $message, $timestamp) {
    $message_id = self::saveMessage($sender, $room, $message, $timestamp);
    self::markMessageUnseen($message_id, $room, $sender);
    return $message_id;
  }
  
  public static function sendFirstMessage($sender, $recipient, $message, $timestamp) {
    $sql = "SELECT chat_rooms.id
            FROM chat_rooms
            LEFT JOIN chat_room_users as room_users ON chat_rooms.id = room_users.room_id
            WHERE chat_rooms.type = 'user'
            GROUP BY room_users.room_id
            HAVING SUM(room_users.user_id IN (:sender, :recipient)) = COUNT(*)
            AND COUNT(DISTINCT room_users.user_id) = 2";
    $stmt = self::$conn->prepare($sql);
    $stmt->bindParam(':sender', $sender, PDO::PARAM_INT);
    $stmt->bindParam(':recipient', $recipient, PDO::PARAM_INT);
    $stmt->execute();
    if($stmt->rowCount() > 0) {
      $room_id = $stmt->fetch(PDO::FETCH_ASSOC)['id'];
    }
    else {
      $room_id = self::createChatroom($sender, 'user');
      self::addUsersToChatroom($room_id, array($sender, $recipient));
    }
    $message_id = self::saveMessage($sender, $room_id, $message, $timestamp);
    self::markMessageUnseen($message_id, $room_id, $sender);
    return array(
      'message_id' => $message_id,
      'room_id' => $room_id,
    );
  }
  
  public static function getUsersForGroup($room, $select_type) {
    $user_list = array();
    $sql = "SELECT user.id, user.profile_picture,
            user_info_am.first_name AS first_name_am, user_info_am.last_name AS last_name_am,
            user_info_ru.first_name AS first_name_ru, user_info_ru.last_name AS last_name_ru,
            user_info_en.first_name AS first_name_en, user_info_en.last_name AS last_name_en
            FROM user
            LEFT JOIN user_info_am ON user_info_am.user_id = user.id
            LEFT JOIN user_info_ru ON user_info_ru.user_id = user.id
            LEFT JOIN user_info_en ON user_info_en.user_id = user.id
            WHERE user.dead_date IS NULL";
    switch ($select_type) {
      case 'add';
        $room_users = self::getChatroomUsers($room);
        $sql .= " AND user.id NOT IN(" . implode(',', $room_users) . ")";
        break;
      case 'remove';
        $room_users = self::getChatroomUsers($room);
        $sql .= " AND user.id IN(" . implode(',', $room_users) . ")";
        break;
    }
    $sql .= " GROUP BY user.id ORDER BY first_name_en";
    $stmt = self::$conn->prepare($sql);
    $stmt->execute();
    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $user_list[] = array(
        'id' => $row['id'],
        'name' => array(
          'am' => $row['first_name_am'] . ' ' . $row['last_name_am'],
          'ru' => $row['first_name_ru'] . ' ' . $row['last_name_ru'],
          'en' => $row['first_name_en'] . ' ' . $row['last_name_en'],
        ),
      );
    }
    return $user_list;
  }  
}