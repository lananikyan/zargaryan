<?php

define('NODEJS_SERVER_VERSION', '1.0.9');

require_once 'Config.php';
require_once 'ConfigLocal.php';

class Nodejs {

  public static $messages = array();
  public static $config = NULL;
  public static $nodeServerVersion = NULL;


  public static function getMessages() {
    return self::$messages;
  }

  public static function enqueueMessage(StdClass $message) {
    self::$messages[] = $message;
  }

  public static function sendMessages() {
    foreach (self::$messages as $message) {
      self::sendMessage($message);
    }
  }

  public static function getServerVersion() {
    if (isset(self::$nodeServerVersion)) {
      return self::$nodeServerVersion;
    }

    $data = self::healthCheck();
    if (!isset($data->version)) {
      self::$nodeServerVersion = FALSE;
    } else {
      self::$nodeServerVersion = $data->version;
    }

    return self::$nodeServerVersion;
  }

  public static function checkServerVersion() {
    $server_version = self::getServerVersion();

    if (!$server_version) {
      // Version number is missing. Assume incompatibility.
      return FALSE;
    }

    $current_parts = explode('.', $server_version);
    $required_parts = explode('.', NODEJS_SERVER_VERSION);
    $current_major = reset($current_parts);
    $required_major = reset($required_parts);

    if ($current_major != $required_major || version_compare($server_version, NODEJS_SERVER_VERSION) < 0) {
      return FALSE;
    }

    return TRUE;
  }

  public static function sendMessage(StdClass $message) {
    $message->clientSocketId = nodejs_get_client_socket_id();
    $options = array(
        'method' => 'POST',
        'data' => json_encode($message),
        'timeout' => 5,
    );
    return self::httpRequest('nodejs/publish', $options);
  }

  public static function healthCheck() {
    $data = self::httpRequest('nodejs/health/check', array(), TRUE);
    return $data;
  }

  public static function setUserPresenceList($uid, array $uids) {
    return self::httpRequest("nodejs/user/presence-list/$uid/" . implode(',', $uids));
  }

  public static function logoutUser($token) {
    $options = array(
        'method' => 'POST',
    );
    return self::httpRequest("nodejs/user/logout/$token", $options);
  }

  public static function sendContentTokenMessage($message) {
    $message->clientSocketId = nodejs_get_client_socket_id();
    $options = array(
        'method' => 'POST',
        'data' => json_encode($message),
        'options' => array('timeout' => 5.0),
    );
    return self::httpRequest('nodejs/content/token/message', $options);
  }

  public static function sendContentToken($message) {
    $options = array(
        'method' => 'POST',
        'data' => json_encode($message),
    );
    return self::httpRequest('nodejs/content/token', $options);
  }

  public static function getContentTokenUsers($message) {
    $options = array(
        'method' => 'POST',
        'data' => json_encode($message),
    );
    return self::httpRequest('nodejs/content/token/users', $options);
  }

  public static function kickUser($uid) {
    $options = array(
        'method' => 'POST',
    );
    return self::httpRequest("nodejs/user/kick/$uid", $options);
  }

  public static function addUserToChannel($uid, $channel) {
    $options = array(
        'method' => 'POST',
    );
    return self::httpRequest("nodejs/user/channel/add/$channel/$uid", $options);
  }

  public static function removeUserFromChannel($uid, $channel) {
    $options = array(
        'method' => 'POST',
    );
    return self::httpRequest("nodejs/user/channel/remove/$channel/$uid", $options);
  }

  public static function addChannel($channel) {
    $options = array(
        'method' => 'POST',
    );
    return self::httpRequest("nodejs/channel/add/$channel", $options);
  }

  public static function checkChannel($channel) {
    return self::httpRequest("nodejs/channel/check/$channel");
  }

  public static function removeChannel($channel) {
    $options = array(
        'method' => 'POST',
    );
    return self::httpRequest("nodejs/channel/remove/$channel", $options);
  }

  public static function httpRequest($url, $options = array(), $ignore_version = FALSE) {

    if (!$ignore_version && !self::checkServerVersion()) {
      return FALSE;
    }

    $options += array(
        'method' => 'GET',
        'headers' => array(),
    );
    $options['headers'] += array(
        'NodejsServiceKey' => Config::getNodejsKey(),
        'Content-type' => 'application/json',
    );

    $response = self::http_request(Config::getNodejsHost() . $url, $options);
    // If a http error occurred, and logging of http errors is enabled, log it.
    if (isset($response->error)) {
      if (true) {
        $params = array(
            '%code' => $response->code,
            '%error' => $response->error,
            '%url' => $url,
        );
        $log_message = 'Error reaching the Node.js server at "%url": [%code] %error.';
        if (!empty($options['data'])) {
          $params['data'] = $options['data'];
          $log_message = 'Error reaching the Node.js server at "%url" with data "%data": [%code] %error.';
        }
      }
      return FALSE;
    }
    // No errors, so return Node.js server response.
    return json_decode($response->data);
  }

  public static function http_request($url, array $options = array()) {

    $result = new stdClass();

    // Parse the URL and make sure we can handle the schema.
    $uri = @parse_url($url);

    if ($uri == FALSE) {
      $result->error = 'unable to parse URL';
      $result->code = -1001;
      return $result;
    }

    if (!isset($uri['scheme'])) {
      $result->error = 'missing schema';
      $result->code = -1002;
      return $result;
    }

    timer_start(__FUNCTION__);

    // Merge the default options.
    $options += array(
        'headers' => array(),
        'method' => 'GET',
        'data' => NULL,
        'max_redirects' => 3,
        'timeout' => 30.0,
        'context' => NULL,
    );

    // stream_socket_client() requires timeout to be a float.
    $options['timeout'] = (float) $options['timeout'];

    switch ($uri['scheme']) {
      case 'proxy':
        // Make the socket connection to a proxy server.
        $socket = 'tcp://' . $proxy_server . ':' . variable_get('proxy_port', 8080);
        // The Host header still needs to match the real request.
        $options['headers']['Host'] = $uri['host'];
        $options['headers']['Host'] .= isset($uri['port']) && $uri['port'] != 80 ? ':' . $uri['port'] : '';
        break;

      case 'http':
      case 'feed':
        $port = isset($uri['port']) ? $uri['port'] : 80;
        $socket = 'tcp://' . $uri['host'] . ':' . $port;
        // RFC 2616: "non-standard ports MUST, default ports MAY be included".
        // We don't add the standard port to prevent from breaking rewrite rules
        // checking the host that do not take into account the port number.
        $options['headers']['Host'] = $uri['host'] . ($port != 80 ? ':' . $port : '');
        break;

      case 'https':
        // Note: Only works when PHP is compiled with OpenSSL support.
        $port = isset($uri['port']) ? $uri['port'] : 443;
        $socket = 'ssl://' . $uri['host'] . ':' . $port;
        $options['headers']['Host'] = $uri['host'] . ($port != 443 ? ':' . $port : '');
        break;

      default:
        $result->error = 'invalid schema ' . $uri['scheme'];
        $result->code = -1003;
        return $result;
    }

    if (empty($options['context'])) {
      $fp = @stream_socket_client($socket, $errno, $errstr, $options['timeout']);
    } else {
      // Create a stream with context. Allows verification of a SSL certificate.
      $fp = @stream_socket_client($socket, $errno, $errstr, $options['timeout'], STREAM_CLIENT_CONNECT, $options['context']);
    }

    // Make sure the socket opened properly.
    if (!$fp) {
      // When a network error occurs, we use a negative number so it does not
      // clash with the HTTP status codes.
      $result->code = -$errno;
      $result->error = trim($errstr) ? trim($errstr) : t('Error opening socket @socket', array('@socket' => $socket));

      // Mark that this request failed. This will trigger a check of the web
      // server's ability to make outgoing HTTP requests the next time that
      // requirements checking is performed.
      // See system_requirements().
      
      return $result;
    }

    // Construct the path to act on.
    $path = isset($uri['path']) ? $uri['path'] : '/';
    if (isset($uri['query'])) {
      $path .= '?' . $uri['query'];
    }

    // Only add Content-Length if we actually have any content or if it is a POST
    // or PUT request. Some non-standard servers get confused by Content-Length in
    // at least HEAD/GET requests, and Squid always requires Content-Length in
    // POST/PUT requests.
    $content_length = strlen($options['data']);
    if ($content_length > 0 || $options['method'] == 'POST' || $options['method'] == 'PUT') {
      $options['headers']['Content-Length'] = $content_length;
    }

    // If the server URL has a user then attempt to use basic authentication.
    if (isset($uri['user'])) {
      $options['headers']['Authorization'] = 'Basic ' . base64_encode($uri['user'] . (isset($uri['pass']) ? ':' . $uri['pass'] : ':'));
    }

    $request = $options['method'] . ' ' . $path . " HTTP/1.0\r\n";
    foreach ($options['headers'] as $name => $value) {
      $request .= $name . ': ' . trim($value) . "\r\n";
    }
    $request .= "\r\n" . $options['data'];
    $result->request = $request;
    // Calculate how much time is left of the original timeout value.
    $timeout = $options['timeout'] - timer_read(__FUNCTION__) / 1000;
    if ($timeout > 0) {
      stream_set_timeout($fp, floor($timeout), floor(1000000 * fmod($timeout, 1)));
      fwrite($fp, $request);
    }

    // Fetch response. Due to PHP bugs like http://bugs.php.net/bug.php?id=43782
    // and http://bugs.php.net/bug.php?id=46049 we can't rely on feof(), but
    // instead must invoke stream_get_meta_data() each iteration.
    $info = stream_get_meta_data($fp);
    $alive = !$info['eof'] && !$info['timed_out'];
    $response = '';

    while ($alive) {
      // Calculate how much time is left of the original timeout value.
      $timeout = $options['timeout'] - timer_read(__FUNCTION__) / 1000;
      if ($timeout <= 0) {
        $info['timed_out'] = TRUE;
        break;
      }
      stream_set_timeout($fp, floor($timeout), floor(1000000 * fmod($timeout, 1)));
      $chunk = fread($fp, 1024);
      $response .= $chunk;
      $info = stream_get_meta_data($fp);
      $alive = !$info['eof'] && !$info['timed_out'] && $chunk;
    }
    fclose($fp);

    if ($info['timed_out']) {
      $result->code = HTTP_REQUEST_TIMEOUT;
      $result->error = 'request timed out';
      return $result;
    }
    // Parse response headers from the response body.
    // Be tolerant of malformed HTTP responses that separate header and body with
    // \n\n or \r\r instead of \r\n\r\n.
    list($response, $result->data) = preg_split("/\r\n\r\n|\n\n|\r\r/", $response, 2);
    $response = preg_split("/\r\n|\n|\r/", $response);

    // Parse the response status line.
    $response_status_array = self::parse_response_status(trim(array_shift($response)));
    $result->protocol = $response_status_array['http_version'];
    $result->status_message = $response_status_array['reason_phrase'];
    $code = $response_status_array['response_code'];

    $result->headers = array();

    // Parse the response headers.
    while ($line = trim(array_shift($response))) {
      list($name, $value) = explode(':', $line, 2);
      $name = strtolower($name);
      if (isset($result->headers[$name]) && $name == 'set-cookie') {
        // RFC 2109: the Set-Cookie response header comprises the token Set-
        // Cookie:, followed by a comma-separated list of one or more cookies.
        $result->headers[$name] .= ',' . trim($value);
      } else {
        $result->headers[$name] = trim($value);
      }
    }

    $responses = array(
        100 => 'Continue',
        101 => 'Switching Protocols',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        307 => 'Temporary Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Time-out',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Large',
        415 => 'Unsupported Media Type',
        416 => 'Requested range not satisfiable',
        417 => 'Expectation Failed',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Time-out',
        505 => 'HTTP Version not supported',
    );
    // RFC 2616 states that all unknown HTTP codes must be treated the same as the
    // base code in their class.
    if (!isset($responses[$code])) {
      $code = floor($code / 100) * 100;
    }
    $result->code = $code;

    switch ($code) {
      case 200: // OK
      case 201: // Created
      case 202: // Accepted
      case 203: // Non-Authoritative Information
      case 204: // No Content
      case 205: // Reset Content
      case 206: // Partial Content
      case 304: // Not modified
        break;
      case 301: // Moved permanently
      case 302: // Moved temporarily
      case 307: // Moved temporarily
        $location = $result->headers['location'];
        $options['timeout'] -= timer_read(__FUNCTION__) / 1000;
        if ($options['timeout'] <= 0) {
          $result->code = HTTP_REQUEST_TIMEOUT;
          $result->error = 'request timed out';
        } elseif ($options['max_redirects']) {
          // Redirect to the new location.
          $options['max_redirects'] --;
          $result = drupal_http_request($location, $options);
          $result->redirect_code = $code;
        }
        if (!isset($result->redirect_url)) {
          $result->redirect_url = $location;
        }
        break;
      default:
        $result->error = $result->status_message;
    }

    return $result;
  }

  public static function parse_response_status($response) {
    $response_array = explode(' ', trim($response), 3);
    // Set up empty values.
    $result = array(
        'reason_phrase' => '',
    );
    $result['http_version'] = $response_array[0];
    $result['response_code'] = $response_array[1];
    if (isset($response_array[2])) {
      $result['reason_phrase'] = $response_array[2];
    }
    return $result;
  }

}


/**
 * Get the client socket id associated with this request.
 */
function nodejs_get_client_socket_id() {
  $client_socket_id = isset($_POST['nodejs_client_socket_id']) ? $_POST['nodejs_client_socket_id'] : '';
  return preg_match('/^[0-9a-z_-]+$/i', $client_socket_id) ? $client_socket_id : '';
}
/**
 * Starts the timer with the specified name.
 *
 * If you start and stop the same timer multiple times, the measured intervals
 * will be accumulated.
 *
 * @param $name
 *   The name of the timer.
 */
function timer_start($name) {
  global $timers;

  $timers[$name]['start'] = microtime(TRUE);
  $timers[$name]['count'] = isset($timers[$name]['count']) ? ++$timers[$name]['count'] : 1;
}

/**
 * Reads the current timer value without stopping the timer.
 *
 * @param $name
 *   The name of the timer.
 *
 * @return
 *   The current timer value in ms.
 */
function timer_read($name) {
  global $timers;

  if (isset($timers[$name]['start'])) {
    $stop = microtime(TRUE);
    $diff = round(($stop - $timers[$name]['start']) * 1000, 2);

    if (isset($timers[$name]['time'])) {
      $diff += $timers[$name]['time'];
    }
    return $diff;
  }
  return $timers[$name]['time'];
}