<?php

require_once '../../Config.php';
require_once '../../ConfigLocal.php';
require_once '../../database_layer/DBconnection.php';
require_once 'chatManager.php';
require_once 'httpHandler.php';


if (basename(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)) == 'message') {
  nodejs_message_handler();
}

/**
 * Menu callback: handles all messages from Node.js server.
 */
function nodejs_message_handler() {
  if (!isset($_POST['serviceKey']) || !nodejs_is_valid_service_key($_POST['serviceKey'])) {
    echo json_encode(array('error' => 'Invalid service key.'));
    exit();
  }

  if (!isset($_POST['messageJson'])) {
    echo json_encode(array('error' => 'No message.'));
    exit();
  }

  $message = json_decode($_POST['messageJson'], true);

  $response = array();
  switch ($message['messageType']) {
    case 'authenticate':
      $response = nodejs_auth_check($message);
      break;

    case 'userOffline':
      if (empty($message['uid'])) {
        $response['error'] = 'Missing uid for userOffline message.';
      } else if (!preg_match('/^\d+$/', $message['uid'])) {
        $response['error'] = 'Invalid (!/^\d+$/) uid for userOffline message.';
      } else {
        $response['message'] = "User {$message['uid']} set offline.";
      }
      break;
  }

  echo json_encode($response ? $response : array('error' => 'Not implemented'));
  exit();
}

/**
 * Check if the given service key is valid.
 */
function nodejs_is_valid_service_key($service_key) {
  return $service_key == Config::getNodejsKey();
}

/**
 * Checks the given key to see if it matches a valid session.
 */
function nodejs_auth_check($message) {
  $uid = nodejs_auth_check_callback($message['authToken']);
  $auth_user = $uid > 0 ? (object) array('uid' => $uid) : (object) array();
  $auth_user->authToken = $message['authToken'];
  $auth_user->nodejsValidAuthToken = $uid !== FALSE;
  $auth_user->clientId = $message['clientId'];

  if ($auth_user->nodejsValidAuthToken) {
    $db = new DBconnection();
    $chat = new chatManager($db->getConnection());
    $auth_user->channels = $chat->getUserChannels($uid);
    $auth_user->serviceKey = Config::getNodejsKey();
    $http = new httpHandler();
    $http::add_http_header('NodejsServiceKey', Config::getNodejsKey());
    $auth_user->contentTokens = isset($message['contentTokens']) ? $message['contentTokens'] : array();
  }
  return $auth_user;
}

/**
 * Default Node.js auth check callback implementation.
 */
function nodejs_auth_check_callback($auth_token) {
  $db = new DBconnection();
  $conn = $db->getConnection();
  $sql = 'SELECT user_id FROM user_sessions WHERE sid = :sid';
  $stmt = $conn->prepare($sql);
  $stmt->bindParam(':sid', $auth_token);
  $stmt->execute();
  if($stmt->rowCount() > 0) {
    return $stmt->fetch(PDO::FETCH_ASSOC)['user_id'];
  }
  return 0;
}
