<?php

class httpHandler {

  public static function send_headers($default_headers = array(), $only_default = FALSE) {
    $headers_sent = &_static(__FUNCTION__, FALSE);
    $headers = self::get_http_header();
    if ($only_default && $headers_sent) {
      $headers = array();
    }
    $headers_sent = TRUE;

    $header_names = self::_set_preferred_header_name();
    foreach ($default_headers as $name => $value) {
      $name_lower = strtolower($name);
      if (!isset($headers[$name_lower])) {
        $headers[$name_lower] = $value;
        $header_names[$name_lower] = $name;
      }
    }
    foreach ($headers as $name_lower => $value) {
      if ($name_lower == 'status') {
        header($_SERVER['SERVER_PROTOCOL'] . ' ' . $value);
      }
      // Skip headers that have been unset.
      elseif ($value !== FALSE) {
        header($header_names[$name_lower] . ': ' . $value);
      }
    }
  }

  public static function get_http_header($name = NULL) {
    $headers = &_static('http_headers', array());
    if (isset($name)) {
      $name = strtolower($name);
      return isset($headers[$name]) ? $headers[$name] : NULL;
    } else {
      return $headers;
    }
  }

  public static function add_http_header($name, $value, $append = FALSE) {
    // The headers as name/value pairs.
    $headers = &_static('http_headers', array());

    $name_lower = strtolower($name);
    self::_set_preferred_header_name($name);

    if ($value === FALSE) {
      $headers[$name_lower] = FALSE;
    } elseif (isset($headers[$name_lower]) && $append) {
      // Multiple headers with identical names may be combined using comma (RFC
      // 2616, section 4.2).
      $headers[$name_lower] .= ',' . $value;
    } else {
      $headers[$name_lower] = $value;
    }
    self::send_headers(array($name => $headers[$name_lower]), TRUE);
  }

  public static function _set_preferred_header_name($name = NULL) {
    static $header_names = array();

    if (!isset($name)) {
      return $header_names;
    }
    $header_names[strtolower($name)] = $name;
  }
}

function &_static($name, $default_value = NULL, $reset = FALSE) {
    static $data = array(), $default = array();
    // First check if dealing with a previously defined static variable.
    if (isset($data[$name]) || array_key_exists($name, $data)) {
      // Non-NULL $name and both $data[$name] and $default[$name] statics exist.
      if ($reset) {
        // Reset pre-existing static variable to its default value.
        $data[$name] = $default[$name];
      }
      return $data[$name];
    }
    // Neither $data[$name] nor $default[$name] static variables exist.
    if (isset($name)) {
      if ($reset) {
        // Reset was called before a default is set and yet a variable must be
        // returned.
        return $data;
      }
      // First call with new non-NULL $name. Initialize a new static variable.
      $default[$name] = $data[$name] = $default_value;
      return $data[$name];
    }
    // Reset all: ($name == NULL). This needs to be done one at a time so that
    // references returned by earlier invocations of _static() also get
    // reset.
    foreach ($default as $name => $value) {
      $data[$name] = $value;
    }
    // As the function returns a reference, the return should always be a
    // variable.
    return $data;
  }