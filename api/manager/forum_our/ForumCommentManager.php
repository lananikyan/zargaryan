<?php

require_once Config::getBaseDir() . 'database_layer/tables/ForumComment_table.php';

class ForumCommentManager {

	
	public function rejectComment( $id ){
		ForumComment_table::rejectComment( $id);
	}

	public function approveComment( $id ){
		ForumComment_table::approveComment( $id);
	}

	public function createComment( $data){
		require_once Config::getBaseDir() . 'manager/user/UserManager.php';
		$userManager = new UserManager();
    	$userData = $userManager->getCurrentUser();

	    $data->author_id  = $userData["id"];
	    //$data->approve_status  = 0;
		  $data->approve_status  = 1;
	    $data->creation_date  = time();

		ForumComment_table::createComment( $data);
	}
	
	public function getForumComment( $id){
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';

   		$langManager = new LanguageManager();

		return ForumComment_table::getForumComment( $id, $langManager->getCurrentLanguage());
	}
	
	public function getForumCommentAdmin(  ){
		return ForumComment_table::getForumCommentAdmin( );
	}

		public function forumCommentCountAdmin() {
    		return ForumComment_table::forumCommentCountAdmin( );

    	}

}