<?php

require_once Config::getBaseDir() . 'database_layer/tables/Forum_table.php';

class Forum_OurManager {

	
	public function createForum( $data ){

		//$data->approve_status = 0;
		$data->approve_status = 1;
		$id = Forum_table::createForum($data);
		
		return $id;
	}
	
	public function rejectForum( $id ){
		Forum_table::rejectForum( $id);
	}
	public function approveForum( $id ){
		Forum_table::approveForum( $id);
	}

	public function updateForumTitle( $id, $title ){
		Forum_table::updateForumTitle( $id, $title);
	}
	
	public function getForum( ){
		return Forum_table::getForum( );
	}
	
	public function getForumAdmin(  ){
		return Forum_table::getForumAdmin( );
	}


	public function forumCountAdmin() {
    		return Forum_table::forumCountAdmin( );

    	}
}