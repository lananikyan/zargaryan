<?php
require_once Config::getBaseDir() . 'database_layer/tables/MainInfo_table.php';

class MainPageManager {
	public function getInfoForUser( $id = null, $type = 0 ){

		$langManager = new LanguageManager();
		$data = MainInfo_table::getInfo( array( $langManager->getCurrentLanguage() ), $id );
		
		if( $type == 0 ){
			for( $i = 0; $i < count( $data ); $i++ ){
				unset( $data[ $i ][ "content" ] );
			}
		}else
		if( $type == 1 ){
			for( $i = 0; $i < count( $data ); $i++ ){
				unset( $data[ $i ][ "short_content" ] );
			}
		}
		
		return $data;
	}
	
	public function getInfoForAdmin( $id = null, $type = null ){
		$data = MainInfo_table::getInfo( Config::getLanguages(), $id );
		$data = $data[ 0 ];
			
		return FormatManager::DBtoUI( $data );
	}
	
	public function updateInfoFromAdmin( $data, $id = null ){
		if ( is_null( $id ) ){
			$id = $data[ "id" ];
			unset( $data[ "id" ] );
		}
		
		$data = FormatManager::UItoDB( $data );
		MainInfo_table::updateInfo($id, $data);
	}
}