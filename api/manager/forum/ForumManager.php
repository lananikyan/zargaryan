<?php
//require_once dirname( dirname( dirname(__FILE__) ) ) . "/lib/phpBB3/MyForumApi.php";

class ForumManager {
	public function getNotificationsCount(){
		$this->loginUser();
		
		$forumApi = new MyForumApi();		
		return $forumApi->getUnreadedNotificationsCount();
	}
	
	
	public function createUser( $login, $pass, $email, $first_name, $last_name ){
		$forumApi = new MyForumApi();
		
		return $forumApi->createUser($login, $pass, $email, ($first_name . " " . $last_name));
	}
	
	public function createUserAvatar( $login, $uploadManager = null ){
		if( is_null( $uploadManager ) ){
			$uploadManager = new UploadManager();
		}
		
		if( $uploadManager->isFileExistForUpload() ){
			$userData = PhpbbUsers_table::getData( array( "username_clean" => strtolower( $login ) ) );
			$userId = $userData[ 0 ][ "user_id" ];
			
			$confData = PhpbbConfig_table::getValues( array( "config_name" => array( "avatar_path", "avatar_salt" ) ) );
			
			$dbFileName = $userId . "_" . time() . "." . $uploadManager->getFileExtension();
			$fileName = $confData[ "avatar_salt" ] . "_" . $userId; //.jpg
			
			$relPath = "lib/phpBB3/" . $confData[ "avatar_path" ] . "/";
			$fileName = $uploadManager->saveUploadedFile( (Config::getBaseDir() . $relPath), $fileName );
						
			if( is_null( $fileName ) ){
				return null;
			}
			
			$data = array(
					"user_avatar" => $dbFileName,
					"user_avatar_type" => "avatar.driver.upload"
			);
			//PhpbbUsers_table::updateUserData($userId, $data);
			
			return $relPath . $fileName;
		}
		
		return null;
	}
	
	public function loginUser(){
		$userManager = new UserManager();
		$userData = $userManager->getCurrentUser( 0 );
		
		if( is_null( $userData ) ){
			return null;
		}
		
		$loginUser = false;
		$forumApi = new MyForumApi();
		
		$res = $forumApi->getCurrentUserLogin();
		if( $res == "Anonymous" ){
			$loginUser = true;
		}else
		if( $res != $userData[ "login" ] ){
			$forumApi->logout();
			$loginUser = true;
		}
		
		if( $loginUser ){
			if( !$forumApi->isLoginExist( $userData[ "login" ] ) ){
				$userInfo = UserInfo_table::getUserData( $userData[ "id" ], array( "en" ) );
		
				$forumApi->createUser($userData[ "login" ], $userData[ "pass" ], $userInfo[ "email" ], ($userInfo[ "first_name" ] . " " . $userInfo[ "last_name" ]));
			}
		
			$admin = false;
			if( $userData[ "role" ] == UserManager::$ROLE_ADMIN ){
				$admin = true;
			}
			$res = $forumApi->login($userData[ "login" ], $userData[ "pass" ], $admin);
		}
		
		return true;
	}
	
	
	public function updatePassword( $pass ){
		$userManager = new UserManager();
		$userData = $userManager->getCurrentUser( 0 );
		
		if( is_null( $userData ) ){
			return null;
		}

		$forumApi = new MyForumApi();
		$forumApi->updatePassword($userData[ "login" ], $pass);
	}
}