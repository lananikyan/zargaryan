<?php
class UploadManager {
	private $_FILES = null;
	
	public function __construct( $fileObj = null ){
		if( is_null( $fileObj ) ){
			$fileObj = $_FILES;
		}
		$this->_FILES = $fileObj;
	}
	
	
	
	public function generateRandomString( $length = 10 ) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $randomString;
	}
	
	public function saveUploadedFile( $dir, $newName = null, $fieldName = null, $overwrite = true ){
		if( !$this->isFileExistForUpload( $fieldName ) ){
			return null;
		}
		
		if( is_null( $fieldName ) ){
			$keys = array_keys( $this->_FILES );
			$fieldName = $keys[ 0 ];
		}
		
		$fileName = is_null( $newName ) ? pathinfo($this->_FILES[ $fieldName ]["name"], PATHINFO_FILENAME) : $newName;
		$ex = pathinfo($this->_FILES[ $fieldName ]["name"], PATHINFO_EXTENSION);
		$fileName = $fileName . "." . $ex;
		
        if (!is_dir($dir)) {
          mkdir($dir, 0775, true);
        }
		if( file_exists( ($dir . $fileName) ) ){
			if( !$overwrite ){
				return null;
			}
			
			unlink( ($dir . $fileName) );
		}
		
		$res = move_uploaded_file( $this->_FILES[ $fieldName ]["tmp_name"], ($dir . $fileName));
		
		return $fileName;
	}
	
	public function isFileExistForUpload( $fieldName = null ){
		if( is_null( $fieldName ) ){
			if( count( array_keys( $this->_FILES ) ) == 0 ){
				return false;
			}
			
			return true;
		}
		
		return isset( $this->_FILES[ $fieldName ] );
	}
	
	public function getFileExtension( $fieldName = null ){
		if( !$this->isFileExistForUpload( $fieldName ) ){
			return null;
		}
		
		if( is_null( $fieldName ) ){
			$keys = array_keys( $this->_FILES );
			$fieldName = $keys[ 0 ];
		}
		
		return pathinfo($this->_FILES[ $fieldName ]["name"], PATHINFO_EXTENSION);
	}

	public function getFileSize( $fieldName = null ){
		if( is_null( $fieldName ) ){
			$keys = array_keys( $this->_FILES );
			$fieldName = $keys[ 0 ];
		}
		return $this->_FILES[ $fieldName ]["size"];
	}

	public function deleteFile($dir, $fileName ){
		if( file_exists( ($dir . $fileName) ) ){
			unlink( ($dir . $fileName) );
		}
	}
}