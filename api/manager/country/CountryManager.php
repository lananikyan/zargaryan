<?php

require_once Config::getBaseDir() . 'database_layer/tables/CountryList_table.php';

class CountryManager {
	public function getCountriesWithAllLang(){
		return CountryList_table::getCountryData( Config::getLanguages() );
	}
}