<?php

require_once Config::getBaseDir() . 'database_layer/tables/User_table.php';

class UserManager {
	public static $ROLE_GOD = 0;
	public static $ROLE_ADMIN = 1;
	public static $ROLE_VIEWER = 2;
	public static $ROLE_USER = 3;

	public static $STATUS_INCORRECT_DATA = 0;
	public static $STATUS_LOGIN = 1;
	public static $STATUS_NOT_APPROVED = 2;
	public static $STATUS_WAIT_APPROVEMENT = 4;
	public static $STATUS_NOT_ACTIVE = 3;
	
	public function login( $login, $password ){
		$res = User_table::login( $login, $password );
        
		//var_dump( $res );
		
		if( is_null( $res ) ){
			return self::$STATUS_INCORRECT_DATA;
		}
		if( $res == User_table::$STATUS_NOT_ACTIVE ){
			return self::$STATUS_NOT_ACTIVE;
		}
		if( $res == User_table::$STATUS__DISAPPROVE ){
			return self::$STATUS_NOT_APPROVED;
		}
		if( $res == User_table::$STATUS__WAIT_APPROVEMENT ){
			return self::$STATUS_WAIT_APPROVEMENT;
		}
		
		$_SESSION[ "currentUserData" ] = $res;
        $_SESSION["currentUserData"]['auth_token'] = User_table::userSessionStart($res['id']);
		
		return self::$STATUS_LOGIN;
	}
	
	public function logout(){

      if( isset( $_SESSION[ "currentUserData" ] ) ) {
		User_table::userSessionDestroy($_SESSION[ "currentUserData" ]['id']);
        unset( $_SESSION[ "currentUserData" ] );
      }
	}
    
	public function getCurrentUser( $type = 0 ){
		if( isset( $_SESSION[ "currentUserData" ] ) ){
			if( $type == 0 ){
				return $_SESSION[ "currentUserData" ];
			}
			
			if( $type == 1 ){
				$data = $_SESSION[ "currentUserData" ];
				unset( $data[ "pass" ] );
				return $data;
			}
		}
		
		return null;
	}
	
	public function rejectUser( $id ){
		User_table::update( $id, array( "approve_status" => User_table::$STATUS__DISAPPROVE ) );
		UserRelation_table::removeUserRelation( $id );
	}
	
	public function approveUser( $id ){
		User_table::update( $id, array( "approve_status" => User_table::$STATUS__APPROVE ) );
	}

	public function updateUserRecoverPass( $id){
		$recover_link = uniqid(time(), true);
		$full_recover_link = '/changePassword/'.$recover_link;
		User_table::update( $id, array( "unique_token" => $recover_link ) );
		return $full_recover_link;
	}
	
	public function getRandomUser( $type = 0 ){
	  	$langManager = new LanguageManager();
		$userData = UserInfo_table::getRandomUserData( $langManager->getCurrentLanguage() );

		if( $type == 0 ){
			return $userData;
		}				
		if( $type == 1 ){
			//unset( $data[ "pass" ] );
			return $userData;
		}
	
		return null;
	}


	public function getUsersByName( $search = null, $member = null ){
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';
		$langManager = new LanguageManager();

		return UserInfo_table::getUserSearchList( $langManager->getCurrentLanguage(), $search, $member );
	}

	public function getUsersByEmail( $search = null ){
		/**
		 * @return Users List which have not $ROLE_VIEWER
		 */
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';
		$langManager = new LanguageManager();

		return UserInfo_table::getUserByEmail( $langManager->getCurrentLanguage(), $search);
	}

	public function getUsersByUniqueToken( $search = null ){
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';
		$langManager = new LanguageManager();

		return UserInfo_table::getUserByUnique( $langManager->getCurrentLanguage(), $search);
	}

	public function getUsersByBirthday( $search = null ){
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';
		$langManager = new LanguageManager();

		return UserInfo_table::getUsersByBirthday( $langManager->getCurrentLanguage(), $search);
	}


	public function getUsersCountWaitingApprove(){
		return User_table::getUsersCount( array( "approve_status" => User_table::$STATUS__WAIT_APPROVEMENT ) );
	}

	public function getVisitCount() {
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';
		return UserInfo_table::getVisitCount();

	}
	public function incrementVisitCount() {
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';
		return UserInfo_table::incrementVisitCount();

	}


	public function createUser( $data, $uploadManager = null){
/*		if( isset( $data[ "dead_date" ] ) && !is_null( $data[ "dead_date" ] ) && $data[ "dead_date" ] != "" ){
			$data[ "pass" ] = GlobalHelper::generateRandomString();
		}else */
		if( !isset( $data[ "pass" ] ) || is_null( $data[ "pass" ] ) || $data[ "pass" ] == "" ){
			$data[ "pass" ] = GlobalHelper::generateRandomString();
		}
		if( !isset( $data[ "login" ] ) || is_null( $data[ "login" ] ) || $data[ "login" ] == "" ){
			$data[ "login" ] = GlobalHelper::generateRandomString();
		}
	  	if( empty( $data[ "email" ] )){
			$data[ "email" ] = "user_" . $data[ "login" ] . '@zargaryan.com';
		}
		//require_once Config::getBaseDir() . 'manager/forum/ForumManager.php';
		//$forumLang = "en";
		//$forumManager = new ForumManager();
		//$forumUserId = $forumManager->createUser($data[ "login" ], md5( $data[ "pass" ] ), $data[ "email" ], $data[ "_languageData" ][ $forumLang ][ "first_name" ], $data[ "_languageData" ][ $forumLang ][ "last_name" ]);
		
		$userFileName = null;
		if( $uploadManager->isFileExistForUpload() ){
			$userFileName = $uploadManager->saveUploadedFile( Config::getUserPhotoFilesDir(), $uploadManager->generateRandomString() );
		}
		
		$userData = $data;
		unset( $userData[ "_languageData" ] );

		if( !is_null( $userFileName ) ){
			$userData[ "profile_picture" ] = $userFileName;
		}
		
		$currentUserData = self::getCurrentUser( 0 );
		if( !is_null( $currentUserData ) && $currentUserData[ "role" ] == self::$ROLE_ADMIN ){
			$userData[ "approve_status" ] = User_table::$STATUS__APPROVE;
		}

		$userId = User_table::create( $userData );

		UserInfo_table::createUserData($userId, $data[ "_languageData" ]);


		
		return $userId;
	}

	public function createMember( $data, $uploadManager = null){
		if( !isset( $data[ "pass" ] ) || is_null( $data[ "pass" ] ) || $data[ "pass" ] == "" ){
			$data[ "pass" ] = GlobalHelper::generateRandomString();
		}
		if( !isset( $data[ "login" ] ) || is_null( $data[ "login" ] ) || $data[ "login" ] == "" ){
			$data[ "login" ] = GlobalHelper::generateRandomString();
		}

		$currentUserId = $data[ "relative_id" ];
		$type = $data[ "rel_type" ];
		$data[ "approve_status" ] = User_table::$STATUS__APPROVE;

		$userFileName = null;
		if( $uploadManager->isFileExistForUpload() ){
			$userFileName = $uploadManager->saveUploadedFile( Config::getUserPhotoFilesDir(), $uploadManager->generateRandomString() );
		}

		if( !is_null( $userFileName ) ){
			$data[ "profile_picture" ] = $userFileName;
		}

		$userId = User_table::create( $data );

		UserInfo_table::createUserData($userId, $data[ "_languageData" ]);
		self::changeUserRelation( $currentUserId, $userId, $type  );
		return $userId;
	}
	
	
	public function updateUser($data, $uploadManager){
		$userManager = new UserManager();
		
		if( isset( $data[ "id" ] ) ){
			$currentUserData = $userManager->getUserData( $data[ "id" ] );
			$currentUserData[ "id" ] = $currentUserData[ "user_id" ];
		}else{
			$currentUserData = $userManager->getCurrentUser( 0 );
		}

		$get_user_role = User_table::getUserRole($currentUserData[ "id" ]);

		$userFileName = null;
		if( $uploadManager->isFileExistForUpload() ){
			if(!empty($currentUserData["profile_picture"])){
				$uploadManager->deleteFile(Config::getUserPhotoFilesDir(), $currentUserData["profile_picture"]);

				//Delete also all styles for current user if exists
				//Need to include imageManager class
				$imageManager = new \Image\ImageManager(Config::getDataDir(), Config::getUserPhotoDirName(), Config::getPhotoStylesDirName());
				$imageManager->deleteAllStyleImagesByName($currentUserData["profile_picture"]);

			}
			$userFileName = $uploadManager->saveUploadedFile( Config::getUserPhotoFilesDir(), $uploadManager->generateRandomString() );
		}

		$userData = $data;
		
		if( isset( $userData[ "_languageData" ] ) ){
			unset( $userData[ "_languageData" ] );
		}
		
		if( !is_null( $userFileName ) ){
			$userData[ "profile_picture" ] = $userFileName;
		}

		if( count( $userData ) > 0 ){
			User_table::update($currentUserData[ "id" ], $userData);
		}
		if( isset( $data[ "_languageData" ] ) ){
			UserInfo_table::updateUserData( $currentUserData[ "id" ], $data[ "_languageData" ] );
		}

		/*if ($get_user_role['role'] == 2) {
			return $get_user_role[ "user_id" ];
		}*/

		return $currentUserData[ "id" ];
	}
	
	
	public function getUserList(){
		$langManager = new LanguageManager();
		
		//$list = UserInfo_table::getUsersList($langManager->getCurrentLanguage(), null, array(User_table::$STATUS__APPROVE, User_table::$STATUS__WAIT_APPROVEMENT));
		$list = UserInfo_table::getUsersList($langManager->getCurrentLanguage(), array('role'=>self::$ROLE_VIEWER), array(User_table::$STATUS__APPROVE, User_table::$STATUS__WAIT_APPROVEMENT));
		return $list;
	}
	
	public function getUserListForTree(){
		$langManager = new LanguageManager();
		
		$list = UserInfo_table::getTreeUsersList( $langManager->getCurrentLanguage() );
		return $list;
	}
	
	public function getUserData( $id ){
		$langManager = new LanguageManager();
		
		return UserInfo_table::getUserData($id, array($langManager->getCurrentLanguage()));
	}

	public function getUserRelatives( $id, $rel_type ){
		$langManager = new LanguageManager();

		return UserRelation_table::getRelatives($id, $langManager->getCurrentLanguage(), $rel_type);
	}

	public function getUserPicFileUsed( $id ){

		return User_table::getUserPicFileUsed($id);
	}

	public function getUserVideoFileUsed( $id ){

		return User_table::getUserVideoFileUsed($id);
	}
	
	public function getUserData_allLang( $id ){
		return UserInfo_table::getUserData($id, Config::getLanguages());
	}
	
	public function getUserTree( $userId, $upCount=2, $downCount=2 ){
		$langManager = new LanguageManager();

		$returnValue = UserRelation_table::getTree( $userId, $upCount, $downCount, $langManager->getCurrentLanguage() );
		$var = UserRelation_table::getTorNer($userId);
		$count = count($var);
		if($count < 9) {
			return $returnValue;
		} else {
			$returnValue1 = array();
			foreach ($returnValue as &$value) {
					$found = 0;
				foreach($var as &$id) {
					if($id["id"] == $value["id"]) {
						$found = 1;
					}
				}
				if($found == 0) {
					array_push($returnValue1, $value);
				}
			}
        }
        return $returnValue1;
	}
	
	public function changeUserRelation( $userId, $relUserId, $type ){
		UserRelation_table::setUserRelation($userId, $relUserId, $type);
	}
	
	public function removeUserRelation( $userId, $relUserId ){
		UserRelation_table::removeUserRelation( $userId, $relUserId );
	}
	public function removeUserRelationInfo( $relUserId ){
		$allLangs = (Config::getLanguages());
		User_table::removeUserRelationInfo( $relUserId,  $allLangs);
	}

	public function sendSystemEmail( $data ){
		$mail = new PHPMailer();
		$body = $data[ "message" ];
		$mail->CharSet  = 'UTF-8';
		$mail->AddAddress( $data[ "email" ] );
		$mail->SetFrom( "noreply@zargaryan.com", "Zargaryan Family" );
		$mail->Subject = $data[ "subject" ];
		//$mail->MsgHTML( $body );
		$mail->Body = $body;

		$mail->Send();
	}
	
	/**
	 * Get admin users list
	 * @param bool $withInfo get admins info data or not
	 * @return Result
	 */
	public function getAdminUsers($withInfo = false){
		$langManager = new LanguageManager();
		return User_table::getUsersByRole(self::$ROLE_ADMIN, $withInfo);
	}

	/**
	 * Send email notification to admin that new user registered.
	 * @param integer $user_id, user id
	 * @param $json_arr all constant texts for current language
	 */
	public function newUserRegisteredEmail($user_id, $json_arr){

		$user = $this->getUserData($user_id);

		$data = array();
		$admins = $this->getAdminUsers();
		if($admins){
			$adminEmails = array();
			foreach($admins as $adminUser){
				$adminEmails[] = $adminUser["email"];
			}
			$data[ "email" ] = implode(",", $adminEmails);
			$data[ "message" ] = 	$json_arr['admin_new_user_registration'] . "\n" . $json_arr['admin_user_table_name'] . " : " . $user["first_name"] . " " . $user["last_name"] . "\n" .
				$json_arr['admin_user_table_email'] . " : " . $user["email"];
			$data[ "subject" ] = $json_arr['admin_new_user_registration'];

			$this->sendSystemEmail( $data );
		}
	}

	public function userApprovedInfoMail($user_id, $json_arr){

		$user = $this->getUserData($user_id);

		if($user){
			$data = array();

			$data[ "email" ] = $user["email"];
			$data[ "message" ] = 	$json_arr["dear"] . " " . $user["first_name"] . " " . $user["last_name"] . "\n" . $json_arr['admin_new_user_approved_subject'];
			$data[ "subject" ] =  $json_arr['admin_new_user_approved_subject'];

			$this->sendSystemEmail( $data );
		}
	}
	
	public function isLoginExist($login){
		return User_table::isLoginExist( $login);
	}
	
}