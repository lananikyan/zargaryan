<?php
class FormatManager{
	public static function DBtoUI( $data ){
		$langData = array();
		foreach ( Config::getLanguages() AS $lang ){
			foreach ( $data AS $key => $val ){
				$parts = explode(($lang . "_"), $key);
				if( count( $parts ) > 1 ){
					$langData[ "_languageData" ][ $lang ][ $parts[ 1 ] ] = $val;
					unset( $data[ $key ] );
				}
			}
		}
		
		$langData = array_merge( $data, $langData );
		
		return $langData;
	}
	
	public static function UItoDB( $data, $oneTable = false ){
		if( !isset( $data[ "_languageData" ] ) ){
			return $data;
		}
		
		$res = null;
		if( $oneTable ){
			$uniqueData = $data;
			unset( $uniqueData[ "_languageData" ] );
				
			foreach ( $data[ "_languageData" ] AS $key => $val ){
				$data[ "_languageData" ][ $key ] = array_merge( $uniqueData, $val );
				foreach ( $data[ "_languageData" ][ $key ] AS $colName => $colVal ){
					$uniqueData[ ($key . "_" . $colName) ] = $colVal;
				}
			}
				
			$res = $uniqueData;
		}else{
			$uniqueData = $data;
			unset( $uniqueData[ "_languageData" ] );
			
			foreach ( $data[ "_languageData" ] AS $key => $val ){
				$data[ "_languageData" ][ $key ] = array_merge( $uniqueData, $val );
			}
			
			$res = $data[ "_languageData" ];
		}
		
		return $res;
	}
}