<?php

define('IN_PHPBB', true);
global $phpbb_root_path;
global $phpEx;
$phpbb_root_path = (dirname(__FILE__) . "/");
$phpEx = "php";
include('common.' . $phpEx);
include('includes/functions_user.' . $phpEx);


//namespace phpBB3;

class MyForumApi {
	
	public function isLoginExist( $login ){
		global $db;
				
		$sql = 'SELECT user_id, username, username_clean FROM ' . USERS_TABLE . ' WHERE username_clean = "' . $db->sql_escape( utf8_clean_string( $login ) ) . '"';		
		$result = $db->sql_query( $sql );
		
		if (!($row = $db->sql_fetchrow($result))){
			//$db->sql_freeresult($result);
			return false;
		}
		
		
		return true;
	}
	
	
	public function updatePassword( $login, $newPassword ){
		global $db;
	
		$sql = 'UPDATE ' . USERS_TABLE . ' SET `user_password` = "' . phpbb_hash( $newPassword ) . '" WHERE username_clean = "' . $db->sql_escape( utf8_clean_string( $login ) ) . '"';
		$result = $db->sql_query( $sql );
	
		return true;
	}
	
	
	public function getUnreadedNotificationsCount(){
		global $phpbb_container;
	
		$phpbb_notifications = $phpbb_container->get('notification_manager');
		$notifications = $phpbb_notifications->load_notifications( array( 'all_unread'	=> true	) );
		
		//var_dump( $notifications );
		
		return $notifications['unread_count'];
	}
	
	public function getCurrentUserLogin(){
		global $user;
		global $auth;
		
		$user->session_begin();
		$auth->acl( $user->data );
		$user->setup();
		
		return $user->data[ 'username' ];
	}
	
	public function login( $login, $pass, $admin = false ){
		global $user;
		global $auth;
		
		$user->session_begin();
		$auth->acl( $user->data );
		$user->setup();
		
		$username = request_var('username', $login);
		$password = request_var('password', $pass);
		
		if( isset( $username ) && isset( $password )){
			$result = $auth->login( $username, $password, true, 1, ($admin ? 1 : 0) );
			if( $result['status'] == LOGIN_SUCCESS ) {
				return true;
			} else {
				return $user->lang[ $result['error_msg'] ];
			}
		}
		
		return false;
	}
	
	public function logout(){
		global $user;
		global $auth;
		
		$user->session_begin();
		$auth->acl( $user->data );
		$user->setup();
		
		$user->session_kill();
		$user->session_begin();
	}
	
	public function createUser( $login, $pass, $email, $name ){
		global $user;
		global $auth;
		
		$user_row = array(
				'username'              => $name,
				'username_clean'		=> utf8_clean_string( $login ),
				'user_password'         => phpbb_hash( $pass ),
				'user_email'            => $email,
				'group_id'              => 2,
				'user_type'             => 0,
				'user_actkey'           => "",
				'user_inactive_reason'  => 0,
				'user_inactive_time'    => 0,
				'user_new'              => 1
		);
		
		$user_id = user_add( $user_row );
		return $user_id;
	}
}

?>