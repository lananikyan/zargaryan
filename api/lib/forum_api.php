<?php 
class ForumApi {
	
	public function createUser( $login, $password, $email ) {
		include($phpbb_root_path . 'includes/functions_user.php');
		
		$user_row = array(
				'username'              => $login,
				'user_password'         => phpbb_hash( $password ),
				'user_email'            => $email,
				'group_id'              => (int) 4,
				'user_type'             => USER_NORMAL
		);
		
		// all the information has been compiled, add the user
		// tables affected: users table, profile_fields_data table, groups table, and config table.
		$user_id = user_add($user_row);
	}
	
}