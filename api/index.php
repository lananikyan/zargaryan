<?php
header('Access-Control-Allow-Origin: *');

session_start();

require_once dirname(__FILE__) . '/Config.php';
require_once dirname(__FILE__) . '/ConfigLocal.php';


require_once Config::getBaseDir() . 'database_layer/DBconnection.php';
require_once Config::getBaseDir() . 'database_layer/DatabaseTables.php';

require_once dirname(__FILE__) . '/manager/chat/chatManager.php';
require_once dirname(__FILE__) . '/manager/chat/nodejsClass.php';

require_once Config::getBaseDir() . 'lib/format.php';

if( isset( $_REQUEST[ "action" ] ) ){

	if( $_REQUEST[ "action" ] == "uploadFile" ){
		require_once Config::getBaseDir() . 'manager/upload/UploadManager.php';
		
		$uploadManager = new UploadManager();
		$fName = $uploadManager->saveUploadedFile( Config::getUploadFileDir(), $uploadManager->generateRandomString() );
		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = Config::getUploadFilePath() . $fName;
		
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "getForumNotificationCount" ){
		require_once Config::getBaseDir() . 'manager/user/UserManager.php';
		require_once Config::getBaseDir() . 'manager/forum/ForumManager.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';
		
		$forumManager = new ForumManager();
		
		$count = $forumManager->getNotificationsCount();
		if( is_null( $count ) ){
			$returnValue = array();
			$returnValue[ "type" ] = 1;
		}else{
			$returnValue = array();
			$returnValue[ "type" ] = 0;
			$returnValue[ "value" ] = $count;
		}
		
		
		echo json_encode( $returnValue );
	}else

	if( $_REQUEST[ "action" ] == "getLangaugeList" ){
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		
		$manager = new LanguageManager();
		
		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = $manager->getLanguageList();
		
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "getCurrentLangauge" ){
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		
		$manager = new LanguageManager();
		
		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = $manager->getCurrentLanguage();
		
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "getAppCaptions" ){
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		
		$manager = new LanguageManager();
		
		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = $manager->getLangObject();
		
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "changeCurrentLanguage" ){
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		
		$manager = new LanguageManager();
		$manager->setCurrentLanguage( $_POST[ "lang" ] );
		
		$returnValue = array();
		$returnValue[ "type" ] = 0;
		
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "enterForum" ){
		require_once Config::getBaseDir() . 'manager/user/UserManager.php';
		require_once Config::getBaseDir() . 'manager/forum/ForumManager.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';
		
		$manager = new ForumManager();
		$manager->loginUser();
		
		$returnValue = array();
		$returnValue[ "type" ] = 0;
		
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "getCountriesWithAllLang" ){
		require_once Config::getBaseDir() . 'manager/country/CountryManager.php';
		
		$manager = new CountryManager();
		$data = $manager->getCountriesWithAllLang();
		
		for( $i = 0; $i < count( $data ); $i++ ){
			$data[ $i ] = FormatManager::DBtoUI( $data[ $i ] );
		}
		
		
		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = $data;
		
		echo json_encode( $returnValue );
	}else
//***************************************************************************************MAIN INFO************************************************************************************//
	if( $_REQUEST[ "action" ] == "getShortMainInfo" ){
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		require_once Config::getBaseDir() . 'manager/main_page/MainPageManager.php';
	
		$mainManager = new MainPageManager();
		$data = $mainManager->getInfoForUser( null, 0 );
		//echo json_encode( $data );
		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = $data;

	
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "getLongMainInfo" ){
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		require_once Config::getBaseDir() . 'manager/main_page/MainPageManager.php';
	
		$mainManager = new MainPageManager();
		$data = $mainManager->getInfoForUser( $_REQUEST[ "id" ], 1 );
		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = $data[ 0 ];
	
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "getLongMainInfo_allLang" ){
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		require_once Config::getBaseDir() . 'manager/main_page/MainPageManager.php';
	
		$mainManager = new MainPageManager();
		$data = $mainManager->getInfoForAdmin( $_REQUEST[ "id" ] );
		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = $data;
	
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "setLongMainInfo_allLang" ){
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		require_once Config::getBaseDir() . 'manager/main_page/MainPageManager.php';
	
		$mainManager = new MainPageManager();
		$mainManager->updateInfoFromAdmin( json_decode( $_REQUEST[ "data" ], true ) );
		$returnValue = array();
		$returnValue[ "type" ] = 0;
	
		echo json_encode( $returnValue );
	}else	
//***************************************************************************************CONTACT US************************************************************************************//
	if( $_REQUEST[ "action" ] == "getNewMessagesCount" ){
		require_once Config::getBaseDir() . 'manager/contact/ContactManager.php';
	
		$manager = new ContactManager();
	
		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = $manager->getNewMessagesCount();
	
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "getNewMessages" ){
		require_once Config::getBaseDir() . 'manager/contact/ContactManager.php';
	
		$manager = new ContactManager();
	
		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = $manager->getNewMessages();
	
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "createNewMessage" ){
		require_once Config::getBaseDir() . "lib/mail/class.smtp.php";
		require_once Config::getBaseDir() . "lib/mail/class.phpmailer.php";
		require_once Config::getBaseDir() . 'manager/contact/ContactManager.php';
		require_once Config::getBaseDir() . 'manager/upload/UploadManager.php';
	
		$manager = new ContactManager();
	
		$data = array();
		$data[ "name" ] = $_REQUEST[ "name" ];
		$data[ "email" ] = $_REQUEST[ "email" ];
		$data[ "phone" ] = isset( $_REQUEST[ "phone" ] ) ? $_REQUEST[ "phone" ] : null;
		$data [ "type" ] = isset( $_REQUEST[ "type" ] ) ? $_REQUEST[ "type" ] : null;
		//$data[ "phone" ] = $_REQUEST[ "phone" ];
		$data[ "header" ] = $_REQUEST[ "header" ];
		$data[ "message" ] = $_REQUEST[ "message" ];
		$data[ "is_new" ] = 1;
	
		$manager->createMessage( $data );
	
		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "data" ] = $data;
	
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "setMessageOld" ){
		require_once Config::getBaseDir() . 'manager/contact/ContactManager.php';
	
		$manager = new ContactManager();
		$manager->setMessageOld( $_REQUEST[ "id" ] );
	
		$returnValue = array();
		$returnValue[ "type" ] = 0;
	
		echo json_encode( $returnValue );
	}else

//***************************************************************************************FORUM OUR************************************************************************************//
	if( $_REQUEST[ "action" ] == "rejectForum" ){
		require_once Config::getBaseDir() . 'manager/forum_our/Forum_OurManager.php';

		$manager = new Forum_OurManager();
		$manager->rejectForum( $_REQUEST[ "id" ] );

		$returnValue = array();
		$returnValue[ "type" ] = 0;

		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "approveForum" ){
		require_once Config::getBaseDir() . 'manager/forum_our/Forum_OurManager.php';

		$manager = new Forum_OurManager();
		$manager->approveForum( $_REQUEST[ "id" ] );

		$returnValue = array();
		$returnValue[ "type" ] = 0;

		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "updateForum" ){
    		require_once Config::getBaseDir() . 'manager/forum_our/Forum_OurManager.php';

    		$manager = new Forum_OurManager();
    		$manager->updateForumTitle( $_REQUEST[ "id" ], $_REQUEST[ "title" ] );

    		$returnValue = array();
    		$returnValue[ "type" ] = 0;

    		echo json_encode( $returnValue );
    	}else
	if( $_REQUEST[ "action" ] == "getForum" ){
		require_once Config::getBaseDir() . 'manager/forum_our/Forum_OurManager.php';

		$manager = new Forum_OurManager();


		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = $manager->getForum();

		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "getForumAdmin" ){
		require_once Config::getBaseDir() . 'manager/forum_our/Forum_OurManager.php';

		$manager = new Forum_OurManager();

		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = $manager->getForumAdmin();


		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "createForum" ){
		require_once Config::getBaseDir() . 'manager/forum_our/Forum_OurManager.php';

		$manager = new Forum_OurManager();

		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = $manager->createForum( json_decode( $_REQUEST[ "data" ]));


		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "forumCountAdmin" ){
		require_once Config::getBaseDir() . 'manager/forum_our/Forum_OurManager.php';

		$manager = new Forum_OurManager();

		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = $manager->forumCountAdmin();


		echo json_encode( $returnValue );
	}else
//***************************************************************************************FORUM COMMENT************************************************************************************//
	if( $_REQUEST[ "action" ] == "forumCommentCountAdmin" ){
		require_once Config::getBaseDir() . 'manager/forum_our/ForumCommentManager.php';

		$manager = new ForumCommentManager();

		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = $manager->forumCommentCountAdmin();


		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "rejectComment" ){
		require_once Config::getBaseDir() . 'manager/forum_our/ForumCommentManager.php';

		$manager = new ForumCommentManager();
		$manager->rejectComment( $_REQUEST[ "id" ] );

		$returnValue = array();
		$returnValue[ "type" ] = 0;

		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "approveComment" ){
		require_once Config::getBaseDir() . 'manager/forum_our/ForumCommentManager.php';

		$manager = new ForumCommentManager();
		$manager->approveComment( $_REQUEST[ "id" ] );

		$returnValue = array();
		$returnValue[ "type" ] = 0;

		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "createComment" ){
    		require_once Config::getBaseDir() . 'manager/forum_our/ForumCommentManager.php';

    		$manager = new ForumCommentManager();
    		$manager->createComment(  json_decode( $_REQUEST[ "data" ] ));

    		$returnValue = array();
    		$returnValue[ "type" ] = 0;
				$returnValue[ "value" ] = 1;

    		echo json_encode( $returnValue );
    	}else
	if( $_REQUEST[ "action" ] == "getForumComment" ){
		require_once Config::getBaseDir() . 'manager/forum_our/ForumCommentManager.php';
		require_once Config::getBaseDir() . 'manager/image/ImageManager.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';

		$manager = new ForumCommentManager();


		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$comments = $manager->getForumComment($_REQUEST[ "id" ]);
		if($comments){
			$imageManager = new \Image\ImageManager(Config::getDataDir(), Config::getUserPhotoDirName(), Config::getPhotoStylesDirName());
			foreach($comments as $key => $comment){
				$comments[$key]['author_name'] = $comment['first_name'].' '.$comment['last_name'];
				if(!empty($comment['profile_picture'])){
					try{
						$comments[$key]["user_avatar"] = Config::getDataDirUrl() . $imageManager->generateAndGetImageByStyle($comment['profile_picture'], "thumbnail");
					}
					catch(Exception $e){ }
				}
			}
		}

		$returnValue[ "value" ] = $comments;

		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "getForumCommentAdmin" ){
		require_once Config::getBaseDir() . 'manager/forum_our/ForumCommentManager.php';

		$manager = new ForumCommentManager();

		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = $manager->getForumCommentAdmin();


		echo json_encode( $returnValue );
	}else
//***************************************************************************************ARTICLE************************************************************************************//
	if( $_REQUEST[ "action" ] == "rejectArticle" ){
		require_once Config::getBaseDir() . 'manager/article/ArticleManager.php';
		
		$manager = new ArticleManager();
		$manager->rejectArticle( $_REQUEST[ "id" ] );
		
		$returnValue = array();
		$returnValue[ "type" ] = 0;
		
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "approveArticle" ){
		require_once Config::getBaseDir() . 'manager/article/ArticleManager.php';
		
		$manager = new ArticleManager();
		$manager->approveArticle( $_REQUEST[ "id" ] );
		
		$returnValue = array();
		$returnValue[ "type" ] = 0;
		
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "getNewArticleCount" ){
		require_once Config::getBaseDir() . 'manager/article/ArticleManager.php';
		
		$manager = new ArticleManager();
		
		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = $manager->getWaitApproveArticleCount();
		
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "createArticle" ){
		require_once Config::getBaseDir() . 'manager/article/ArticleManager.php';
		require_once Config::getBaseDir() . 'manager/user/UserManager.php';
		
		$manager = new ArticleManager();
		$id = $manager->createArticle( json_decode( $_REQUEST[ "data" ], true ) );
		
		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = $id;
		
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "getArticlesShortList" ){
		require_once Config::getBaseDir() . 'manager/article/ArticleManager.php';
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';
		
		$search = isset( $_REQUEST[ "search" ] ) ? $_REQUEST[ "search" ] : null;
		$manager = new ArticleManager();
		$list = $manager->getArticlesShortList( $search );
		
		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = $list;
		
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "getArticlesShortListForAdmin" ){
		require_once Config::getBaseDir() . 'manager/article/ArticleManager.php';
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';
		
		$search = isset( $_REQUEST[ "search" ] ) ? $_REQUEST[ "search" ] : null;
		$manager = new ArticleManager();
		$list = $manager->getArticlesShortListForAdmin( $search );
		
		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = $list;
		
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "getArticlesDetail" ){
		require_once Config::getBaseDir() . 'manager/article/ArticleManager.php';
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';
		require_once Config::getBaseDir() . 'database_layer/tables/User_table.php';
		require_once Config::getBaseDir() . 'manager/image/ImageManager.php';
		
		$manager = new ArticleManager();
		$info = $manager->getArtcleDetail( $_REQUEST[ "id" ] );


		if(!empty($info['profile_picture'])){
			try{
				$imageManager = new \Image\ImageManager(Config::getDataDir(), Config::getUserPhotoDirName(), Config::getPhotoStylesDirName());
				$info["user_avatar"] = Config::getDataDirUrl() . $imageManager->generateAndGetImageByStyle($info['profile_picture'], "thumbnail");
			}
			catch(Exception $e){ }
		}
		
		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = $info;
		
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "getArticlesDetail_allLang" ){
		require_once Config::getBaseDir() . 'manager/article/ArticleManager.php';
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';
		require_once Config::getBaseDir() . 'database_layer/tables/User_table.php';
		require_once Config::getBaseDir() . 'manager/image/ImageManager.php';
		
		$manager = new ArticleManager();
		$info = $manager->getArtcleDetail_allLang( $_REQUEST[ "id" ] );
		if(!empty($info['profile_picture'])){
			try{
				$imageManager = new \Image\ImageManager(Config::getDataDir(), Config::getUserPhotoDirName(), Config::getPhotoStylesDirName());
				$info["user_avatar"] = Config::getDataDirUrl() . $imageManager->generateAndGetImageByStyle($info['profile_picture'], "thumbnail");
			}
			catch(Exception $e){ }
		}
		
		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = FormatManager::DBtoUI( $info );
		
		echo json_encode( $returnValue );
	}else 
//***************************************************************************************USER************************************************************************************//
	if( $_REQUEST[ "action" ] == "userLogin" ){
		require_once Config::getBaseDir() . 'manager/user/UserManager.php';
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';
	
		$langManager = new LanguageManager();
		$userManager = new UserManager();
		
		$res = $userManager->login($_REQUEST[ "login" ], $_REQUEST[ "pass" ]);
	
		$returnValue = array();
		if( $res === UserManager::$STATUS_LOGIN ){
			$userData = $userManager->getCurrentUser( 1 );
			$userInfoData = UserInfo_table::getUserData($userData[ "id" ], array( $langManager->getCurrentLanguage() ));
				
			$returnValue[ "type" ] = 0;
			$returnValue[ "value" ] = array_merge($userData, $userInfoData);
		}else
			if( $res === UserManager::$STATUS_INCORRECT_DATA ){
			$returnValue[ "type" ] = 1;
		}else
			if( $res === UserManager::$STATUS_NOT_ACTIVE ){
			$returnValue[ "type" ] = 2;
		}else
			if( $res === UserManager::$STATUS_NOT_APPROVED ){
			$returnValue[ "type" ] = 3;
		}else
			if( $res === UserManager::$STATUS_WAIT_APPROVEMENT ){
			$returnValue[ "type" ] = 4;
		}
	
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "getCurrentUser" ){
		require_once Config::getBaseDir() . 'manager/user/UserManager.php';
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';
		require_once Config::getBaseDir() . 'manager/image/ImageManager.php';
        require_once Config::getBaseDir() . 'manager/chat/chatManager.php';
	
		$langManager = new LanguageManager();
		$userManager = new UserManager();
        $db = new DBconnection();
        $chat = new chatManager($db->getConnection());
		$userData = $userManager->getCurrentUser( 1 );
		$returnValue = array();
		if( !is_null( $userData ) ){
			$userInfoData = UserInfo_table::getUserData($userData[ "id" ], array( $langManager->getCurrentLanguage() ));
            $userInfoData['msg_unseen'] = $chat->getUserUnseenMessages($userData["id"]);
			if(!empty($userData['profile_picture'])){
				try{
					$imageManager = new \Image\ImageManager(Config::getDataDir(), Config::getUserPhotoDirName(), Config::getPhotoStylesDirName());
					$userData["user_avatar"] = Config::getDataDirUrl() . $imageManager->generateAndGetImageByStyle($userData['profile_picture'], "thumbnail");
				}
				catch(Exception $e){ }
			}
	
			$returnValue[ "type" ] = 0;
			$returnValue[ "value" ] = array_merge($userData, $userInfoData);
		}else{
			$returnValue[ "type" ] = 1;
		}
	
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "userLogout" ){
		require_once Config::getBaseDir() . 'manager/user/UserManager.php';
	
		$userManager = new UserManager();
		$userManager->logout();
		$returnValue = array();
		$returnValue[ "type" ] = 0;
	
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "getShortRandomUser" ){
		require_once Config::getBaseDir() . 'manager/user/UserManager.php';
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';
	
		$userManager = new UserManager();
		$userData = $userManager->getRandomUser( 0 );
		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = $userData;
	
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "getLongRandomUser" ){
		require_once Config::getBaseDir() . 'manager/user/UserManager.php';
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';
	
		$userManager = new UserManager();
		$userData = $userManager->getRandomUser( 0 );
		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = $userData;
	
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "getUsersWaitingApproveCount" ){
		require_once Config::getBaseDir() . 'manager/user/UserManager.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';
		
		$userManager = new Usermanager();
		
		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = $userManager->getUsersCountWaitingApprove();
		
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "createNewUser" ){
		//require_once Config::getBaseDir() . 'database_layer/tables/PhpbbConfig_table.php';
		//require_once Config::getBaseDir() . 'database_layer/tables/PhpbbUsers_table.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';
		require_once Config::getBaseDir() . 'lib/global_helper.php';
		require_once Config::getBaseDir() . 'manager/upload/UploadManager.php';
		
		
		$uploadManager = new UploadManager();
		$data = $_REQUEST[ "data" ];
        
		require_once Config::getBaseDir() . 'manager/user/UserManager.php';
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		require_once Config::getBaseDir() . "lib/mail/class.smtp.php";
		require_once Config::getBaseDir() . "lib/mail/class.phpmailer.php";
		
		$returnValue = array();

		$manager = new UserManager();
		$request_data = json_decode($data, true);
		if (isset($request_data['login'])){
			$UsersByEmailList = $manager->getUsersByEmail( $request_data['email'] );
		}
		else {
			$UsersByEmailList = '';
		}
		if (isset($request_data['login'])){
			$UsersByLogin = $manager->isLoginExist( $request_data['login'] );
		}
		else {
			$UsersByLogin = '';
		}

		$returnValue[ "error" ] = array();
		
		if($UsersByLogin == true) {
			$returnValue[ "type" ] = 1;
			$returnValue[ "error" ]["login"] = 'regieter_form_error_login';
		}
		if(!empty($UsersByEmailList)) {
			$returnValue[ "type" ] = 1;
			$returnValue[ "error" ]["mail"] = 'regieter_form_error_email';
		}
		
		if(empty($returnValue[ "error" ])){
			if (isset($request_data['birth_date'])){
				$request_data['birth_date'] = strtotime($request_data['birth_date']);
			}
			//$userId = $manager->createUser( json_decode( $data, true ), $uploadManager );
			$userId = $manager->createUser( $request_data, $uploadManager );
			if($userId){
				require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
				$lang = new LanguageManager();
				$current_language = $lang->getCurrentLanguage();

				$string = file_get_contents('../languages/'. $current_language .'.json');
				$json_arr = json_decode($string, true);

				$manager->newUserRegisteredEmail($userId, $json_arr );
				$returnValue[ "type" ] = 0;

			}
			else {
				$returnValue[ "type" ] = 1;
			}
			$returnValue[ "value" ] = $userId;
		}
		
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "createNewMember" ){
		//require_once Config::getBaseDir() . 'database_layer/tables/PhpbbConfig_table.php';
		//require_once Config::getBaseDir() . 'database_layer/tables/PhpbbUsers_table.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserRelation_table.php';
		require_once Config::getBaseDir() . 'lib/global_helper.php';
		require_once Config::getBaseDir() . 'manager/upload/UploadManager.php';
		require_once Config::getBaseDir() . 'manager/image/ImageManager.php';
		$uploadManager = new UploadManager();
		$data = $_REQUEST[ "data" ];

		require_once Config::getBaseDir() . 'manager/user/UserManager.php';
		// require_once Config::getBaseDir() . 'manager/forum/ForumManager.php';

		$request_data = json_decode($data, true);

		$manager = new UserManager();
		if (isset($request_data['birth_date'])){
			$request_data['birth_date'] = strtotime($request_data['birth_date']);
		}

		$userId = $manager->createMember( $request_data, $uploadManager );

		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = $userId;

		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "updateUser" ){
		//require_once Config::getBaseDir() . 'database_layer/tables/PhpbbConfig_table.php';
		//require_once Config::getBaseDir() . 'database_layer/tables/PhpbbUsers_table.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserRelation_table.php';
		require_once Config::getBaseDir() . 'lib/global_helper.php';
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		require_once Config::getBaseDir() . 'manager/upload/UploadManager.php';
		require_once Config::getBaseDir() . 'manager/image/ImageManager.php';

		$returnValue = array();
		$data = $_REQUEST[ "data" ];

		require_once Config::getBaseDir() . 'manager/user/UserManager.php';
		
		$manager = new UserManager();
		$uploadManager = new UploadManager();

		$request_data = json_decode($data, true);

		if (isset($request_data['role']) && $request_data['role'] != '2'){
			if (isset($request_data['email'])){
				$UsersByEmailList = $manager->getUsersByEmail($request_data['email'] );
				foreach ($UsersByEmailList as $key=>$userWithSameEmail){
					if ($userWithSameEmail['user_id'] == $request_data['id']){
						unset ($UsersByEmailList[$key]);
					}
				}
			}
		}
		else {
			$UsersByEmailList = array();
		}


		$returnValue[ "error" ] = array();

		if(!empty($UsersByEmailList)) {
			$returnValue[ "type" ] = 1;
			$returnValue[ "error" ]["mail"] = 'regieter_form_error_email';
		}

		if(empty($returnValue[ "error" ])){
			//$userId = $manager->updateUser( json_decode( $data, true ), $uploadManager);
			if (isset($request_data['birth_date'])){
				$request_data['birth_date'] = strtotime($request_data['birth_date']);
			}
			if (isset($request_data['dead_date'])){
				$request_data['dead_date'] = strtotime($request_data['dead_date']);
			}
			$userId = $manager->updateUser( $request_data, $uploadManager);
			if(!empty($userId)){
				$returnValue[ "type" ] = 0;
				$returnValue[ "value" ] = $userId;
			}
			else {
				$returnValue[ "type" ] = 1;
			}
		}


		echo json_encode( $returnValue );
	}else 
	if( $_REQUEST[ "action" ] == "rejectUser" ){
		require_once Config::getBaseDir() . 'database_layer/tables/UserRelation_table.php';
		require_once Config::getBaseDir() . 'manager/user/UserManager.php';
	
		$manager = new UserManager();
		$manager->rejectUser( $_REQUEST[ "id" ] );
	
		$returnValue = array();
		$returnValue[ "type" ] = 0;
	
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "approveUser" ){
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';
		require_once Config::getBaseDir() . 'manager/user/UserManager.php';
		require_once Config::getBaseDir() . "lib/mail/class.smtp.php";
		require_once Config::getBaseDir() . "lib/mail/class.phpmailer.php";

		$returnValue = array();

		$manager = new UserManager();
		$manager->approveUser( $_REQUEST[ "id" ] );

		$lang = new LanguageManager();
		$current_language = $lang->getCurrentLanguage();
		$string = file_get_contents('../languages/'. $current_language .'.json');
		$json_arr = json_decode($string, true);

		$manager->userApprovedInfoMail( $_REQUEST[ "id" ], $json_arr);
	

		$returnValue[ "type" ] = 0;

		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "getUserList" ) {
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';
		require_once Config::getBaseDir() . 'manager/user/UserManager.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserRelation_table.php';

		require_once Config::getBaseDir() . 'database_layer/tables/Album_table.php';
		require_once Config::getBaseDir() . 'database_layer/tables/AlbumPicture_table.php';
		require_once Config::getBaseDir() . 'database_layer/tables/Videos_table.php';

		$manager = new UserManager();
		$data = $manager->getUserList();
		$data_for_tree = UserRelation_table::getAllFatherUsersList('en');

		$inTreeId = array();
		foreach ($data_for_tree as $key_inTree => $allUsersval) {
			$inTreeId[] = $allUsersval['id'];
		}
		
		foreach ($data as $key => $allUsersval) {
			$data[$key]['fileUsed'] = ceil((($data[$key]['UsedFileSize']+$data[$key]['UsedVideoZise'])/1024)/1024);
			//unset($data[$key]['UsedFileSize']);
			//unset($data[$key]['UsedVideoZise']);
			
			if (in_array($allUsersval['user_id'], $inTreeId, true)) {
				$data[$key]['inTree'] = 1;
			}
			else {
				$data[$key]['inTree'] = 0;
			}
		}

		$returnValue = array();
		$returnValue["type"] = 0;
		$returnValue["value"] = $data;

		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "getUserListForTree" ){
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';
		require_once Config::getBaseDir() . 'manager/user/UserManager.php';
	
		$manager = new UserManager();
		$data = $manager->getUserListForTree();
	
		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = $data;
	
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "getTreeAllUsersList"){
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';
		require_once Config::getBaseDir() . 'manager/user/UserManager.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserRelation_table.php';

		$langManager = new LanguageManager();

		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$users_list = UserRelation_table::getAllFatherUsersList( $langManager->getCurrentLanguage() );
		$returnValue[ "value" ] = $users_list;
		if(empty($users_list)){
			$returnValue[ "type" ] = 1;
		}

		echo json_encode( $returnValue );

	}	else
	if( $_REQUEST[ "action" ] == "getUserData" ){
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserRelation_table.php';
		require_once Config::getBaseDir() . 'database_layer/tables/CountryList_table.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserRelation_table.php';
		require_once Config::getBaseDir() . 'database_layer/tables/AlbumPicture_table.php';
		require_once Config::getBaseDir() . 'database_layer/tables/Album_table.php';
		require_once Config::getBaseDir() . 'database_layer/tables/Videos_table.php';
		require_once Config::getBaseDir() . 'manager/user/UserManager.php';
		require_once Config::getBaseDir() . 'manager/image/ImageManager.php';


		$manager = new UserManager();
		$langManager = new LanguageManager();
		$imageManager = new \Image\ImageManager(Config::getDataDir(), Config::getUserPhotoDirName(), Config::getPhotoStylesDirName());


		$data = $manager->getUserData( $_REQUEST[ "id" ] );
		if(!empty($data['profile_picture'])){

			try{
				$data["user_avatar"] = Config::getDataDirUrl() . $imageManager->generateAndGetImageByStyle($data['profile_picture'], "small");
				$data['avatar_bigPath'] = Config::getDataDirUrl() .$imageManager->generateAndGetImageByStyle($data['profile_picture'], "large");
			}
			catch(Exception $e){
				//TODO: check exception;
			}
		}

		$child_type = 'child';
		$wife_type = 'couple';
		$data['children'] = $manager->getUserRelatives( $_REQUEST[ "id" ], $child_type );
		if(!empty($data['children'])){
			foreach($data['children'] as $child_key => $child){
				if(!empty($child['profile_picture'])){
					try{
						$data["children"][$child_key]['user_avatar'] = Config::getDataDirUrl() . $imageManager->generateAndGetImageByStyle($child['profile_picture'], "small");
						$data["children"][$child_key]['avatar_bigPath'] = Config::getDataDirUrl() .$imageManager->generateAndGetImageByStyle($child['profile_picture'], "large");
					}
					catch(Exception $e){
						//TODO: check exception;
					}
				}
			}
		}
		$data['couple'] = $manager->getUserRelatives( $_REQUEST[ "id" ], $wife_type );
		if(!empty($data['couple'])){
			foreach($data['couple'] as $couple_key => $couple){
				if(!empty($couple['profile_picture'])){
					try{
						$data["couple"][$couple_key]['user_avatar'] = Config::getDataDirUrl() . $imageManager->generateAndGetImageByStyle($couple['profile_picture'], "small");
						$data["couple"][$couple_key]['avatar_bigPath'] = Config::getDataDirUrl() . $imageManager->generateAndGetImageByStyle($couple['profile_picture'], "large");
					}
					catch(Exception $e){
						//TODO: check exception;
					}
				}
			}
		}

		$picFilesTotal = $manager->getUserPicFileUsed( $_REQUEST[ "id" ] );
		$videoFileTotal = $manager->getUserVideoFileUsed( $_REQUEST[ "id" ] );
		$data['fileUsed'] = ceil((($picFilesTotal+$videoFileTotal)/1024)/1024);
		$data['fileLimit'] = Config::getUserFileUploadLimit();
		$data['max_upload'] = ini_get('upload_max_filesize');
	
		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = $data;
		$returnValue['included_in_tree'] = 0;
	    // If user listed in tree or is a first user
		if(UserRelation_table::is_user_exists($_REQUEST[ "id" ]) or $_REQUEST[ "id" ] == 1){
			$returnValue['included_in_tree'] = 1;
		}

		echo json_encode( $returnValue );
	}
	if( $_REQUEST[ "action" ] == "removeRelatives" ){
		require_once Config::getBaseDir() . 'database_layer/tables/UserRelation_table.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';
		require_once Config::getBaseDir() . 'manager/user/UserManager.php';

		$manager = new UserManager();
		$manager_info = new UserManager();
		$returnValue = array();
		//$returnValue['type'] = 1;

		$user_info = $manager_info->removeUserRelationInfo( $_REQUEST[ "rel_user_id" ] );
		$user_rel = $manager->removeUserRelation( $_REQUEST[ "user_id" ], $_REQUEST[ "rel_user_id" ] );

		//if($user_info && $user_rel) {
			$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = 1;
		//}

		echo json_encode( $returnValue );
	}
	if( $_REQUEST[ "action" ] == "forgetPassword" ){

		require_once Config::getBaseDir() . 'manager/user/UserManager.php';
		require_once Config::getBaseDir() . 'manager/article/ArticleManager.php';
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';

		require_once Config::getBaseDir() . "lib/mail/class.smtp.php";
		require_once Config::getBaseDir() . "lib/mail/class.phpmailer.php";

		$user_mail = $_REQUEST[ "data" ];

		$manager = new UserManager();
		$list = $manager->getUsersByEmail( $user_mail );
		$lang = $_REQUEST[ "lang" ];
		$string = file_get_contents('../languages/'.$lang.'.json');
		$json_arr = json_decode($string, true);

		$returnValue = array();
		$returnValue[ "type" ] = 1;
		if(!empty($list) && count($list) == 1) {
			$returnValue[ "type" ] = 0;

			$user_id = $list[0]['user_id'];
			$recover_link = $manager->updateUserRecoverPass( $user_id );
			$list[0][ "recover-full-link" ] = Config::getBaseURL().$recover_link;
			$data[ "name" ] = $list[0][ "first_name" ];
			$data[ "email" ] = $user_mail;
			$data[ "phone" ] = $list[0][ "phone" ];
			//$data [ "type" ] = 'Recover-mail';
			$data[ "loginTitle" ] = $json_arr['register_form_login'];
			$data[ "message" ] = $data[ "loginTitle" ].': '.$list[0][ "login" ]."\n".$json_arr['forgot_password_recover_mail_message']."\n".$list[0][ "recover-full-link" ];
			$data[ "subject" ] = $json_arr['forgot_password_recover_mail_subject'];

			$manager->sendSystemEmail( $data );
		}

		echo json_encode( $returnValue);
	}else
	if( $_REQUEST[ "action" ] == "getUserData_allLang" ){
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';
		require_once Config::getBaseDir() . 'database_layer/tables/CountryList_table.php';
		require_once Config::getBaseDir() . 'manager/user/UserManager.php';
		require_once Config::getBaseDir() . 'manager/image/ImageManager.php';
	
		$manager = new UserManager();
		$data = $manager->getUserData_allLang( $_REQUEST[ "id" ] );

		if(!empty($data['profile_picture'])){
			$imageManager = new \Image\ImageManager(Config::getDataDir(), Config::getUserPhotoDirName(), Config::getPhotoStylesDirName());
			try{
				$data["user_avatar"] = Config::getDataDirUrl() . $imageManager->generateAndGetImageByStyle($data['profile_picture'], "thumbnail");
			}
			catch(Exception $e){
				//TODO: check expetion;
			}
		}
	
		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = FormatManager::DBtoUI( $data );
	
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "getUserTree" ){
		require_once Config::getBaseDir() . 'database_layer/tables/UserRelation_table.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';
		require_once Config::getBaseDir() . 'database_layer/tables/User_table.php';
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		require_once Config::getBaseDir() . 'manager/user/UserManager.php';
		require_once Config::getBaseDir() . 'manager/image/ImageManager.php';

	  	$upCount = 2;
	  	$downCount = 2;
		$manager = new UserManager();
	  	if (isset($_REQUEST["screen_width"])) {
		  $width = $_REQUEST["screen_width"];
		  if ($width > 1024) {
			$upCount = 2;
			$downCount = 2;
		  }
		  elseif($width > 768) {
			$upCount = 2;
			$downCount = 1;
		  }
		  else{
			$upCount = 1;
			$downCount = 1;
		  }
		}
		
		$value = $manager->getUserTree( $_REQUEST[ "user_id" ], $upCount, $downCount );

		//TODO: CHeck with Lev is it good solution for user avatars
		foreach($value as $key => $user_item){
			if(!empty($user_item['profile_picture'])){
				$imageManager = new \Image\ImageManager(Config::getDataDir(), Config::getUserPhotoDirName(), Config::getPhotoStylesDirName());
				try{
					$value[$key]["user_avatar"] = Config::getDataDirUrl() . $imageManager->generateAndGetImageByStyle($user_item['profile_picture'], "thumbnail_crop", true, true);
				}
				catch(Exception $e){ }
			}
		}
		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = $value;
		
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "changeUserRelation" ){
		require_once Config::getBaseDir() . 'database_layer/tables/UserRelation_table.php';
		require_once Config::getBaseDir() . 'manager/user/UserManager.php';
		
		$manager = new UserManager();
		
		$manager->changeUserRelation( $_REQUEST[ "user_id" ], $_REQUEST[ "rel_user_id" ], $_REQUEST[ "type" ] );
		
		$returnValue = array();
		$returnValue[ "type" ] = 0;
		
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "removeUserRelation" ){
		require_once Config::getBaseDir() . 'database_layer/tables/UserRelation_table.php';
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		require_once Config::getBaseDir() . 'manager/user/UserManager.php';

		$manager = new UserManager();

		$manager->removeUserRelation( $_REQUEST[ "user_id" ], $_REQUEST[ "rel_user_id" ] );

		$returnValue = array();
		$returnValue[ "type" ] = 0;

		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "getUsersByName" ){
			require_once Config::getBaseDir() . 'manager/user/UserManager.php';
    	require_once Config::getBaseDir() . 'manager/article/ArticleManager.php';
    	require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
    	require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';
			require_once Config::getBaseDir() . 'manager/image/ImageManager.php';

    		$search = isset( $_REQUEST[ "search" ] ) ? $_REQUEST[ "search" ] : null;
			$manager = new UserManager();
    		$list = $manager->getUsersByName( $search, array('onlyMembers'));

			foreach($list as $key => $user_item){
				if(!empty($user_item['profile_picture'])){
					$imageManager = new \Image\ImageManager(Config::getDataDir(), Config::getUserPhotoDirName(), Config::getPhotoStylesDirName());
					try{
						$list[$key]["user_avatar"] = Config::getDataDirUrl() . $imageManager->generateAndGetImageByStyle($user_item['profile_picture'], "small");
					}
					catch(Exception $e){ }
				}
			}

    		$returnValue = array();
    		$returnValue[ "type" ] = 0;
    		$returnValue[ "value" ] = $list;

    		echo json_encode( $returnValue );
   }else
	if( $_REQUEST[ "action" ] == "getVisitCount" ){
			require_once Config::getBaseDir() . 'manager/user/UserManager.php';
			$manager = new UserManager();
    		$val = $manager->getVisitCount();

    		$returnValue = array();
    		$returnValue[ "type" ] = 0;
    		$returnValue[ "value" ] = $val;

    		echo json_encode( $returnValue );
   }else
	if( $_REQUEST[ "action" ] == "incrementVisitCount" ){
			require_once Config::getBaseDir() . 'manager/user/UserManager.php';
			$manager = new UserManager();
    		$val = $manager->incrementVisitCount();

    		$returnValue = array();
    		$returnValue[ "type" ] = 0;
    		$returnValue[ "value" ] = $val;

    		echo json_encode( $returnValue );
   }else
//***************************************************************************************ALBUM************************************************************************************//
	if( $_REQUEST[ "action" ] == "createAlbum" ){
		require_once Config::getBaseDir() . 'manager/user/UserManager.php';
		require_once Config::getBaseDir() . 'manager/album/AlbumManager.php';
	
		$manager = new AlbumManager();
		$id = $manager->createAlbum( json_decode( $_REQUEST[ "data" ], true ) );
	
		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = $id;
	
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "editAlbum" ){
		require_once Config::getBaseDir() . 'manager/album/AlbumManager.php';
	
		$data = json_decode( $_REQUEST[ "data" ], true );
		$manager = new AlbumManager();
		$manager->editAlbum( $data[ "id" ], $data );
	
		$returnValue = array();
		$returnValue[ "type" ] = 0;
	
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "removeAlbum" ){
		require_once Config::getBaseDir() . 'database_layer/tables/AlbumPicture_table.php';
		require_once Config::getBaseDir() . 'manager/album/AlbumManager.php';
	
		$manager = new AlbumManager();
		$manager->removeAlbum( $_REQUEST[ "id" ] );
	
		$returnValue = array();
		$returnValue[ "type" ] = 0;
	
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "getAlbum_allLang" ){
		require_once Config::getBaseDir() . 'manager/album/AlbumManager.php';
	
		$manager = new AlbumManager();
		$data = $manager->getAlbum_allLang( $_REQUEST[ "id" ] );
	
		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = FormatManager::DBtoUI( $data );
	
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "getAlbumList" ){
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		require_once Config::getBaseDir() . 'manager/user/UserManager.php';
		require_once Config::getBaseDir() . 'manager/album/AlbumManager.php';
		require_once Config::getBaseDir() . 'manager/image/ImageManager.php';
	
		$manager = new AlbumManager();
		$data = $manager->getAlbumList( (isset( $_REQUEST[ "user_id" ] ) ? $_REQUEST[ "user_id" ] : null) );
		$imageManager = new \Image\ImageManager(Config::getDataDir(), Config::getUserPhotoDirName(), Config::getUserPhotoDirName()."/".Config::getPhotoStylesDirName());

		foreach ($data as $key=>$albumsList){
			$dataPictureList = $manager->getPictureList( $albumsList['id'] );
			if(!empty($dataPictureList)) {
				$fileName = explode("/", end($dataPictureList)['path']);
				$data[$key]['bigPath'] = Config::getDataDirUrl() . $imageManager->generateAndGetImageByStyle($fileName[2], "large");
			}
		}
	
		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = $data;
	
		echo json_encode( $returnValue );
	}
//***************************************************************************************ALBUM-PHOTO************************************************************************************//
	if( $_REQUEST[ "action" ] == "createAlbumPicture" ){
		sleep(5);
		require_once Config::getBaseDir() . 'manager/upload/UploadManager.php';
		require_once Config::getBaseDir() . 'manager/album/AlbumManager.php';
		require_once Config::getBaseDir() . 'manager/image/ImageManager.php';
	
		$data = json_decode( $_REQUEST[ "data" ], true );
		
		$manager = new AlbumManager();
		$id = $manager->createPicture( $data[ "album_id" ], $data );
	
		$returnValue = array();
		if( $id === false ){
			$returnValue[ "type" ] = 1;
		}else{
			$returnValue[ "type" ] = 0;
			$returnValue[ "value" ] = $id;
		}
	
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "editAlbumPicture" ){
		require_once Config::getBaseDir() . 'manager/album/AlbumManager.php';
	
		$data = json_decode( $_REQUEST[ "data" ], true );
		$manager = new AlbumManager();
		$manager->editPicture( $data[ "id" ], $data );
	
		$returnValue = array();
		$returnValue[ "type" ] = 0;
	
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "removeAlbumPicture" ){
		require_once Config::getBaseDir() . 'manager/album/AlbumManager.php';
	
		$manager = new AlbumManager();
		$manager->removePicture( $_REQUEST[ "id" ] );
	
		$returnValue = array();
		$returnValue[ "type" ] = 0;
	
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "getAlbumPicture_allLang" ){
		require_once Config::getBaseDir() . 'manager/album/AlbumManager.php';
	
		$manager = new AlbumManager();
		$data = $manager->getPicture_allLang( $_REQUEST[ "id" ] );
	
		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = FormatManager::DBtoUI( $data );
	
		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "getAlbumPictureList" ){
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		require_once Config::getBaseDir() . 'manager/album/AlbumManager.php';
		require_once Config::getBaseDir() . 'manager/image/ImageManager.php';
	
		$manager = new AlbumManager();
		$imageManager = new \Image\ImageManager(Config::getDataDir(), Config::getUserPhotoDirName(), Config::getUserPhotoDirName()."/".Config::getPhotoStylesDirName());
		$data = $manager->getPictureList( $_REQUEST[ "album_id" ] );

		foreach ($data as $key=>$albumList){
			if(!empty($albumList['path'])){
				$fileName = explode("/", $albumList['path']);
				try{
					$data[$key]['path'] = Config::getDataDirUrl() .$imageManager->generateAndGetImageByStyle($fileName[2], "small");
					$data[$key]['bigPath'] = Config::getDataDirUrl() .$imageManager->generateAndGetImageByStyle($fileName[2], "medium_height");
					$data[$key]['originPath'] = Config::getDataDirUrl().$fileName[1].'/'.$fileName[2];
				}
				catch(Exception $e){
					//TODO: check exception;
				}
			}
		}

		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = $data;
	
		echo json_encode( $returnValue );
	}
//***************************************************************************************Videos************************************************************************************//
	if( $_REQUEST[ "action" ] == "createNewVideo" ){
		require_once Config::getBaseDir() . 'manager/upload/UploadManager.php';
		require_once Config::getBaseDir() . 'manager/video/VideoManager.php';

		$data = json_decode( $_REQUEST[ "data" ], true );

		$manager = new VideoManager();
		$id = $manager->createVideo( $data[ "user_id" ], $data );

		$returnValue = array();
		if( $id === false ){
			$returnValue[ "type" ] = 1;
		}else{
			$returnValue[ "type" ] = 0;
			$returnValue[ "value" ] = $id;
		}

		echo json_encode( $returnValue );
	}if( $_REQUEST[ "action" ] == "getVideoList" ){
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		require_once Config::getBaseDir() . 'manager/user/UserManager.php';
		require_once Config::getBaseDir() . 'manager/video/VideoManager.php';
		
		$manager = new VideoManager();
		$data = $manager->getVideoList( (isset( $_REQUEST[ "user_id" ] ) ? $_REQUEST[ "user_id" ] : null) );

		$doc = new DOMDocument();
		
		foreach ($data as &$item) {
			if($item['embed_code']) {
				@$doc->loadHTML($item['embed_code']);
				$tags = $doc->getElementsByTagName('iframe');
				foreach ($tags as $tag) {
					if (strpos($tag->getAttribute('src'), 'youtube') !== false) {
						$item['embed_type'] = 'youtube';
						$url = $tag->getAttribute('src');
						$pattern = '#^(?:https?://)?(?:www\.)?(?:youtu\.be/|youtube\.com(?:/embed/|/v/|/watch\?v=|/watch\?.+&v=))([\w-]{11})(?:.+)?$#x';
						preg_match($pattern, $url, $matches);
						$item['embed_videoID'] = (isset($matches[1])) ? $matches[1] : false;
					}
					else if (strpos($tag->getAttribute('src'), 'vimeo') !== false) {
						$item['embed_type'] = 'vimeo';

						$url = $tag->getAttribute('src');
						$item['embed_videoID'] = substr(parse_url($url, PHP_URL_PATH), 7);
						$hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/".$item['embed_videoID'].".php"));
						$tt = (explode("_640",$hash[0]['thumbnail_large']));
						$item['vimeo_thumb'] = $tt[0].'_740'.$tt[1];
						//$item['vimeo_thumb'] = $hash[0]['thumbnail_large'];
					}
					else if (strpos($tag->getAttribute('src'), 'rutube') !== false) {
						$item['embed_type'] = 'rutube';
						$url = $tag->getAttribute('src');
						$item['embed_videoID'] = substr(parse_url($url, PHP_URL_PATH), 12);

					}
					else {
						$item['embed_type'] = 'other';
						$item['embed_videoID'] = false;
					}
				}
			}
		}

		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = $data;

		echo json_encode( $returnValue );
	}if( $_REQUEST[ "action" ] == "removeVideoItem" ){
		require_once Config::getBaseDir() . 'manager/video/VideoManager.php';

		$manager = new VideoManager();
		$manager->removeVideoItem( $_REQUEST[ "id" ] );

		$returnValue = array();
		$returnValue[ "type" ] = 0;

		echo json_encode( $returnValue );
	}if( $_REQUEST[ "action" ] == "getVideo_allLang" ){
		require_once Config::getBaseDir() . 'manager/video/VideoManager.php';

		$manager = new VideoManager();
		$data = $manager->getVideo_allLang( $_REQUEST[ "id" ] );

		$returnValue = array();
		$returnValue[ "type" ] = 0;
		$returnValue[ "value" ] = FormatManager::DBtoUI( $data );

		echo json_encode( $returnValue );
	}if( $_REQUEST[ "action" ] == "editVideoInfo" ){
		require_once Config::getBaseDir() . 'manager/video/VideoManager.php';

		$data = json_decode( $_REQUEST[ "data" ], true );
		$manager = new VideoManager();
		$manager->editVideo( $data[ "id" ], $data );

		$returnValue = array();
		$returnValue[ "type" ] = 0;

		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "changePassword" ){
		require_once Config::getBaseDir() . 'manager/user/UserManager.php';
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';

		$data = isset( $_REQUEST[ "id" ] ) ? $_REQUEST[ "id" ] : null;
		$manager = new UserManager();
		$list = $manager->getUsersByUniqueToken( $data );

		$returnValue = array();
		$returnValue[ "type" ] = 1;
		if (!empty($list)) {
			$returnValue[ "type" ] = 0;
			$returnValue[ "value" ] = $list[ 0 ]['user_id'];
		}

		echo json_encode( $returnValue );
	}else
	if( $_REQUEST[ "action" ] == "addUserToTreeSubmit" ){
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';
		require_once Config::getBaseDir() . 'manager/user/UserManager.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserRelation_table.php';

		$returnValue = array();
		$returnValue[ "type" ] = 0;

		if(!empty($_REQUEST['current_user_id']) && !empty($_REQUEST['related_user_id'])){
			if(!UserRelation_table::is_user_exists($_REQUEST['current_user_id'])){
				UserRelation_table::setUserRelation($_REQUEST['current_user_id'], $_REQUEST['related_user_id'], UserRelation_table::$relationshipType_father);
			}
			else {
				//User exists;
				$returnValue[ "type" ] = 2;
			}
		}
		else {
			$returnValue[ "type" ] = 1;
		}

		echo json_encode( $returnValue );
	} else
	if( $_REQUEST[ "action" ] == "mergeUserWithTreeUserSubmit"){
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';
		require_once Config::getBaseDir() . 'manager/user/UserManager.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserRelation_table.php';

		$returnValue = array();
		$returnValue[ "type" ] = 0;

		if(!empty($_REQUEST['current_user_id']) && !empty($_REQUEST['related_user_id'])){
			if(!UserRelation_table::is_user_exists($_REQUEST['current_user_id'])){
				UserRelation_table::mergeUsers($_REQUEST['current_user_id'], $_REQUEST['related_user_id']);
			}
			else {
				//User exists;
				$returnValue[ "type" ] = 2;
			}
		}
		else {
			$returnValue[ "type" ] = 1;
		}

		echo json_encode( $returnValue );

	}else
    if( $_REQUEST[ "action" ] == "chatSendFirstMessage"){
      $db = new DBconnection();
      $chat = new chatManager($db->getConnection());
      $nodejs = new Nodejs();
      $returnValue = array();
      $date = new DateTime(null);
      $time = $date->getTimestamp();
      $sender = $_REQUEST['sender'];
      $recipient = $_REQUEST['recipient'];
      $msg = $_REQUEST['message'];
      $message_arr = $chat->sendFirstMessage(
        $sender,
        $recipient,
        $msg,
        $time
      );
      if($message_arr) {
        require_once Config::getBaseDir() . 'manager/image/ImageManager.php';
        $imageManager = new \Image\ImageManager(Config::getDataDir(), Config::getUserPhotoDirName(), Config::getPhotoStylesDirName());
        $user_info = $chat->getUserChatInfo($sender);
        if(!is_null($user_info['profile_picture'])) {
          $user_info['profile_picture'] = Config::getDataDirUrl() . $imageManager->generateAndGetImageByStyle($user_info['profile_picture'], "thumbnail");
        }
        $data = array(
          'message_id' => $message_arr['message_id'],
          'sender' => $sender,
          'room' => $message_arr['room_id'],
          'room_type' => 'user',
          'message' => $msg,
          'time' => $time,
          'chat_info' => array(
            'name' => array(
              'en' => $user_info['first_name_en'] . ' ' . $user_info['last_name_en'],
              'ru' => $user_info['first_name_ru'] . ' ' . $user_info['last_name_ru'],
              'am' => $user_info['first_name_am'] . ' ' . $user_info['last_name_am'],
          ),
          'picture' => $user_info['profile_picture'],
          ),
        );
        $channel = 'room_' . $message_arr['room_id'];
        $check_channel = $nodejs->checkChannel($channel);
        if(empty($check_channel->result)) {
          $nodejs->addChannel($channel);
          $nodejs->addUserToChannel($sender, $channel);
          $nodejs->addUserToChannel($recipient, $channel);
        }
        $message = (object) array(
          'channel' => $channel,
          'broadcast' => FALSE,
          'type' => 'newMessage',
          'data' => json_encode($data),
        );
        
        $nodejs->sendMessage($message);
      }
      $returnValue['type'] = 0;
      $returnValue['status'] = ($message_arr) ? 1 : 0;
      echo json_encode( $returnValue );
    } else
    if( $_REQUEST[ "action" ] == "chatSendMessage"){
      $db = new DBconnection();
      $chat = new chatManager($db->getConnection());
      $nodejs = new Nodejs();
      $date = new DateTime(null);
      $time = $date->getTimestamp();
      $sender = $_REQUEST['sender'];
      $room = $_REQUEST['room'];
      $room_type = $_REQUEST['room_type'];
      $msg = $_REQUEST['message'];
      $message_id = $chat->sendMessage(
        $sender,
        $room,
        $msg,
        $time
      );
      $message_js_id = isset($_REQUEST['message_js_id']) ? $_REQUEST['message_js_id'] : null;
      if($message_id) {
        require_once Config::getBaseDir() . 'manager/image/ImageManager.php';
        $imageManager = new \Image\ImageManager(Config::getDataDir(), Config::getUserPhotoDirName(), Config::getPhotoStylesDirName());
        $user_info = $chat->getUserChatInfo($sender);
        if(!is_null($user_info['profile_picture'])) {
          $user_info['profile_picture'] = Config::getDataDirUrl() . $imageManager->generateAndGetImageByStyle($user_info['profile_picture'], "thumbnail");
        }
        $data = array(
          'message_id' => $message_id,
          'message_js_id' => $message_js_id,
          'sender' => $sender,
          'room' => $room,
          'room_type' => $room_type,
          'message' => $msg,
          'time' => $time,
          'chat_info' => array(
            'name' => array(
              'en' => $user_info['first_name_en'] . ' ' . $user_info['last_name_en'],
              'ru' => $user_info['first_name_ru'] . ' ' . $user_info['last_name_ru'],
              'am' => $user_info['first_name_am'] . ' ' . $user_info['last_name_am'],
            ),
            'picture' => $user_info['profile_picture'],
          ),
        );
        if($room_type == 'group') {
          $data['chat_info']['chat_name'] = $chat->getGroupChatName($room, $sender);
        }
        $message = (object) array(
          'channel' => 'room_' . $room,
          'broadcast' => FALSE,
          'type' => 'newMessage',
          'data' => json_encode($data),
        );
        $nodejs->sendMessage($message);
      }
      $returnValue = array(
        'message_id' => ($message_id) ? $message_id : null,
        'message_js_id' => $message_js_id,
        'type' => 0,
        'status' => ($message_id) ? 1 : 0,
        'time' => $time,
      );
      echo json_encode( $returnValue );
    }else
    if( $_REQUEST[ "action" ] == "getUserChatrooms"){
      $db = new DBconnection();
      $chat = new chatManager($db->getConnection());
      require_once Config::getBaseDir() . 'manager/image/ImageManager.php';
      $returnValue = array();
      $returnValue['type'] = 0;
      $chatrooms = $chat->getUserChatrooms($_REQUEST['user_id']);
      $imageManager = new \Image\ImageManager(Config::getDataDir(), Config::getUserPhotoDirName(), Config::getPhotoStylesDirName());
      foreach($chatrooms as &$chatroom){
        if(!is_null($chatroom['picture'])){
          $chatroom['picture'] = Config::getDataDirUrl() . $imageManager->generateAndGetImageByStyle($chatroom['picture'], "thumbnail");
        }
      }
      $returnValue['chatrooms'] = $chatrooms;
      echo json_encode( $returnValue );
    } else
    if( $_REQUEST[ "action" ] == "getChatHistory"){
      $db = new DBconnection();
      $chat = new chatManager($db->getConnection());
      require_once Config::getBaseDir() . 'manager/image/ImageManager.php';
      $returnValue = array();
      $returnValue['type'] = 0;
      $history = $chat->getChatroomHistory($_REQUEST['room_id'], intval($_REQUEST['offset']), intval($_REQUEST['limit']));
      $imageManager = new \Image\ImageManager(Config::getDataDir(), Config::getUserPhotoDirName(), Config::getPhotoStylesDirName());
      foreach($history as &$message){
        if(!is_null($message['user_info']['picture'])){
          $message['user_info']['picture'] = Config::getDataDirUrl() . $imageManager->generateAndGetImageByStyle($message['user_info']['picture'], "thumbnail");
        }
        $message['status'] = $message['seen'] ? 'seen' : 'unseen';
      }
      $returnValue['history'] = $history;
      echo json_encode( $returnValue );
    } else
    if( $_REQUEST[ "action" ] == "getChatGroupMembers"){
      $db = new DBconnection();
      $chat = new chatManager($db->getConnection());
      $returnValue = array();
      $members = $chat->getChatGroupMembers($_REQUEST['room_id'], array($_REQUEST['user_id']));
      $returnValue['members'] = $members;
      $name = $chat->getGroupChatName($_REQUEST['room_id'], $_REQUEST['user_id']);
      $returnValue['name'] = $name;
      echo json_encode( $returnValue );
    } else
    if( $_REQUEST[ "action" ] == "setChatSeen"){
      $db = new DBconnection();
      $chat = new chatManager($db->getConnection());
      $nodejs = new Nodejs();
      $user_id = $_REQUEST['user_id'];
      $room_id = $_REQUEST['room_id'];
      $set_seen = $chat->setRoomMessagesSeen($user_id, $room_id);
      $returnValue = array();
      $returnValue['type'] = 0;
      if($chat->getRoomType($room_id) == 'user') {
        $data = array(
          'room' => $room_id,
        );
        $message = (object) array(
          'channel' => 'room_' . $room_id,
          'broadcast' => FALSE,
          'type' => 'seenChat',
          'data' => json_encode($data),
        );
        $nodejs->sendMessage($message);
      }
      echo json_encode( $returnValue );
    } else
    if( $_REQUEST[ "action" ] == "setMessageSeen"){
      $db = new DBconnection();
      $chat = new chatManager($db->getConnection());
      $nodejs = new Nodejs();
      $user_id = $_REQUEST['user_id'];
      $message_id = $_REQUEST['message_id'];
      $message_js_id = $_REQUEST['message_js_id'];
      $room_id = $_REQUEST['room_id'];
      $set_seen = $chat->setMessageSeen($user_id, $message_id);
      $returnValue = array();
      $returnValue['type'] = 0;
      if($chat->getRoomType($room_id) == 'user') {
        $data = array(
          'message_id' => $message_id,
          'message_js_id' => $message_js_id,
          'room' => $room_id,
        );
        $message = (object) array(
          'channel' => 'room_' . $room_id,
          'broadcast' => FALSE,
          'type' => 'seenMessage',
          'data' => json_encode($data),
        );
        $nodejs->sendMessage($message);
      }
      echo json_encode( $returnValue );
    }else
    if( $_REQUEST[ "action" ] == "chatGetUsersList"){
      $db = new DBconnection();
      $chat = new chatManager($db->getConnection());
      $returnValue['list'] = $chat->getUsersForGroup($_REQUEST['room'], $_REQUEST['type']);
      echo json_encode( $returnValue );
    }else
    if( $_REQUEST[ "action" ] == "chatCreateGroup"){
      $db = new DBconnection();
      $chat = new chatManager($db->getConnection());
      $nodejs = new Nodejs();
      $user_id = $_REQUEST['user_id'];
      $members = array_merge(explode(',', $_REQUEST['members']), $chat->getChatroomUsers($_REQUEST['room_id']));
      $room = $chat->createChatroom($user_id, 'group');
      $chat->addUsersToChatroom($room, $members);
      $date = new DateTime(null);
      $time = $date->getTimestamp();
      $sender = $user_id;
      $msg = $_REQUEST['msg'];
      $message_id = $chat->sendMessage(
        $sender,
        $room,
        $msg,
        $time
      );
      if($message_id) {
        require_once Config::getBaseDir() . 'manager/image/ImageManager.php';
        $imageManager = new \Image\ImageManager(Config::getDataDir(), Config::getUserPhotoDirName(), Config::getPhotoStylesDirName());
        $user_info = $chat->getUserChatInfo($sender);
        if(!is_null($user_info['profile_picture'])) {
          $user_info['profile_picture'] = Config::getDataDirUrl() . $imageManager->generateAndGetImageByStyle($user_info['profile_picture'], "thumbnail");
        }
        $data = array(
          'message_id' => $message_id,
          'message_js_id' => 0,
          'sender' => $sender,
          'room' => $room,
          'room_type' => 'group',
          'message' => $msg,
          'time' => $time,
          'chat_info' => array(
            'name' => array(
              'en' => $user_info['first_name_en'] . ' ' . $user_info['last_name_en'],
              'ru' => $user_info['first_name_ru'] . ' ' . $user_info['last_name_ru'],
              'am' => $user_info['first_name_am'] . ' ' . $user_info['last_name_am'],
            ),
            'picture' => $user_info['profile_picture'],
            'chat_name' => $chat->getGroupChatName($room, $sender),
          ),
        );
        $channel = 'room_' . $room;
        $check_channel = $nodejs->checkChannel($channel);
        if(empty($check_channel->result)) {
          $nodejs->addChannel($channel);
          foreach($members as $member) {
            $nodejs->addUserToChannel($member, $channel);
          }
        }
        $message = (object) array(
          'channel' => $channel,
          'broadcast' => FALSE,
          'type' => 'newMessage',
          'data' => json_encode($data),
        );
        $nodejs->sendMessage($message);
      }
      $returnValue = array(
        'message_id' => ($message_id) ? $message_id : null,
        'message_js_id' => 0,
        'type' => 0,
        'status' => ($message_id) ? 1 : 0,
        'time' => $time,
      );
      echo json_encode( $returnValue );
    } else
    if( $_REQUEST[ "action" ] == "chatAddUsersToGroup"){
      $db = new DBconnection();
      $chat = new chatManager($db->getConnection());
      $nodejs = new Nodejs();
      $user_id = $_REQUEST['user_id'];
      $members = explode(',', $_REQUEST['members']);
      $room = $_REQUEST['room_id'];
      $chat->addUsersToChatroom($room, $members);
      $date = new DateTime(null);
      $time = $date->getTimestamp();
      $sender = $user_id;
      $msg = $_REQUEST['msg'];
      $message_id = $chat->sendMessage(
        $sender,
        $room,
        $msg,
        $time
      );
      if($message_id) {
        require_once Config::getBaseDir() . 'manager/image/ImageManager.php';
        $imageManager = new \Image\ImageManager(Config::getDataDir(), Config::getUserPhotoDirName(), Config::getPhotoStylesDirName());
        $user_info = $chat->getUserChatInfo($sender);
        if(!is_null($user_info['profile_picture'])) {
          $user_info['profile_picture'] = Config::getDataDirUrl() . $imageManager->generateAndGetImageByStyle($user_info['profile_picture'], "thumbnail");
        }
        $data = array(
          'message_id' => $message_id,
          'message_js_id' => 0,
          'sender' => $sender,
          'room' => $room,
          'room_type' => 'group',
          'message' => $msg,
          'time' => $time,
          'chat_info' => array(
            'name' => array(
              'en' => $user_info['first_name_en'] . ' ' . $user_info['last_name_en'],
              'ru' => $user_info['first_name_ru'] . ' ' . $user_info['last_name_ru'],
              'am' => $user_info['first_name_am'] . ' ' . $user_info['last_name_am'],
            ),
            'picture' => $user_info['profile_picture'],
            'chat_name' => $chat->getGroupChatName($room, $sender),
          ),
        );
        $channel = 'room_' . $room;
        foreach($members as $member) {
          $nodejs->addUserToChannel($member, $channel);
        }
        $message = (object) array(
          'channel' => $channel,
          'broadcast' => FALSE,
          'type' => 'newMessage',
          'data' => json_encode($data),
        );
        $nodejs->sendMessage($message);
      }
      $returnValue = array(
        'message_id' => ($message_id) ? $message_id : null,
        'message_js_id' => 0,
        'type' => 0,
        'status' => ($message_id) ? 1 : 0,
        'time' => $time,
      );
      echo json_encode( $returnValue );
    } else
    if( $_REQUEST[ "action" ] == "removeChatGroupMember"){
      $db = new DBconnection();
      $chat = new chatManager($db->getConnection());
      $nodejs = new Nodejs();
      $date = new DateTime(null);
      $time = $date->getTimestamp();
      $sender = $_REQUEST['author'];
      $room = $_REQUEST['room_id'];
      $msg = $_REQUEST['msg'];
      $members = explode(',', $_REQUEST['users']);
      $room_type = 'group';
      if($sender !== $chat->getChatroomAuthor($room)) {
        return json_encode(array('status' => 0));
      }
      $chat->removeUsersFromChatroom($room, $members);
      $message_id = $chat->sendMessage(
        $sender,
        $room,
        $msg,
        $time
      );
      if($message_id) {
        require_once Config::getBaseDir() . 'manager/image/ImageManager.php';
        $imageManager = new \Image\ImageManager(Config::getDataDir(), Config::getUserPhotoDirName(), Config::getPhotoStylesDirName());
        $user_info = $chat->getUserChatInfo($sender);
        if(!is_null($user_info['profile_picture'])) {
          $user_info['profile_picture'] = Config::getDataDirUrl() . $imageManager->generateAndGetImageByStyle($user_info['profile_picture'], "thumbnail");
        }
        $data = array(
          'message_id' => $message_id,
          'message_js_id' => 0,
          'sender' => $sender,
          'room' => $room,
          'room_type' => 'group',
          'message' => $msg,
          'time' => $time,
          'chat_info' => array(
            'name' => array(
              'en' => $user_info['first_name_en'] . ' ' . $user_info['last_name_en'],
              'ru' => $user_info['first_name_ru'] . ' ' . $user_info['last_name_ru'],
              'am' => $user_info['first_name_am'] . ' ' . $user_info['last_name_am'],
            ),
            'picture' => $user_info['profile_picture'],
            'chat_name' => $chat->getGroupChatName($room, $sender),
          ),
          'info' => array(
            'type' => 'remove_from_room',
            'users' => $members,
          ),
        );
        $channel = 'room_' . $room;
        $message = (object) array(
          'channel' => $channel,
          'broadcast' => FALSE,
          'type' => 'newMessage',
          'data' => json_encode($data),
        );
        $nodejs->sendMessage($message);
        foreach($members as $member) {
          $nodejs->removeUserFromChannel($member, $channel);
        }
      }
      $returnValue = array(
        'message_id' => ($message_id) ? $message_id : null,
        'message_js_id' => 0,
        'type' => 0,
        'status' => ($message_id) ? 1 : 0,
        'time' => $time,
      );
      echo json_encode( $returnValue );
    } else
    if( $_REQUEST[ "action" ] == "removeChatGroup"){
      $db = new DBconnection();
      $chat = new chatManager($db->getConnection());
      $nodejs = new Nodejs();
      $author = $_REQUEST['author'];
      $room = $_REQUEST['room_id'];
      if($author !== $chat->getChatroomAuthor($room)) {
        return json_encode(array('status' => 0));
      }
      $group_members = $chat->getChatroomUsers($room);
      $chat->removeChatroom($room);
      $data = array(
        'room' => $room,
        'info' => array(
          'type' => 'remove_from_room',
          'users' => $group_members,
        ),
      );
      $channel = 'room_' . $room;
      $message = (object) array(
        'channel' => $channel,
        'broadcast' => FALSE,
        'type' => 'newMessage',
        'data' => json_encode($data),
      );
      if($nodejs->sendMessage($message)) {
       // $nodejs->removeChannel($channel);
      }
      $returnValue = array('status' => 1);
      echo json_encode( $returnValue );
    } else
    if( $_REQUEST[ "action" ] == "renameChatGroup"){
      $db = new DBconnection();
      $chat = new chatManager($db->getConnection());
      $nodejs = new Nodejs();
      $date = new DateTime(null);
      $time = $date->getTimestamp();
      $sender = $_REQUEST['user'];
      $room = $_REQUEST['room_id'];
      $msg = $_REQUEST['msg'];
      $room_type = 'group';
      $name = $chat->setGroupChatName($room, $_REQUEST['name']);
      $message_id = $chat->sendMessage(
        $sender,
        $room,
        $msg,
        $time
      );
      if($message_id) {
        require_once Config::getBaseDir() . 'manager/image/ImageManager.php';
        $imageManager = new \Image\ImageManager(Config::getDataDir(), Config::getUserPhotoDirName(), Config::getPhotoStylesDirName());
        $user_info = $chat->getUserChatInfo($sender);
        if(!is_null($user_info['profile_picture'])) {
          $user_info['profile_picture'] = Config::getDataDirUrl() . $imageManager->generateAndGetImageByStyle($user_info['profile_picture'], "thumbnail");
        }
        $data = array(
          'message_id' => $message_id,
          'message_js_id' => 0,
          'sender' => $sender,
          'room' => $room,
          'room_type' => 'group',
          'message' => $msg,
          'time' => $time,
          'chat_info' => array(
            'name' => array(
              'en' => $user_info['first_name_en'] . ' ' . $user_info['last_name_en'],
              'ru' => $user_info['first_name_ru'] . ' ' . $user_info['last_name_ru'],
              'am' => $user_info['first_name_am'] . ' ' . $user_info['last_name_am'],
            ),
            'picture' => $user_info['profile_picture'],
            'chat_name' => $name,
          ),
        );
        $message = (object) array(
          'channel' => 'room_' . $room,
          'broadcast' => FALSE,
          'type' => 'newMessage',
          'data' => json_encode($data),
        );
        $nodejs->sendMessage($message);
      }
      $returnValue = array(
        'message_id' => ($message_id) ? $message_id : null,
        'message_js_id' => 0,
        'type' => 0,
        'status' => ($message_id) ? 1 : 0,
        'time' => $time,
      );
      echo json_encode( $returnValue );
    } else
    if( $_REQUEST[ "action" ] == "chatAttachFile"){
      require_once Config::getBaseDir() . 'manager/upload/UploadManager.php';
      $uploadManager = new UploadManager();
      $fName = $uploadManager->saveUploadedFile( Config::getUploadFileDir(), $uploadManager->generateRandomString() );
      $returnValue = array();
      $returnValue["status"] = 1;
      $returnValue["file"] = Config::getBaseURL() . Config::getUploadFilePath() . $fName;
      $returnValue["room"] = $_REQUEST['room'];
      echo json_encode( $returnValue );
    } else
	if( $_REQUEST[ "action" ] == "deleteUserFromTree"){
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';
		require_once Config::getBaseDir() . 'manager/user/UserManager.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserRelation_table.php';

		$returnValue = array();
		$returnValue[ "type" ] = 0;

		$manager = new UserManager();

		if(!empty($_REQUEST['current_user_id'])){
			if(UserRelation_table::is_user_exists($_REQUEST['current_user_id'])){
				if(!UserRelation_table::is_user_has_children($_REQUEST['current_user_id'])){
					UserRelation_table::removeUserRelation($_REQUEST['current_user_id'], null, 'father');
				}
				else {
					$returnValue[ "type" ] = 2;
				}
			}
			else {
				$returnValue[ "type" ] = 3;
			}
		}
		else {
			$returnValue[ "type" ] = 1;
		}

		echo json_encode( $returnValue );
	}
	if( $_REQUEST[ "action" ] == "getUsersByBirthday" ){
		require_once Config::getBaseDir() . 'manager/user/UserManager.php';
		require_once Config::getBaseDir() . 'manager/language/LanguageManager.php';
		require_once Config::getBaseDir() . 'database_layer/tables/UserInfo_table.php';
		
		$manager = new UserManager();
		$list = $manager->getUsersByBirthday( );

		$tz = date_default_timezone_get();
		$dateTimeZone = new DateTimeZone($tz);
		$dateTimeZ = new DateTime("now", $dateTimeZone);

		$returnValue = array();
		$returnValue[ "type" ] = 0;
		if (!empty($list)) {
			$returnValue[ "type" ] = 1;
			$returnValue[ "value" ] = $list;
			$returnValue[ "tz" ] = $dateTimeZ->format('d m Y, H:i');
		}

		echo json_encode( $returnValue );
	}
}