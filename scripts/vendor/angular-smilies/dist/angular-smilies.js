/*!
 * Angular Smilies 1.3.0
 * Copyright 2014-2016 Damien "Mistic" Sorel (http://www.strangeplanet.fr)
 * Licensed under MIT (http://opensource.org/licenses/MIT)
 */
(function(){
    'use strict';

    var
    main = "smile",
    
    smilies = [
      "smiley",
      "smile",
      "laughing",
      "blush",
      "relaxed",
      "smirk",
      "pensive",
      "heart_eyes",
      "kissing_heart",
      "kissing_closed_eyes",
      "flushed",
      "relieved",
      "bowtie",
      "grin",
      "wink",
      "winky_face",
      "stuck_out_tongue_closed_eyes",
      "grinning",
      "kissing",
      "kissing_smiling_eyes",
      "stuck_out_tongue",
      "sleeping",
      "worried",
      "frowning",
      "anguished",
      "surprised",
      "confused",
      "hushed",
      "unamused",
      "sweat_smile",
      "sweat",
      "disappointed_relieved",
      "weary",
      "disappointed",
      "confounded",
      "fearful",
      "cold_sweat",
      "persevere",
      "cry",
      "sob",
      "joy",
      "scream",
      "tired_face",
      "angry",
      "rage",
      "triumph",
      "sleepy",
      "yum",
      "mask",
      "sunglasses",
      "dizzy_face",
      "smiling_imp",
      "no_mouth",
      "innocent",
      "neutral",
      "alien",
      "heart",
      "star",
      "poop",
      "facepunch",
      "wave",
      "pray",
      "couple",
      "family",
      "dancer",
      "dancers",
      "sunny",
      "moon",
      "umbrella",
      "cloud",
      "rainbow",
      "snowflake",
      "snowman",
      "zap",
      "cyclone",
      "foggy",
      "ocean",
      "car",
      "airplane",
      "computer",
      "tv",
      "iphone",
      "phone",
      "alarm_clock",
      "watch",
      "radio",
      "satellite",
      "mag",
      "lock",
      "email",
      "smoking",
      "bomb",
      "gun",
      "soccer",
      "trophy",
      "calendar",
      "books",
      "evergreen_tree",
      "gift",
      "balloon",
      "tada",
      "bouquet",
      "cherry_blossom",
      "tulip",
      "four_leaf_clover",
      "rose",
      "sunflower",
      "cat",
      "dog",
      "mouse",
      "hamster",
      "rabbit",
      "wolf",
      "frog",
      "tiger",
      "koala",
      "bear",
      "pig",
      "cow",
      "boar",
      "monkey",
      "racehorse",
      "camel",
      "sheep",
      "elephant",
      "panda_face",
    ],
  shorts = {":D":"laughing",":-D":"laughing",":S":"confused",":-S":"confused",";(":"cry",";-(":"cry","OO":"dizzy_face","<3":"thumbsup","&lt;3":"thumbsup","^^":"grinning",":|":"neutral",":-|":"neutral",":P":"stuck_out_tongue",":-P":"stuck_out_tongue",":(":"pensive",":-(":"pensive",":)":"smile",":-)":"smile",":O":"surprised",":-O":"surprised",";)":"wink",";-)":"wink"},

    regex = new RegExp(':(' + smilies.join('|') + '):', 'g'),
    template = '<i class="smiley-$1" title="$1"></i>',

    escapeRegExp = function(str) {
        return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    },

    regExpForShort = function(str) {
        if (/^[a-z]+$/i.test(str)) { // use word boundaries if emoji is letters only
            return new RegExp('\\b'+ str +'\\b', 'gi');
        }
        else {
            return new RegExp(escapeRegExp(str), 'gi');
        }
    },

    templateForSmiley = function(str) {
        return template.replace('$1', str);
    },

    apply = function(input) {
        if (!input) return '';

        var output = input.replace(regex, template);

        for (var sm in shorts) {
            if (shorts.hasOwnProperty(sm)) {
                output = output.replace(regExpForShort(sm), templateForSmiley(shorts[sm]));
            }
        }

        return output;
    };


    angular.module('angular-smilies', [])
    /* smilies parser filter */
    .filter('smilies', function() {
        return apply;
    })
    /* smilies parser attribute */
    .directive('smilies', function() {
        return {
            restrict: 'A',
            scope: {
                source: '=smilies'
            },
            link: function($scope, el) {
                el.html(apply($scope.source));
            }
        };
    })
    /* smilies selector directive */
    .directive('smiliesSelector', ['$timeout', function($timeout) {
        var templateUrl;
        try {
            angular.module('ui.bootstrap.popover');
            templateUrl = 'template/smilies/button-a.html';
        }
        catch(e) {
            try {
                angular.module('mgcrea.ngStrap.popover');
                templateUrl = 'template/smilies/button-b.html';
            }
            catch(e) {
                console.error('No Popover module found');
                return {};
            }
        }

        return {
            restrict: 'A',
            templateUrl: templateUrl,
            scope: {
                source: '=smiliesSelector',
                placement: '@smiliesPlacement',
                title: '@smiliesTitle',
                keepOpen: '@smiliesKeepOpen',
                text: '@smiliesText',
            },
            link: function($scope, el) {
                $scope.smilies = smilies;
                if($scope.source === undefined) {
                  $scope.source = '';
                }
                $scope.append = function(smiley) {
                    $scope.source += ' :'+smiley+': ';
                    if ($scope.keepOpen === undefined) {
                        $timeout(function() {
                            el.children('i').trigger('click');  // close the popover
                        });    
                    }
                };
            }
        };

    }])
    /* helper directive for input focusing */
    .directive('focusOnChange', function() {
        return {
            restrict: 'A',
            link: function($scope, el, attrs) {
                $scope.$watch(attrs.focusOnChange, function(old_val, new_val) {
                  if(typeof new_val !== 'undefined' && new_val.length > 0) {
                    el[0].focus();
                  }
                });
            }
        };
    })
    /* popover template */
    .run(['$templateCache', function($templateCache) {
        $templateCache.put('template/smilies/button-a.html',
            '<i class="smiley-'+ main +' smilies-selector" '+
                'popover-template="\'template/smilies/popover-a.html\'" '+
                'popover-placement="{{!placement && \'left\' || placement}}" '+
                'popover-trigger="click"' +
                'popover-append-to-body="true"' +
                'popover-class="popover-smilies"' +
                'popover-title="{{title}}">{{text}}</i>'
        );
        $templateCache.put('template/smilies/button-b.html',
            '<i class="smiley-'+ main +' smilies-selector" bs-popover '+
                'data-template="template/smilies/popover-b.html" '+
                'data-placement="{{!placement && \'left\' || placement}}" '+
                'title="{{title}}"></i>'
        );
        $templateCache.put('template/smilies/popover-a.html',
            '<div ng-model="smilies" class="smilies-selector-content">'+
              '<i class="smiley-{{smiley}}" ng-repeat="smiley in smilies" ng-click="append(smiley)"></i>'+
            '</div>'
        );
        $templateCache.put('template/smilies/popover-b.html',
            '<div class="popover" tabindex="-1">'+
                '<div class="arrow"></div>'+
                '<h3 class="popover-title" ng-bind-html="title" ng-show="title"></h3>'+
                '<div class="popover-content">'+
                    $templateCache.get('template/smilies/popover-a.html') +
                '</div>'+
            '</div>'
        );
    }]);

}());
