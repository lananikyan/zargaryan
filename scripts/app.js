var familyApp = angular.module('familyApp', [
        'ngCookies',
        'ngSanitize',
        'ui.bootstrap',
        'ngRoute',
        'pascalprecht.translate',
        'textAngular',
        'validation.match',
        'angularTree',
        'ngTouch',
        'angular.filter',
        'luegg.directives',
        'ngAnimate',
        'angular-smilies',
        'localytics.directives',
        'angular-slideout'
    ])
    .config(['$controllerProvider', function($controllerProvider) {
        $controllerProvider.allowGlobals();
    }])
    .config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
        $routeProvider
            .when('/login',{
                templateUrl: 'views/login.html',
                controller: 'LoginController',
                loginRequired:false,
                anonymous:  true,
                resolve: {
                  page: function ($route) {
                    $route.current.params.page = 'login';
                  }
                }
            })
            .when('/register',{
                templateUrl: 'views/register.html',
                controller: 'RegisterController',
                loginRequired:false,
                anonymous:  true,
                resolve: {
                  page: function ($route) {
                    $route.current.params.page = 'register';
                  }
                }
            })
            .when('/mainPage', {
                templateUrl: 'views/mainpage/index.html',
                controller: 'MainPageController',
                loginRequired:true,
                isMainPage:true,
                resolve: {
                  page: function ($route) {
                    $route.current.params.page = 'mainPage';
                  }
                }
            })
            .when('/mainPage/:type', {
                templateUrl: 'views/mainpage/detail.html',
                controller: 'MainPageController',
                loginRequired:false,
                resolve: {
                  page: function ($route) {
                    $route.current.params.page = 'mainPage';
                  }
                }
            })
          /*  .when('/mainPage/info/:id', {
                templateUrl: 'views/mypage/index.html',
                controller: 'MyPageController',
                loginRequired:false
            })*/
            .when('/admin/:type?/:part?', {
                templateUrl: 'views/admin.html',
                controller: 'AdminController',
                loginRequired:true,
                adminRequired:true,
                resolve: {
                  page: function ($route) {
                    $route.current.params.page = 'admin';
                  }
                }
            })
            .when('/contactUs', {
                templateUrl: 'views/contactUs.html',
                controller: 'ContactUsController',
                loginRequired:false,
                resolve: {
                  page: function ($route) {
                    $route.current.params.page = 'contactUs';
                  }
                }
            })
            .when('/entertainingBook', {
                templateUrl: 'views/entertainingBook/index.html',
                controller: 'EntertainingBookController',
                loginRequired:true,
                resolve: {
                  page: function ($route) {
                    $route.current.params.page = 'entertainingBook';
                  }
                }
            })
            .when('/familyTree', {
                templateUrl: 'views/familyTree/index.html',
                controller: 'FamilyTreeController',
                loginRequired:true,
                resolve: {
                  page: function ($route) {
                    $route.current.params.page = 'familyTree';
                  }
                }
            })
            .when('/guessBook', {
                templateUrl: 'views/guessBook/index.html',
                controller: 'GuessBookController',
                loginRequired:true,
                resolve: {
                  page: function ($route) {
                    $route.current.params.page = 'guessBook';
                  }
                }
            })
            .when('/chat', {
                templateUrl: 'views/chat/index.html',
                controller: 'ChatController',
                loginRequired:true,
                resolve: {
                  page: function ($route) {
                    $route.current.params.page = 'chat';
                  }
                }
            })
            .when('/profile', {
                templateUrl: 'views/mypage/index.html',
                controller: 'MyPageController',
                loginRequired:true,
                resolve: {
                  page: function ($route) {
                    $route.current.params.page = 'myPage';
                  }
                }
            })
            .when('/profile/:id', {
                templateUrl: 'views/mypage/index.html',
                controller: 'MyPageController',
                loginRequired:true,
                resolve: {
                  page: function ($route) {
                    $route.current.params.page = 'myPage';
                  }
                }
            })
            .when('/changePassword/:id', {
                templateUrl: 'views/mypage/passwordPopup.html',
                controller: 'PasswordChangeController',
                loginRequired:false,
                resolve: {
                  page: function ($route) {
                    $route.current.params.page = '';
                  }
                }
            })
            .otherwise({
                redirectTo: '/mainPage',
                resolve: {
                  page: function ($route) {
                    $route.current.params.page = 'mainPage';
                  }
                }
            });
            
            $locationProvider.html5Mode(true);
            $locationProvider.hashPrefix('!');
            
    }])
    .config(function($translateProvider) {
        $translateProvider.useStaticFilesLoader({
            prefix: '/languages/',
            suffix: '.json'
        });

    //    $translateProvider.preferredLanguage('am');
        //$translate.use('am')
    })
    .run(function ($rootScope, $location, $route, $routeParams, $http, httpRequestService, rememberMeService, $cookies, socket) {
        var ACTION_GET_CURRENT_USER = "getCurrentUser";
        var ACTION_INCREMENT_VISIT_COUNT = "incrementVisitCount";
        $rootScope.chat_active_room = null;
        $rootScope.currentUser = null;
        $rootScope.socket = socket.connect();
        $rootScope.socket.on('connect', function () {
          httpRequestService.sendAjaxRequest(ACTION_GET_CURRENT_USER,null,null,false,function(data){
            if(data.type == 0){
              $rootScope.currentUser = data.value;
              socket.authenticate($rootScope.socket, $rootScope.currentUser.auth_token);
            }
            else if(data.type == 1) {
              var cookieUser = rememberMeService.getUser();
              console.log(cookieUser);
              if(cookieUser) {
                $rootScope.currentUser = cookieUser;
                rememberMeService.remember($rootScope.currentUser);
                socket.authenticate($rootScope.socket, $rootScope.currentUser.auth_token);
              }
            }
          });
        });
        $rootScope.socket.on('message', function(obj){
          if(obj.type == 'newMessage') {
            var message = $.parseJSON(obj.data);
            if(message.sender != $rootScope.currentUser.id) {
                if ($rootScope.page != 'chat') {
                  $rootScope.currentUser.msg_unseen++;
                }
                else if ($rootScope.chat_active_room != message.room) {
                  $rootScope.currentUser.msg_unseen++;
                }
                $rootScope.$apply();
            }
          }
        });
        $rootScope.emailValidationPattern = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
        var cookie = $cookies.get('visitor');
        if(!cookie ){
            httpRequestService.sendAjaxRequest(ACTION_INCREMENT_VISIT_COUNT,null,null,false,function(data){

            });
            var date = new Date();
            var minutes = 60;
            date.setTime(date.getTime() + (minutes * 60 * 1000));
            $cookies.put('visitor', '1', {'expires': date});
        }

        $rootScope.changeTab = function(tabName){
            $rootScope.heroIndex = Math.floor((Math.random() * 41));
            if ( $rootScope.currentUser == null ) {
                httpRequestService.sendAjaxRequest(ACTION_GET_CURRENT_USER,null,null,false,function(data){
                    if(data && data.type == 0){
                        $rootScope.currentUser = data.value;
                        $rootScope.currentUser.birth_date = $rootScope.currentUser.birth_date != 0 ?new Date( $rootScope.currentUser.birth_date *1000) :undefined;
                    }
                     $location.path( "/" +tabName );
                     if ($(".navbar-collapse.collapse").hasClass('in')) {
                        $(".navbar-toggle").click();
                    }
                });
            }else{
                $location.path( "/" +tabName );
                if ($(".navbar-collapse.collapse").hasClass('in')) {
                    $(".navbar-toggle").click();
                }
            }
        }

        $rootScope.heroIndex = Math.floor((Math.random() * 41));

        // register listener to watch route changes
        $rootScope.$on( "$routeChangeStart", function(event, next, current) {
     
            httpRequestService.sendAjaxRequest(ACTION_GET_CURRENT_USER,null,null,false,function(data){
                if(data && data.type == 0){
                    $rootScope.currentUser = data.value;
                    $rootScope.currentUser.birth_date =$rootScope.currentUser.birth_date != 0 ?new Date( $rootScope.currentUser.birth_date *1000):undefined;
                }
                if ( $rootScope.currentUser == null ) {
                    if ( next.loginRequired  ) {
                        if( !next.isMainPage) {
                          var cookieUser = rememberMeService.getUser();
                            if(!cookieUser) {
                              $location.path( "/login" );
                            }
                            else {
                              rememberMeService.remember(cookieUser);
                            }
                        } 
                        else
                            $location.path( "/mainPage" );
                    }
                }else if(next.adminRequired && $rootScope.currentUser.role != 1 ){
                    $location.path( "/login" );
                }
            });

        });
    });



