familyApp.service('rememberMeService', function($cookies) {
  
  this.remember = function(user) {
    var date_expire = new Date();
    // Epire the cookie after 6 months
    date_expire.setMonth(date_expire.getMonth() + 6);
    $cookies.putObject('currentUser', user, {'expires': date_expire});
  };
  
  this.forget = function() {
    $cookies.remove('currentUser');
  };
  
  this.getUser = function() {
    return $cookies.getObject('currentUser');
  };
});