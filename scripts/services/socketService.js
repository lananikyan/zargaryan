familyApp.service('socket', function($rootScope) {
  this.connect = function() {
    return io.connect(nodejsHost);
  };
  
  this.reconnect = function($socket) {
    $socket.socket.reconnect();
  };

  this.disconnect = function($socket) {
    if ($socket)
      $socket.disconnect();
  };
  
  this.authenticate = function($socket, token) {
    var authMessage = {
      authToken: token
    };
   $socket.emit('authenticate', authMessage);
  };
  
});