
familyApp.service('httpRequestService', function($rootScope, $http,$modal ,$timeout) {
    this.sendAjaxRequest = function(action,getParams, postParam, isPOST,successFunc,failedFunc, showPopup, showSuccessPopup, successMessage){
      if(typeof(showSuccessPopup) == 'undefined') {
        showSuccessPopup = true;
      }
      if (typeof (successMessage) == 'undefined') {
        successMessage = 'success_popup_title';
      }
      var modalInstance;
        if(showPopup){
            modalInstance = $modal.open({
                templateUrl:'./views/request/loader.html',
                windowClass: 'app-modal-small',
                backdrop:'static'
            });
        }
        getParams = ((typeof getParams !== 'undefined' && getParams !== null) ? ("&" + getParams) : "");
        postParam = ((typeof postParam !== 'undefined' && postParam !== null) ? postParam : "");
        isPOST = ((typeof isPOST !== 'undefined' && isPOST !== null) ? isPOST : true);
        
        // Fix for IE
        if (!window.location.origin) {
          window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
        }
        baseUrl = window.location.origin + '/api/';
        var url = baseUrl+ "?action=" + action + getParams;

        var reqParams = {url: url, headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'}, method: 'GET'};
        if( isPOST ){
            if( postParam instanceof FormData ){
                reqParams.headers = {'Content-Type': undefined};
            }
            reqParams.method = 'POST';
            reqParams.data = postParam;
            reqParams.transformRequest = angular.identity;
        }
        $http( reqParams ).success(function(data, status, headers, config) {
            successFunc(data);
            if(modalInstance)
                modalInstance.close();
            var tempUrl;
            if(data.type != 0 && isPOST){
                tempUrl = './views/request/failPopup.html';
            }else if(isPOST){
                tempUrl = './views/request/successPopup.html';
            }
            if(action == "changeCurrentLanguage" || action == "uploadFile")
                tempUrl = undefined;
            if(tempUrl && showSuccessPopup){
              $rootScope.successMessage = successMessage;
                var responseModalInstance = $modal.open({
                    templateUrl:tempUrl,
                    windowClass: 'app-modal-small',
                    scope: $rootScope
                });
                $timeout(function(){responseModalInstance.close();},2000);
            }

        }).error(function(data, status, headers, config) {
            var tempUrl = './views/request/failPopup.html';
            var responseModalInstance = $modal.open({
                templateUrl:tempUrl,
                windowClass: 'app-modal-small'
            });
            $timeout(function(){responseModalInstance.close();},2000);

        });;

    };
});