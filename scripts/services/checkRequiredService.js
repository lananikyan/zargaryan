familyApp.service('checkRequiredService', function($rootScope) {

  this.check = function (form_values, fields) {
    var required = 1;
    if (typeof form_values == 'undefined' || form_values.length == 0) {
      return required;
    }
    var field = fields[0];
    var has_value = false;
    $.each(form_values, function (lang, value) {
      if(value[field] !== undefined && value[field] !== '') {
        has_value = true;
      }
    });
    
    if(has_value) {
      required = 0;
    }

    return required;
  }
  
});