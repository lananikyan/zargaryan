familyApp.filter('ios_emoji', function() {
  return function(input) {
    var output = twemoji.parse(input);
    return output;
  }
});