familyApp.filter('file_thumbnail', function() {
  return function(input) {
    if(input.split('[file]').length == 1) {
      return input;
    }
    var output = '';
    var fileString = input.split('[file]')[1].split('[/file]')[0];
    output += '<a href="' + fileString + '" target="_blank" class="attached-file">';
    var extension = fileString.split('.').pop();
    switch(extension.toLowerCase()) {
        case 'jpg':
        case 'png':
        case 'gif':
        case 'jpeg':
          output += '<img src="' + fileString + '" />';
        break;                         
        case 'zip':
        case 'rar':
        case 'tar':
        case 'gz':
          output += '<i class="fa fa-file-archive-o"></i>';
          output += '<span>' + extension + '</span>';
        break;
        case 'pdf':
          output += '<i class="fa fa-file-pdf-o"></i>';
          output += '<span>' + extension + '</span>';
        break;
        case 'xls':
        case 'xlsx':
          output += '<i class="fa fa-file-excel-o"></i>';
          output += '<span>' + extension + '</span>';
        break;
        case 'doc':
        case 'docx':
          output += '<i class="fa fa-file-word-o"></i>';
          output += '<span>' + extension + '</span>';
        break;
        default:
          output += '<i class="fa fa-file"></i>';
          output += '<span>' + extension + '</span>';
    }
    output += '</a>';
    return output;
  }
});

familyApp.directive('customOnChange', function() {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      var onChangeFunc = scope.$eval(attrs.customOnChange);
      element.bind('change', onChangeFunc);
    }
  };
});
