﻿familyApp.directive('execOnScrollToTop', function () {

  return {

    restrict: 'A',
    link: function (scope, element, attrs) {
      var fn = scope.$eval(attrs.execOnScrollToTop);

      element.on('scroll', function (e) {

        if (e.target.scrollTop > 5 && e.target.scrollTop < 10) {
          scope.$apply(fn);
        }

      });
    }

  };

});