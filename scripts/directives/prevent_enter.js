familyApp.directive('prevententer', function () {
  return {
    restrict: 'A',
    link: function(scope, element) {
      element.bind('keydown keypress keyup', function (event) {
          if(event.keyCode == 13 && !event.shiftKey){
            event.preventDefault();
          }
      });
    }
  };
});