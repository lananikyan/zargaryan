familyApp.controller('LoginController', function($scope,$http,$rootScope, $routeParams, $location, $modal, httpRequestService, rememberMeService, socket, $timeout, $cookies) {
    // Redirect authenticated users to mainpage
    if($rootScope.currentUser) {
      $location.path( "/mainPage" );  
    }
  
    var ACTION_USER_LOGOUT = "userLogout";
    var ACTION_USER_LOGIN = "userLogin";

    $scope.variables = {};
    $scope.variables.message;
    $scope.variables.email;
    $scope.variables.remember = false;
    $scope.mailData = {};

    $rootScope.page = $routeParams.page;

    $scope.submitForm = function(){
        var getParam = 'login='+ $scope.variables.login + "&pass=" + $scope.variables.password;
        if(!$rootScope.socket.connected) {
            $rootScope.socket = socket.connect();
        }
        else {
            socket.disconnect();
            $rootScope.socket = socket.connect();
        }

        $rootScope.socket.on('connect', function () {
            httpRequestService.sendAjaxRequest(ACTION_USER_LOGIN,getParam,null,false,function(data){
                if(data.type == 0) {
                  // Ser current user
                  $rootScope.currentUser = data.value;
                  // Open socket connection
                  socket.authenticate($rootScope.socket, $rootScope.currentUser.auth_token);
                  // Set cookie if Remember me was checked
                  if($scope.variables.remember) {
                    rememberMeService.remember($rootScope.currentUser);
                  }
                  // Redirect to front page
                  $location.path('/mainPage');
                } else {
                  $scope.variables.message = 'login_form_error_'+ data.type;
                }
            });
        });

        $rootScope.socket.on('message', function(obj){
            if(obj.type == 'newMessage') {
                var message = $.parseJSON(obj.data);
                if(message.sender != $rootScope.currentUser.id) {
                  if ($rootScope.page != 'chat') {
                    $rootScope.currentUser.msg_unseen++;
                  }
                  else if ($rootScope.chat_active_room != message.room) {
                    $rootScope.currentUser.msg_unseen++;
                  }
                  $rootScope.$apply();
                }
            }
        });
    }

    $scope.logoutUser = function(){
        httpRequestService.sendAjaxRequest(ACTION_USER_LOGOUT,null,null,false,function(data){
            $rootScope.currentUser = data.type == 0? null:$rootScope.currentUser;
            if ($rootScope.socket) {
              socket.disconnect($rootScope.socket);
            }
            rememberMeService.forget();
            $location.path('/login');
        });
    }

    $scope.editPassword = function(){
        var modalInstance = $modal.open({
            templateUrl:'./views/recoverPasssword.html',
            controller: 'NewPasswordModalInstanceCtrl',
            windowClass: 'app-modal-small',
            resolve: {
                email: function () {
                    return $scope.email
                }
            }
        });
        modalInstance.result.then(function (reload) {
            //if(!reload) return;
        });
    }
});

familyApp.controller('NewPasswordModalInstanceCtrl', function ($scope, $modalInstance,httpRequestService,email,$rootScope) {
    var ACTION_FORGOT_PASSWORD = "forgetPassword";
    $scope.userExists = '';

    $scope.close =  function(){
        $modalInstance.close(true);
    }

    $scope.submitForm =  function(){
        var formData = new FormData();
        var mailData = new FormData();
        mailData.append("bodyText", $('.mail-body').text());
        mailData.append("headerText", $('.mail-header').text());
        mailData.append("email", $scope.email);

        httpRequestService.sendAjaxRequest(ACTION_FORGOT_PASSWORD,null, ("data="+$scope.email+"&lang="+$rootScope.language),true,function(data){
            if(data.type == 0) {
                $scope.userExists = 1;
            }
        });
    }
});
