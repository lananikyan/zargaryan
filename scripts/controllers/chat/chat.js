﻿familyApp.controller('ChatController', function($scope, $rootScope, $routeParams, httpRequestService, $translate, $modal, $timeout, $filter, $window) {
  var ACTION_GET_CHATROOMS = "getUserChatrooms";
  var ACTION_GET_CHAT_HISTORY = "getChatHistory";
  var ACTION_SET_CHAT_SEEN = "setChatSeen";
  var ACTION_SET_MESSAGE_SEEN = "setMessageSeen";
  var ACTION_SEND_MESSAGE = "chatSendMessage";
  var ACTION_CREATE_GROUP = "chatCreateGroup";
  var ACTION_ADD_GROUP_USERS = "chatAddUsersToGroup";
  var ACTION_GET_GROUP_MEMBERS = "getChatGroupMembers";
  var ACTION_REMOVE_GROUP_MEMBER = "removeChatGroupMember";
  var ACTION_REMOVE_GROUP = "removeChatGroup";
  var ACTION_ATTACH_FILE = "chatAttachFile";
  
  $rootScope.page = $routeParams.page;

  $scope.variables = {};
  $scope.variables.limit = 10;
  $scope.variables.history = {};
  $scope.variables.members = {};
  $scope.variables.chat_history_msg = 'no_chat_history_message';
  $scope.variables.timezone = moment.tz.guess();
  $scope.variables.isChatListOpen = true;
  
  $scope.$watch('variables.isChatListOpen', function (newValue, oldValue) {
    if(newValue && $(window).width() < 767) {
      $('body').addClass('mobile-chat-opened');
    }
    else {
      $('body').removeClass('mobile-chat-opened');
    }
  });
  
  $scope.$watch('variables.active_room', function (newValue, oldValue) {
    if(newValue !== undefined) {
      $rootScope.chat_active_room = newValue.id;
    }
  });
   
  $scope.$watch(function(){ return $rootScope.currentUser},function(){
    if(!$rootScope.currentUser) return;
    $scope.getUserChatrooms($rootScope.currentUser.id);
  });
  
  $scope.getUserChatrooms = function(user_id){
    $scope.variables.chat_chatrooms_msg = 'loading';
    httpRequestService.sendAjaxRequest(ACTION_GET_CHATROOMS, 'user_id=' + user_id, null, false, function(data){
      if(data.chatrooms.length == 0) {
        $scope.variables.chat_chatrooms_msg = 'no_chats_message';
        return false;
      }
      $scope.variables.chatrooms = data.chatrooms;
      $scope.variables.active_room = {};
      $scope.variables.chat_chatrooms_msg = '';
    });
  };
  
  $scope.getChatHistory = function(room_id, offset, limit){
    $scope.variables.offset = offset;
    $scope.variables.chat_history_msg = '';
    if(offset == 0) {
      $scope.variables.chat_history_msg = 'loading';
      var room_index = findindex($scope.variables.chatrooms, 'id', room_id);
      $scope.variables.active_room = $scope.variables.chatrooms[room_index];
      $scope.variables.history = {};
      if ($(window).width() < 767) {
        $scope.variables.isChatListOpen = false;
      }
    }
    httpRequestService.sendAjaxRequest(ACTION_GET_CHAT_HISTORY, 'room_id=' + room_id + '&offset=' + offset + '&limit=' + limit, null, false, function(data){
      $.each(data.history, function(i, v) {
        var unixDate = moment.unix(v.timestamp);
        var moment_date = unixDate.tz($scope.variables.timezone);
        v.date_month = moment_date.format("M");
        v.date_day = moment_date.format("DD");
        v.date_year = moment_date.format("YYYY");
      });
      if(offset == 0) {
        $scope.glued = true;
        $scope.variables.history = data.history;
        $scope.variables.currUser = $rootScope.currentUser.id;
        var room = $.grep($scope.variables.chatrooms, function(e){ return e.id === room_id; });
        if(room[0].unseen_messages > 0) {
          $scope.setChatSeen(room_id, $rootScope.currentUser.id);
        }
        if($scope.variables.active_room.type == 'group') {
          $scope.getGroupMembers();
        }
        else {
          $scope.variables.members = {};
        }
        equalSections();
      }
      else {
        var last_msg = $(data.history).last()[0];
        if(typeof last_msg != 'undefined' && data.history.length > 0) {
          $scope.variables.history = $.merge(data.history, $scope.variables.history);
          $timeout(function() {
            $(".chat-area-history").scrollTop(
              $('#msg-' + last_msg.id).offset().top - $(".chat_area").offset().top + $(".chat_area").scrollTop()
            );
          }, 0, false);
        }
      }
      $scope.variables.chat_history_msg = '';
    });
  };
  $scope.getGroupMembers = function() {
    httpRequestService.sendAjaxRequest(ACTION_GET_GROUP_MEMBERS, null, 'room_id=' + $scope.variables.active_room.id + '&user_id=' + $rootScope.currentUser.id, true,
      function (data) {
        $scope.variables.members = data['members'];
        $scope.variables.active_room.name = data['name'];
        equalSections();
      }, function () {}, false, false
    );
  }
  $scope.handleScrollToTop = function() {
    var offset = $scope.variables.offset + $scope.variables.limit;
    $scope.getChatHistory($scope.variables.active_room.id, offset, $scope.variables.limit);
  };
  $scope.setChatSeen = function(room_id, user_id){
    httpRequestService.sendAjaxRequest(ACTION_SET_CHAT_SEEN, null, 'room_id=' + room_id + '&user_id=' + user_id, true, 
    function(data){
      $.each($scope.variables.chatrooms, function(i, v){
        if(v.id == room_id) {
          $rootScope.currentUser.msg_unseen = $rootScope.currentUser.msg_unseen - $scope.variables.chatrooms[i].unseen_messages;
          $scope.variables.chatrooms[i].unseen_messages = 0;
        }
      });
    }, function(data){}, false, false);
  };
  $scope.setMessageSeen = function(user_id, room_id, message_id, message_js_id) {
    httpRequestService.sendAjaxRequest(ACTION_SET_MESSAGE_SEEN, null, 'user_id=' + user_id + '&message_id=' + message_id + '&message_js_id=' + message_js_id + '&room_id=' + room_id, true, 
    function(data){
    }, function(data){}, false, false);
  };
  $scope.submitMessage = function(event){
    event.preventDefault();
    if (event.keyCode == 13 && !event.shiftKey) {
      return true;
    }
  };
  $scope.sendChatMessage = function(form, room) {
    if(room == undefined) {
      if ($scope.variables.active_room.length === 0) {
        return false;
      }
      room = $scope.variables.active_room;
    }
    var room_type = room.type;
    var msg = form.message;
    if(msg.length == 0) {
      return false;
    }
    var Date = moment();
    var moment_date = Date.tz($scope.variables.timezone);
    var new_message = {
      message: msg,
      room_id: room.id,
      room_type: room_type,
      user_id: $rootScope.currentUser.id,
      user_info: {
        picture: ($rootScope.currentUser.user_avatar != undefined) ? $rootScope.currentUser.user_avatar : null,
      },
      status: 'pending',
      message_js_id: Math.random().toString(36).substr(2),
      date_month: moment_date.format("M"),
      date_day: moment_date.format("DD"),
      date_year: moment_date.format("YYYY"),
    };
    $scope.variables.history.push(new_message);
    $scope.form.message = '';
    httpRequestService.sendAjaxRequest(ACTION_SEND_MESSAGE, null, 'room=' + room.id + '&room_type=' + room_type + '&sender=' + $rootScope.currentUser.id + '&message=' + msg + '&message_js_id=' + new_message.message_js_id, true, 
    function(data){
      if(data.status == 1) {
        var msg_index = findindex($scope.variables.history, 'message_js_id', data.message_js_id);
        $scope.variables.history[msg_index].timestamp = data.time;
        $scope.variables.history[msg_index].id = data.message_id;
        $scope.variables.history[msg_index].status = 'unseen';
        if(room_type == 'group') {
          $scope.variables.history[msg_index].status = 'seen';
        }
        var room_index = findindex($scope.variables.chatrooms, 'id', new_message.room_id);
        if(room_index >= 0) {
          $scope.variables.chatrooms[room_index].last_message = {
            message: new_message.message,
            timestamp: data.time
          };
        }
      }
    }, function(data){}, false, false);
  };
  
  $scope.formatTime = function(timestamp, show_date) {
    var unixDate = moment.unix(timestamp);
    var moment_date = unixDate.tz($scope.variables.timezone);
    if(show_date) {
      if(moment().diff(moment_date, 'days') !== 0) {
        return moment_date.format('DD') + ' ' +  $translate.instant('month_' + moment_date.format('M'));
      }
    }
    return moment_date.format('HH:mm');
  };
  
  $rootScope.socket.on('message', function(obj) {
    if(obj.type == 'newMessage') {
      if($rootScope.page !== 'chat') {
        return false;
      }
      var message = $.parseJSON(obj.data);
      if(message.info !== undefined) {
        // User was removed from chat
        if(message.info.type == 'remove_from_room' && $.inArray($rootScope.currentUser.id, message.info.users) != -1) {
          var room_index = findindex($scope.variables.chatrooms, 'id', message.room);
          if(room_index >= 0) {
            $scope.variables.chatrooms.splice(room_index, 1);
            if($scope.variables.active_room.id == message.room) {
              $scope.variables.active_room = {};
              $scope.variables.history = {};
              $scope.variables.chat_history_msg = 'no_chat_history_message';
            }
          }
          return false;
        }
      }
      var author = 'other';
      if(message.sender == $rootScope.currentUser.id) {
        author = 'me';
        if($scope.variables.history.length > 0) {
          if(message.message_js_id !== 0) {
            var msg_index = findindex($scope.variables.history, 'message_js_id', message.message_js_id);
          }
          else {
            var msg_index = findindex($scope.variables.history, 'message_id', message.message_id);
          }
          if(msg_index >= 0) {
            return false;
          }
        }
      }
      var unixDate = moment.unix(message.time);
      var moment_date = unixDate.tz($scope.variables.timezone);
    
      var new_message = {
        message: message.message,
        message_id: message.message_id,
        room_id: message.room,
        user_id: message.sender,
        user_info: message.chat_info,
        timestamp: message.time,
        date_month: moment_date.format("M"),
        date_day: moment_date.format("DD"),
        date_year: moment_date.format("YYYY"),
        status: 'seen'
      };
      if(author == 'me') {
        if(message.room_type !== 'group') {
          new_message.status = 'unseen';
        }
        new_message.message_js_id = message.message_js_id;
      }
      if($scope.variables.history.length > 0 && message.room === $scope.variables.active_room.id) {
        $scope.variables.history.push(new_message);
        if(author !== 'me') {
          $scope.setMessageSeen($rootScope.currentUser.id, new_message.room_id, new_message.message_id, message.message_js_id);
        }
      }
      var room_index = findindex($scope.variables.chatrooms, 'id', message.room);
      var new_last_message = {
        message: new_message.message,
        timestamp: new_message.timestamp
      };
      if(room_index >= 0) {
        $scope.variables.chatrooms[room_index].last_message = new_last_message;
        if(message.room !== $scope.variables.active_room.id && author !== 'me') {
          $scope.variables.chatrooms[room_index].unseen_messages ++;
        }
      }
      else {
        var new_room = {
          id: message.room,
          last_message: new_last_message,
          name: message.chat_info.name,
          picture: message.chat_info.picture,
          type: message.room_type,
          unseen_messages: 0
        }
        if(message.room_type == 'group') {
          new_room.name = message.chat_info.chat_name;
        }
        if(author == 'me') {
          new_room.author = new_message.user_id;
        }
        else {
          new_room.unseen_messages = 1;
        }
        $scope.variables.chatrooms.push(new_room);
        if(message.room_type == 'group' && author == 'me') {
          $scope.getChatHistory(new_room.id, 0, $scope.variables.limit);
        }
      }
      if(message.room_type == 'group') {
        $scope.getGroupMembers();
      }
      $scope.$apply();
    }
    else if(obj.type == 'seenMessage') {
      var message = $.parseJSON(obj.data);
      if($scope.variables.history.length > 0 && message.room === $scope.variables.active_room.id) {
        var msg_index = findindex($scope.variables.history, 'id', message.message_id);
        var msg_js_index = findindex($scope.variables.history, 'message_js_id', message.message_js_id);
        if(msg_index >= 0) {
          $scope.variables.history[msg_index].status = 'seen';
          $scope.$apply();
        }
        if(msg_js_index >= 0) {
          $scope.variables.history[msg_js_index].status = 'seen';
          $scope.$apply();
        }
      }
    }
    else if(obj.type == 'seenChat') {
      var message = $.parseJSON(obj.data);
      if($scope.variables.history.length > 0 && message.room === $scope.variables.active_room.id) {
        $.each($scope.variables.history, function(i, v){
          if(v.status == 'unseen') {
            v.status = 'seen';
          }
        });
        $scope.$apply();
      }
    }
  });
  $scope.addUsersToGroup = function(){
    var usr = $scope.variables.user;
    var room = $scope.variables.active_room;
    $modal.open({
      templateUrl:'./views/chat/selectUsers.html',
      controller: 'selectChatUsers',
      windowClass: 'app-modal-small',
      resolve: {
        user: function () {
          return usr;
        },
        list_type: function () {
          return 'add';
        },
        room: function () {
          return room.id;
        }
      }
    }).result.then(function(result) {
      if(result == undefined) {
        return false;
      }
      var msg_members_added = $rootScope.currentUser.first_name + ' ' + $rootScope.currentUser.last_name + ' ' + $translate.instant('added_members') + ' ' + result.names + ' ' + $translate.instant('to_this_chat');
      if(room.type == 'user') {
        httpRequestService.sendAjaxRequest(ACTION_CREATE_GROUP, null, 'user_id=' + $rootScope.currentUser.id + '&room_id=' + room.id + '&members=' + result.ids + '&msg=' + msg_members_added, true,
          function (data) {
            $scope.variables.chat_users = data['list'];
          }, function () {}, false, false
        );
      }
      else if(room.type == 'group') {
        httpRequestService.sendAjaxRequest(ACTION_ADD_GROUP_USERS, null, 'user_id=' + $rootScope.currentUser.id + '&room_id=' + room.id + '&members=' + result.ids + '&msg=' + msg_members_added, true,
          function (data) {
            $scope.variables.chat_users = data['list'];
          }, function () {}, false, false
        );
      }
    });
  };
  $scope.renameGroup = function(){
    var room = $scope.variables.active_room;
    $modal.open({
      templateUrl:'./views/chat/renameGroup.html',
      controller: 'renameChatGroup',
      windowClass: 'app-modal-small',
      resolve: {
        room: function () {
          return room.id;
        }
      }
    }).result.then(function(result) {
      if(result == undefined) {
        return false;
      }
      var room_index = findindex($scope.variables.chatrooms, 'id', room.id);
      if(room_index >= 0) {
        $.each(['am', 'en', 'ru'], function(i, v) {
          $scope.variables.chatrooms[room_index].name[v] = result.name;
          $scope.variables.active_room.name[v] = result.name;
        });
      }
    });
  };
  $scope.removeUserFromGroup = function(user_id, user_name) {
    var msg = $rootScope.currentUser.first_name + ' ' + $rootScope.currentUser.last_name + ' ' + $translate.instant('removed_members') + ' ' + user_name + ' ' + $translate.instant('from_this_chat');
    httpRequestService.sendAjaxRequest(ACTION_REMOVE_GROUP_MEMBER, null, 'author=' + $rootScope.currentUser.id + '&users=' + user_id + '&room_id=' + $scope.variables.active_room.id + '&msg=' + msg, true,
      function (data) {
        if(data['status'] == 1) {
          $scope.getGroupMembers();
        }
      }, function () {}, false, false
    );
  };
  $scope.removeGroup = function() {
    var delete_confirm = $window.confirm($translate.instant('delete_group_confirm'));
    if(delete_confirm){
      httpRequestService.sendAjaxRequest(ACTION_REMOVE_GROUP, null, 'author=' + $rootScope.currentUser.id + '&room_id=' + $scope.variables.active_room.id, true,
        function (data) {
        }, function () {}, false, false
      );
    }
  }
  $scope.uploadFile = function() {
    if(event.target.files[0] == undefined) {
      return false;
    }
    var formData = new FormData();
    var file = event.target.files[0];
    formData.append('attached_file', file);
    formData.append('room', JSON.stringify($scope.variables.active_room));
    httpRequestService.sendAjaxRequest(ACTION_ATTACH_FILE, null, formData, true, function (data) {
      var msg = {
        message: '[file]' + data.file + '[/file]',
      }
      $scope.sendChatMessage(msg, $.parseJSON(data.room));
    }, function () {}, false, false);
  };
  function findindex(items, f_index, item){
    var index = -1;
    for (var i = 0; i < items.length; ++i) {
      if (items[i][f_index] == item) {
        index = i;
        return index;
      }
    }
    return index;
  }
  function equalSections(width) {
    if(width == undefined) {
      width = $(window).width();
    }
    if(width > 767) {
      $timeout(function () {
        $('.member_list').height($('.message_section').height() - $('#custom-search-input').outerHeight());
      }, 0);
    }
  }
  $(window).scroll(function(){
    $(".open-chatrooms").css("top", '44%');
  });
});

