familyApp.controller('selectChatUsers', function ($scope, $modalInstance, httpRequestService, $modal, user, list_type, room) {
  var ACTION_GET_USERS_LIST = "chatGetUsersList";
  
  httpRequestService.sendAjaxRequest(ACTION_GET_USERS_LIST, null, 'type=' + list_type + '&room=' + room, true,
    function (data) {
      $scope.chat_users = data['list'];
    }, function () {}, false, false
  );
  
  $scope.selectUsers = function() {
    if($scope.form == undefined) {
      return false;
    }
    var selected_users = $scope.form.selected_users;
    if(selected_users.length == 0) {
      return false;
    }
    var selected_user_names = '';
    $("#SelectUsersForm select :selected").each(function(i, v){
      if (i !== 0) {
        selected_user_names += ', ';
      }
      selected_user_names += $(this).text();
    });
    var result = {
      ids: selected_users,
      names: selected_user_names,
    }
    $modalInstance.close(result);
  };
});

familyApp.controller('renameChatGroup', function ($rootScope, $scope, $translate, $modalInstance, httpRequestService, $modal, room) {
  var ACTION_RENAME_GROUP = "renameChatGroup";
  
  $scope.renameGroup = function() {
    if($scope.form == undefined) {
      return false;
    }
    var new_name = $scope.form.name;
    if(new_name.length == 0) {
      return false;
    }
    var result = {};
    var msg_renamed_group = $rootScope.currentUser.first_name + ' ' + $rootScope.currentUser.last_name + ' ' + $translate.instant('renamed_group');
    httpRequestService.sendAjaxRequest(ACTION_RENAME_GROUP, null, 'user=' + $rootScope.currentUser.id + '&room_id=' + room + '&name=' + new_name + '&msg=' + msg_renamed_group, true,
      function (data) {
        result.name = new_name;
        $modalInstance.close(result);
      }, function () {}, false, false
    );
  };
});