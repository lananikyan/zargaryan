familyApp.controller('GuessBookController', function ($scope, $rootScope, $routeParams, httpRequestService, $modal, $location, $anchorScroll, $timeout) {
  var ACTION_GET_FORUM = "getForum";
  var ACTION_GET_FORUM_COMMENT = "getForumComment";
  var ACTION_GET_USERS_BY_BIRTDAY = "getUsersByBirthday";

  var me = this;

  $rootScope.page = $routeParams.page;

  $scope.variables = {};

  $scope.variables.forums = [];
  $scope.variables.users = [];
  $scope.now = new Date().getTime();

  $scope.$watch(function () {
    return $rootScope.language
  }, function () {
    $scope.loadForums();
    $scope.loadUsersByBirthday();
    $scope.timestamp();
    $anchorScroll.yOffset = 120;
  });

  $scope.variables.isGuestListOpen = true;

  $scope.$watch('variables.isGuestListOpen', function (newValue, oldValue) {
    if (newValue && $(window).width() < 767) {
      $('body').addClass('mobile-list-opened');
    }
    else {
      $('body').removeClass('mobile-list-opened');
    }
  });

  $scope.gotoForumContent = function () {
    var old = $location.hash();
    $location.hash('forumContent');
    $anchorScroll();
    $location.hash(old);
  };

  $scope.loadForums = function () {
    httpRequestService.sendAjaxRequest(ACTION_GET_FORUM, null, null, false, function (data) {
      $scope.variables.forums = data.value;

      $scope.totalItems = $scope.variables.forums.length;
      $scope.currentPage = 1;
      $scope.itemsPerPage = 10;
      $scope.numPerPage = 10;
      $scope.maxSize = 3;
    });
  }

  $scope.loadUsersByBirthday = function () {
    httpRequestService.sendAjaxRequest(ACTION_GET_USERS_BY_BIRTDAY, null, null, false, function (data) {
      for (var i in data.value) {
        var item = data.value[i];
        var yers_old = dateDiffInYears(item.birth_date * 1000, $scope.now);
        item.years_old = yers_old;
        var day = new Date(item.birth_date * 1000).getDate();
        var month = new Date(item.birth_date * 1000).getMonth() + 1;
        //console.log(new Date().getDate());
        if (day == new Date().getDate() && month == new Date().getMonth() + 1) {
          $scope.variables.users[i] = item;
        }

      }
      //$scope.variables.users = data.value;
      $scope.curr_timestamp = data.tz;
    });
  }
  var dateDiffInYears = function (dateold, datenew) {
    var one_day = 1000 * 60 * 60 * 24;
    // Calculate the difference in milliseconds
    var difference_ms = datenew - dateold;
    // Convert back to days and return
    return Math.round(difference_ms / one_day / 366);
  }


  $scope.loadForumDetail = function (ind) {
    //$scope.ind = ind;
    //$scope.variables.selectedForum = $scope.variables.forums[ind];
    $scope.ind = (ind) + ($scope.currentPage - 1) * $scope.numPerPage;
    $scope.variables.selectedForum = $scope.variables.forums[$scope.ind];
    $scope.variables.isGuestListOpen = false;
    httpRequestService.sendAjaxRequest(ACTION_GET_FORUM_COMMENT, "id=" + $scope.variables.selectedForum.id, null, false, function (data) {
      if (data.type == 0) {
        for (var i in data.value) {
          var item = data.value[i];

          item.creation_date = new Date(item.creation_date * 1000);
        }
        console.log(data);
        $scope.variables.comments = data.value;
        /*$scope.commentTotalItems = $scope.variables.comments.length;
         $scope.commentCurrentPage = 1;
         $scope.commentItemsPerPage = 10;
         $scope.commentNumPerPage = 10;
         $scope.commentMaxSize = 3;*/
        $scope.limit = 10;
        
      }

    });
  }

  $scope.createForum = function () {
    var modalInstance = $modal.open({
      templateUrl: './views/guessBook/newForumPopup.html',
      controller: 'NewForumModalInstanceCtrl'
    });
    modalInstance.result.then(function (reload) {
      if (!reload)
        return;
      $scope.loadForums()
    });
  }

  $scope.createComment = function () {
    if (!$scope.variables.selectedForum)
      return;
    var modalInstance = $modal.open({
      templateUrl: './views/guessBook/createCommentPopup.html',
      controller: 'CreateCommentModalInstanceCtrl',
      resolve: {
        forumId: function () {
          return $scope.variables.selectedForum.id
        }
      }
    });
    modalInstance.result.then(function (reload) {
      if (!reload)
        return;
      $scope.loadForumDetail($scope.ind)
    });
  }
  //$scope.link_user_id = 1;
  $scope.viewProfile = function (link_id) {
    $location.path("/profile/" + link_id);
  }

  $scope.timestamp = function (unix_time) {
    var epoch = (unix_time * 1000);
    var date = new Date();
    var localOffset = (-1) * date.getTimezoneOffset() * 60000;
    var stamp = Math.round(new Date(epoch + localOffset).getTime());
    return stamp;
  };
  $(window).scroll(function () {
    $(".open-list").css("top", '44%');
  });
});
familyApp.filter('pagination', function () {
  return function (input, currentPage, pageSize) {
    if (angular.isArray(input)) {
      var start = (currentPage - 1) * pageSize;
      var end = currentPage * pageSize;
      return input.slice(start, end);
    }
  };
});