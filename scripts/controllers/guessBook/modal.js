familyApp.controller('NewForumModalInstanceCtrl', function ($scope, $modalInstance,httpRequestService,$rootScope) {
    var ACTION_CREATE_FORUM = "createForum";

    $scope.variables = {};
    $scope.variables.forum = {};

    $scope.submitNewForumForm =  function(){
        $scope.variables.forum.lang = $rootScope.language;
        httpRequestService.sendAjaxRequest(ACTION_CREATE_FORUM,null, ("data=" + JSON.stringify($scope.variables.forum)),true,function(data){
            if(data.type == 0){
                $modalInstance.close(data.value);
            }
        });
    }
});

familyApp.controller('CreateCommentModalInstanceCtrl', function ($scope, $modalInstance,httpRequestService,forumId) {
    var ACTION_CREATE_COMMENT = "createComment";

    $scope.variables = {};
    $scope.variables.comment = {};

    $scope.submitNewCommentForm =  function(){
        $scope.variables.comment.forum_id = forumId;
        httpRequestService.sendAjaxRequest(ACTION_CREATE_COMMENT,null, ("data=" + JSON.stringify($scope.variables.comment)),true,function(data){
            if(data.type == 0){
                $modalInstance.close(data.value);
            }
        });
    }
});
