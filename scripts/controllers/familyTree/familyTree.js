﻿familyApp.controller('FamilyTreeController', function($scope,$rootScope, $routeParams, httpRequestService,$timeout,$modal) {
    var ACTION_GET_USER_TREE = "getUserTree";
    var ACTION_GET_USERS_BY_NAME = "getUsersByName";

    var me = this;
    
    $rootScope.page = $routeParams.page;

    $scope.variables = {};
    $scope.variables.userList = [];

    this.loadUserTree = function(id){
        $rootScope.treeRootUserID = id;
        // Get current screen width
        var getParam = "user_id=" + id + "&screen_width=" + screen.width;
        var deviceWidth = window.screen.width ;
        var width = $('#familyTree').width();
        var innerWidth = $('.inner-center').width();
        if(deviceWidth < width){
            width = deviceWidth;
        }
        if(parseInt(innerWidth) > parseInt(width)){
            $scope.innerCenterZoom = parseInt(width)/parseInt(innerWidth);
        }
        else{
            $scope.innerCenterZoom = 1;
            //$('.inner-center').css('zoom',1);
        }
        httpRequestService.sendAjaxRequest(ACTION_GET_USER_TREE,getParam,null,false,function(data){
            if(data.type == 0) {
                $scope.variables.userList = data.value;
                var usersList = data.value;
                var rootIndex = null;
                for(var i in usersList){
                    if(usersList[i].id == id){
                        $scope.variables.user = usersList[i];
                    }
                    if(usersList[i].sex == 1){
                        usersList[i].type = "Son";
                    }else{
                        if(usersList[i].rel_type == "couple"){
                            usersList[i].type = "Wife";
                        }else{
                            usersList[i].type = "Daughter";
                        }
                    }
                    usersList[i].birth_date = usersList[i].birth_date != null ?new Date( usersList[i].birth_date*1000) :undefined;
                    usersList[i].dead_date = usersList[i].dead_date != null ?new Date( usersList[i].dead_date*1000) :undefined;
                }
                for(var i = 0; i <data.value.length;i++){
                    var isRoot = true;
                    for(var j = 0; j < data.value.length;j++){
                        if(data.value[i].rel_user_id == data.value[j].id){
                            isRoot = false;
                            break;
                        };
                    }
                    if(isRoot){
                        rootIndex = i;
                        break;
                    }
                }
                var rootID = usersList[rootIndex].rel_user_id;
                var unflatten = function( array, parent, tree ){
                    tree = typeof tree !== 'undefined' ? tree : [];
                    parent = typeof parent !== 'undefined' ? parent : { id: rootID };

                    var children = _.filter( array, function(child) {return child.rel_user_id == parent.id; });

                    if( !_.isEmpty( children )  ){
                        if( parent.id == rootID ){
                            tree = children;
                        }else{
                            parent['children'] = children
                        }
                        _.each( children, function( child ){ unflatten( array, child ) } );
                    }

                    return tree;
                }
                var familyTree = unflatten( usersList );
                $scope.familyTree = familyTree;
                $scope.$watch('variables.userList', function (newValue, oldValue) {
                  $timeout(function () {
                    var deviceWidth = window.screen.width;
                    var width = $('#familyTree').width();
                    var innerWidth = $('.inner-center').width();
                    if (deviceWidth < width) {
                      width = deviceWidth;
                    }
                    if (parseInt(innerWidth) > parseInt(width)) {
                      $scope.innerCenterZoom = parseInt(width) / parseInt(innerWidth);
                    }
                    else {
                      $scope.innerCenterZoom = 1;
                    }
                    $scope.innerCenterZoomParent = 1;
                    if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
                      if ($scope.innerCenterZoom < 0.8) {
                        $scope.innerCenterZoomParent = 0.8;
                      }
                    }
                    else {
                      $('.inner-center').css({
                        '-webkit-transform' : 'scale('+$scope.innerCenterZoom+')',
                        '-moz-transform'    : 'scale('+$scope.innerCenterZoom+')',
                        '-ms-transform'     : 'scale('+$scope.innerCenterZoom+')',
                        '-o-transform'      : 'scale('+$scope.innerCenterZoom+')',
                        'transform'         : 'scale('+$scope.innerCenterZoom+')'
                      });
                    }
                  }, 0);
                });
            }
        })
    }
 //////change this code repeats
    if($rootScope.treeRootUserID){
        me.loadUserTree( $rootScope.treeRootUserID);
    }else{
        $scope.$watch(function(){ return $rootScope.currentUser},function(){
            if(!$rootScope.currentUser) return;
            me.loadUserTree( $rootScope.currentUser.id);

        })
    }

    $scope.$watch(function(){ return $rootScope.language},function(){
        if($rootScope.treeRootUserID){
            me.loadUserTree( $rootScope.treeRootUserID);
        }else{
            $scope.$watch(function(){ return $rootScope.currentUser},function(){
                if(!$rootScope.currentUser) return;
                me.loadUserTree( $rootScope.currentUser.id);
            })
        }
    })


    $scope.loadUserData = function(id){
        if(id != $rootScope.treeRootUserID){
            me.loadUserTree(id);
        }else{
            var usersList = $scope.variables.userList;
            for(var i in usersList) {
                if (usersList[i].id == id) {
                    $scope.variables.user = usersList[i];
                }
            }
            var modalInstance = $modal.open({
                templateUrl:'./views/familyTree/treeUserPopup.html',
                controller: 'TreeUserViewInstanceCtrl',
                windowClass: 'app-modal-small',
                resolve: {
                    user: function () {
                        return $scope.variables.user;
                    }
                }
            });
            modalInstance.result.then(function (id) {
                if(!id) return;
                $scope.loadUserProfile(id)
            });
        }
    };

    $scope.searchByName = function(){
        var s
        if($scope.searchVariable){
            s = $scope.searchVariable.replace(/զարգարյան/i, "").replace(/заргарян/i, "").replace(/zargaryan/i, "").replace(/ /g, "");
        }
        var getData = s? "search=" + s :null;
        httpRequestService.sendAjaxRequest(ACTION_GET_USERS_BY_NAME,getData,null,false,function(data){
            $scope.variables.searchUsers = data.value;
            for(var i in $scope.variables.searchUsers){
                var user = $scope.variables.searchUsers[i];
                user.birth_date = user.birth_date != 0 ?  new Date(user.birth_date*1000):undefined;

            }
            var modalInstance = $modal.open({
                templateUrl:'./views/familyTree/searchUserPopup.html',
                controller: 'SearchUserViewInstanceCtrl',
                windowClass: 'app-modal-small',
                resolve: {
                    users: function () {
                        return $scope.variables.searchUsers;
                    }
                }
            });
            modalInstance.result.then(function (id) {
                if(!id) return;
                $scope.loadUserData(id)
            });
        });
    }
});