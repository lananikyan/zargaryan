familyApp.controller('TreeUserViewInstanceCtrl', function ($scope,$location, $modalInstance,user) {
    $scope.variables = {};

    $scope.variables.user = user;
    $scope.viewProfile = function(){
        $modalInstance.close();
        $location.path("/profile/"+ $scope.variables.user.id);
    }

    $scope.close = function(){
        $modalInstance.close();
    }
});

familyApp.controller('SearchUserViewInstanceCtrl', function ($scope,$location, $modalInstance,users) {
    $scope.variables = {};

    $scope.variables.users = users;
    $scope.close = function(){
        $modalInstance.close();
    }

    $scope.loadUserTree = function(id){
        $modalInstance.close(id);
    }
});

