familyApp.controller('RegisterController', function ($scope, $http, $rootScope, $routeParams, $location, httpRequestService,$timeout) {
  // Redirect authenticated users to mainpage
  if($rootScope.currentUser) {
    $location.path( "/mainPage" );  
  }
  
  var ACTION_CREATE_NEW_USER = "createNewUser";
  var ACTION_GET_COUNTRIES_WITH_ALL_LANG = "getCountriesWithAllLang";

  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
  $scope.format = $scope.formats[0];

  $scope.variables = {};
  $scope.variables.user = {};
  $scope.variables.user.sex = 1;
  $scope.variables.mode = 'register';
  $rootScope.page = $routeParams.page;

  $scope.variables.message;

  $scope.open = function ($event) {
    $event.preventDefault();
    $event.stopPropagation();

    $scope.opened = true;
  };

  $scope.openDead = function ($event) {
    $event.preventDefault();
    $event.stopPropagation();

    $scope.deadOpened = true;
  };

  $scope.setFile = function (el) {
    //$scope.variables.fileObject = el.files[0];
    $scope.$apply(function($scope) {
      $scope.variables.fileObject = el.files[0];
    });
  }
  
  $scope.variables.errors = [];
  
  $scope.submitForm = function () {
    var formData = new FormData();
    if ($scope.variables.user.dead_date == undefined) {
      $scope.variables.user.dead_date = null;
    } else {
      $scope.variables.user.dead_date = $scope.variables.user.dead_date.getTime() / 1000;
    }
	
	if (typeof $scope.variables.user.birth_date == 'number'){
		$scope.variables.user.birth_date = new Date($scope.variables.user.birth_date);
	}
    //$scope.variables.user.birth_date = $scope.variables.user.birth_date.getTime() / 1000;
	
    formData.append("data", JSON.stringify($scope.variables.user));
    if ($scope.variables.fileObject && typeof $scope.variables.fileObject.name !== 'undefined') {
      formData.append('profile_picture', $scope.variables.fileObject);
    }
    httpRequestService.sendAjaxRequest(ACTION_CREATE_NEW_USER, null, formData, true, function (data) {
		$scope.variables.errors = [];
      if (data.type == 0) {
        $location.path('/login');
      }
	  else if (data.type == 1 ){
		  $.each(data.error, function (i, v){
			$scope.variables.errors.push(v);
		  });

	  }
	  
    });
  }

  this.loadCountryList = function () {
    httpRequestService.sendAjaxRequest(ACTION_GET_COUNTRIES_WITH_ALL_LANG, null, null, false, function (data) {
      if (data.type == 0) {
        $scope.variables.coutries = data.value;
      }
    });
  }

  this.loadCountryList();

  $scope.scopeCheckRequired = function (form_values) {
    var required = 1;
    if (typeof form_values == 'undefined' || form_values.length == 0) {
      return required;
    }
    var multilang_req_fields = [
      'birth_place',
      'city',
      'first_name',
      'last_name',
    ];

    $.each(form_values, function (lang, value) {
      var all_fields_exist = true;
      $.each(multilang_req_fields, function (i, v) {
        if (value[v] == undefined || value[v] == '') {
          all_fields_exist = false;
        }
      });
      if (all_fields_exist) {
        required = 0;
        return required;
      }
    });
    return required;
  };
});