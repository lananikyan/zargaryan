familyApp.controller('ContactUsController', function($scope,$rootScope, $routeParams, $http,httpRequestService) {
    var ACTION_CREATE_NEW_MESSAGE = "createNewMessage";

    $rootScope.page = $routeParams.page;

    $scope.variables = {};
    $scope.mode = 0;

    $scope.$watch(function(){ return $rootScope.currentUser},function(){
        if(!$rootScope.currentUser) return;
        $scope.variables.name = $rootScope.currentUser.first_name + " " +  $rootScope.currentUser.last_name;
        $scope.variables.email = $rootScope.currentUser.email;
        $scope.variables.phone = $rootScope.currentUser.phone;
    })

    $scope.setFile = function(el){
        $scope.$apply(function($scope) {
            $scope.variables.fileObject = el.files[0];
        });
    }
    $scope.submitForm = function(){
        var formData = new FormData();
        formData.append("name", $scope.variables.name);
        formData.append("email", $scope.variables.email);
        formData.append("phone", $scope.variables.phone);
        formData.append("header", $scope.variables.header);
        formData.append("message", $scope.variables.message);

        if($scope.variables.fileObject && typeof $scope.variables.fileObject.name !== 'undefined'){
            formData.append('file', $scope.variables.fileObject);
        }

        httpRequestService.sendAjaxRequest(ACTION_CREATE_NEW_MESSAGE,null,formData,true,function(data){
            if(data.type == 0){
                $scope.mode = 1;
            }
        }, null,true);
    }
});