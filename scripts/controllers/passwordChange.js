familyApp.controller('PasswordChangeController', function($scope,$http,$rootScope,$location,$modal,httpRequestService, $routeParams) {
  var ACTION_CHECK_UNIQUE_ID = "changePassword";
  var ACTION_UPDATE_USER = "updateUser";
  var unique_id = $routeParams['id'];
  $scope.variables = {};
  $scope.variables.user = {};

  httpRequestService.sendAjaxRequest(ACTION_CHECK_UNIQUE_ID,null, ("id="+unique_id ), true, function(data){
    if(data.type == 0) {
      $scope.variables.user.id = data.value;
    }
    else {
      $location.path( "/login" );
    }
  });

  $scope.submitForm = function(){
    var formData = new FormData();
    $scope.variables.user.unique_token = 0;
    formData.append("data", JSON.stringify($scope.variables.user));
    httpRequestService.sendAjaxRequest(ACTION_UPDATE_USER, null, formData, true, function(data){
      if(data.type == 0){
        $location.path( "/login" );
      }
    });
  }

});
