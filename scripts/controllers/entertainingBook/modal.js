familyApp.controller('NewArticleModalInstanceCtrl', function ($scope, $modalInstance,httpRequestService) {
    var ACTION_CREATE_ARTICLE = "createArticle";

    $scope.variables = {};

    $scope.submitNewArticleForm =  function(){
        httpRequestService.sendAjaxRequest(ACTION_CREATE_ARTICLE,null, ("data=" + JSON.stringify($scope.variables.article)),true,function(data){
            $modalInstance.close();
        },null,true);
    }
});