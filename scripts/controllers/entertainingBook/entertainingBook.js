familyApp.controller('EntertainingBookController', function ($scope, $rootScope, $routeParams, httpRequestService, $modal, $location, $anchorScroll, $timeout) {
  var ACTION_GET_ARTICLES_SHORT_LIST = "getArticlesShortList";
  var ACTION_GET_ARTICLES_DETAIL = "getArticlesDetail";

  var me = this;

  $rootScope.page = $routeParams.page;

  $scope.variables = {};
  $scope.variables.articles = [];

  $scope.$watch(function () {
    return $rootScope.language
  }, function () {
    $scope.loadArticlesInfo();
    $anchorScroll.yOffset = 120;
  })

  familyApp.run(function(){
    if ($(window).width() < 767) {
      $scope.variables.isBookListOpen = false;
    }
    else {
      $scope.variables.isBookListOpen = true;
    }
  });
  
  

  $scope.$watch('variables.isBookListOpen', function (newValue, oldValue) {
    if (newValue && $(window).width() < 767) {
      $('body').addClass('mobile-list-opened');
    }
    else {
      $('body').removeClass('mobile-list-opened');
    }
  });

  $scope.gotoArticleContent = function () {
    var old = $location.hash();
    $location.hash('articleContent');
    $anchorScroll();
    // offset position by 100px down from the top
    //$anchorScroll.yOffset = 100;
    $location.hash(old);
  };

  $scope.loadArticlesInfo = function () {
    var getData = $scope.searchVariable ? "search=" + $scope.searchVariable : null;
    httpRequestService.sendAjaxRequest(ACTION_GET_ARTICLES_SHORT_LIST, getData, null, false, function (data) {
      for (var i in data.value) {
        var item = data.value[i];

        item.creation_date = new Date(item.creation_date * 1000);
      }
      $scope.variables.articles = data.value;
      $scope.totalItems = $scope.variables.articles.length;
      $scope.currentPage = 1;
      $scope.itemsPerPage = 5;
      $scope.numPerPage = 5;
      $scope.maxSize = 3;


      if ($scope.variables.articles.length != 0) {
        var ind = Math.floor(Math.random() * $scope.variables.articles.length);
        $scope.loadArticleDetail(ind);
      }
    });
  }

  $scope.loadArticleDetail = function (ind) {
    $scope.ind = (ind) + ($scope.currentPage - 1) * $scope.numPerPage;
    $scope.variables.selectedArticle = $scope.variables.articles[$scope.ind];
    $scope.variables.isBookListOpen = false;
    httpRequestService.sendAjaxRequest(ACTION_GET_ARTICLES_DETAIL, "id=" + $scope.variables.selectedArticle.id, null, false, function (data) {
      if (data.type == 0) {
        $scope.variables.selectedArticle = data.value;
        $scope.variables.selectedArticle.creation_date = $scope.variables.selectedArticle.creation_date != 0 ? new Date($scope.variables.selectedArticle.creation_date * 1000) : undefined;
      }

    });
  }

  $scope.createArticle = function () {
    var modalInstance = $modal.open({
      templateUrl: './views/entertainingBook/newArticlePopup.html',
      controller: 'NewArticleModalInstanceCtrl'
    });
  }
  $(window).scroll(function () {
    $(".open-list").css("top", '44%');
  });
});