familyApp.controller('MyPageController', function($scope,$rootScope,$routeParams,$modal,httpRequestService,$timeout,$filter) {
    var ACTION_GET_USER_DATA = "getUserData";
    var ACTION_GET_USER_FULL_INFO_ALL_LANG = "getUserData_allLang";
    var ACTION_GET_ALBOM_LIST = "getAlbumList";
    var ACTION_GET_VIDEO_LIST = "getVideoList";
    //var ACTION_REMOVE_ALBUM = "removeAlbum";
    var ACTION_GET_ALBUM_ALL_LANGUAGES = "getAlbum_allLang";
    var ACTION_GET_ALBUM_PICTURE_LIST = "getAlbumPictureList";
    var ACTION_GET_TREE_ALL_USERS_LIST = "getTreeAllUsersList";
    var ACTION_GET_VIDEO_INFO_ALL_LANGUAGES = "getVideo_allLang";
    //var ACTION_REMOVE_VIDEO = "removeVideoItem";



    $rootScope.page = $routeParams.page;


    $scope.variables = {};
    $scope.tab = 1;

    //$scope.mobileWidth = 767+1;
    $scope.mobileWidth = 767+1;
    $scope.$watch('width', function(old_width, new_width){
        if(typeof new_width == 'undefined') {
            return false;
        }
       $scope.new_width = new_width;
       $scope.old_width = old_width;
    });

    $scope.loadUserProfile = function(id){
        var getParam = "id=" + id;
        httpRequestService.sendAjaxRequest(ACTION_GET_USER_DATA,getParam,null,false,function(data){
            if(data.type == 0){
                $scope.variables.user = data.value;
                $scope.variables.user.birth_date = $scope.variables.user.birth_date != null ?new Date( $scope.variables.user.birth_date*1000) :undefined;
                $scope.variables.user.dead_date = $scope.variables.user.dead_date != null ?new Date( $scope.variables.user.dead_date*1000) :undefined;
                $scope.variables.included_in_tree = data.included_in_tree;
                var usersChildList = $scope.variables.user.children;
                var usersCoupleList = $scope.variables.user.couple;
                for(var i in usersChildList) {
                   $scope.variables.user.children[i].birth_date = $scope.variables.user.children[i].birth_date != 0 ?new Date( $scope.variables.user.children[i].birth_date*1000) :undefined;
                   $scope.variables.user.children[i].countryName = $scope.variables.user.children[i].caption != 0 ? $scope.variables.user.children[i].caption :undefined;
                }
                for(var j in usersCoupleList) {
                    $scope.variables.user.couple[j].birth_date = $scope.variables.user.couple[j].birth_date != 0 ?new Date( $scope.variables.user.couple[j].birth_date*1000) :undefined;
                    $scope.variables.user.couple[j].countryName = $scope.variables.user.couple[j].caption != 0 ? $scope.variables.user.couple[j].caption :undefined;
                }
            }
        })
    }

    $scope.loadAlbums = function(id){
        var getParamAlbum = "user_id=" + id;
        httpRequestService.sendAjaxRequest(ACTION_GET_ALBOM_LIST,getParamAlbum,null,false,function(data){
            if(data.type == 0){
                $scope.variables.albums = data.value;
                if($scope.variables.selectedAlbum ){
                    $scope.loadAlbumPhotos();
                }
                else{
                    if($scope.variables.albums.length != 0){
                    }

                    else{
                        $scope.variables.pictures = [];
                    }
                }

            }
        });
    }

    $scope.loadVideos = function(id){
        var getParamAlbum = "user_id=" + id;
        httpRequestService.sendAjaxRequest(ACTION_GET_VIDEO_LIST,getParamAlbum,null,false,function(data){
            if(data.type == 0){
                $scope.variables.videos = data.value;
            }
        });
    }

    $scope.loadUserInfo = function(id){
        $scope.loadUserProfile(id);
        $scope.loadAlbums(id);
        $scope.loadVideos(id);
    }
    if($routeParams.id){
        $scope.loadUserInfo($routeParams.id);
    }

    $scope.$watch(function(){ return $rootScope.language},function(){
        if($scope.variables.user ) {
            $scope.loadUserInfo($scope.variables.user.id);
        }
    })

    $scope.$watch(function(){ return $rootScope.currentUser},function(){
        if(!$rootScope.currentUser) return;
        $scope.variables.currentUser = $rootScope.currentUser;
        if(!$routeParams.id ){
            $scope.loadUserInfo($scope.variables.currentUser.id);
        }
    })

    $scope.editProfile = function(){
        var getParam = "id=" + $scope.variables.user.user_id;
        httpRequestService.sendAjaxRequest(ACTION_GET_USER_FULL_INFO_ALL_LANG,getParam,null,false,function(data){
            if(data.type == 0){
                var modalInstance = $modal.open({
                    templateUrl:'./views/register.html',
                    controller: 'UserEditInstanceCtrl',
                    windowClass: 'app-modal-large',
                    size: 'lg',
                    resolve: {
                        user: function () {

                            birth_date_obj = data.value.birth_date != 0 ? new Date(data.value.birth_date*1000) : undefined;
                            if (birth_date_obj != undefined) {
                                birth_date_obj.setHours(0, -birth_date_obj.getTimezoneOffset(), 0, 0);
                                data.value.birth_date = birth_date_obj.toISOString().slice(0,10);
                            }
                            else {
                                //data.value.birth_date = '';
                            }

                            dead_date_obj = data.value.dead_date != null ? new Date(data.value.dead_date*1000):undefined;
                            if(dead_date_obj != undefined) {
                                dead_date_obj.setHours(0, -dead_date_obj.getTimezoneOffset(), 0, 0);
                                data.value.dead_date = dead_date_obj.toISOString().slice(0,10);
                            }

                            return data.value;
                        }
                    }
                });
                modalInstance.result.then(function (id) {
                    if(!id) return;
                    $scope.loadUserProfile(id)
                });
            }
        });
    }

    $scope.editPassword = function(){
        var getParam = "id=" + $scope.variables.user.user_id;
        httpRequestService.sendAjaxRequest(ACTION_GET_USER_FULL_INFO_ALL_LANG,getParam,null,false,function(data){
            if(data.type == 0){
                var modalInstance = $modal.open({
                    templateUrl:'./views/mypage/passwordPopup.html',
                    controller: 'UserEditInstanceCtrl',
                    windowClass: 'app-modal-small',
                    resolve: {
                        user: function () {
                            data.value.birth_date = data.value.birth_date != 0 ?  new Date(data.value.birth_date*1000):undefined;
                            return data.value;
                        }
                    }
                });
                modalInstance.result.then(function (id) {
                    if(!id) return;
                    $scope.loadUserProfile(id)
                });
            }
        });
    }

    $scope.createAlbum = function(){
        var modalInstance = $modal.open({
            templateUrl:'./views/mypage/albumPopup.html',
            controller: 'NewAlbumModalInstanceCtrl',
            windowClass: 'app-modal-small'
        });
        modalInstance.result.then(function (reload) {
            if(!reload) return;
            $scope.loadAlbums($scope.variables.user.id)
        });
    }

    $scope.loadAlbumPhotos = function(ind){
        if(ind != undefined)
            $scope.variables.selectedAlbum = $scope.variables.albums[ind];
        var getParamAlbum = "album_id=" + $scope.variables.selectedAlbum.id;
        httpRequestService.sendAjaxRequest(ACTION_GET_ALBUM_PICTURE_LIST,getParamAlbum,null,false,function(data){
            if(data.type == 0){
                $scope.variables.pictures = data.value;
            }
        });
    }

    //Tabs
    $scope.setTab = function(newTab){
        $scope.tab = newTab;
        $scope.variables.selectedAlbum = '';
    };

    $scope.isSet = function(tabNum){
        return $scope.tab === tabNum;
    };

    $scope.editAlbum = function(ind){
        //var getParam = "id=" + $scope.variables.selectedAlbum.id;
        var getParam = "id=" + $scope.variables.albums[ind].id;
        httpRequestService.sendAjaxRequest(ACTION_GET_ALBUM_ALL_LANGUAGES,getParam,null,false,function(data){
            if(data.type == 0){
                var modalInstance = $modal.open({
                    templateUrl:'./views/mypage/albumPopup.html',
                    controller: 'AlbumEditInstanceCtrl',
                    windowClass: 'app-modal-small',
                    resolve: {
                        album: function () {
                            return data.value
                        }
                    }
                });
                modalInstance.result.then(function (reload) {
                    if(!reload) return;
                    $scope.variables.selectedAlbum = '';
                    $scope.loadAlbums($scope.variables.user.id)
                });
            }
        });
    }

    $scope.removeAlbum = function(ind){
        //var getParam = "id=" + $scope.variables.selectedAlbum.id;
        var getParam = "id=" + $scope.variables.albums[ind].id;
        //var getParam = "id=" + albumId;
        /*httpRequestService.sendAjaxRequest(ACTION_REMOVE_ALBUM,getParam,null,false,function(data){
            if(data.type == 0){
                $scope.variables.selectedAlbum = null;
                $scope.loadAlbums($scope.variables.user.id)
            }
        });*/
        //var usrParent = $scope.variables.user;
        //var user = $scope.variables.user.children[user_id];
        //var getParam = "rel_user_id=" + user.rel_user_id+"&user_id="+user.user_id;
        var modalInstance = $modal.open({
            templateUrl:'./views/mypage/confirmPopup.html',
            controller: 'RemoveAlbumConfirmModalInstanceCtrl',
            windowClass: 'app-modal-small',
            resolve: {
                albumId: function () {
                    return getParam;
                }

            }
        });
        modalInstance.result.then(function (reload) {
            if(!reload) return;
            $scope.loadAlbums($scope.variables.user.id);
            $scope.variables.selectedAlbum = null;
        });
    }

    $scope.createAlbumPicture = function(ind){

        var modalInstance = $modal.open({
            templateUrl:'./views/mypage/albumPicturePopup.html',
            controller: 'NewAlbumPictureModalInstanceCtrl',
            windowClass: 'app-modal-small',
            resolve: {
                albumId: function () {
                    //return $scope.variables.selectedAlbum.id
                    return $scope.variables.albums[ind].id
                },
                max_upload: function () {
                    return $scope.variables.user.max_upload;
                }
            }
        });
        modalInstance.result.then(function (reload) {
            if(!reload) return;
            $scope.loadAlbums($scope.variables.user.id),
            $scope.loadUserProfile($scope.variables.user.id)
        });
    }

    $scope.createAlbumPicture_thumbs = function(album_id){
        var foundItem = $filter('filter')($scope.variables.albums, { id: album_id  }, true)[0];
        //get the index
        var index = $scope.variables.albums.indexOf(foundItem );

        var modalInstance = $modal.open({
            templateUrl:'./views/mypage/albumPicturePopup.html',
            controller: 'NewAlbumPictureModalInstanceCtrl',
            windowClass: 'app-modal-small',
            resolve: {
                albumId: function () {
                    //return $scope.variables.selectedAlbum.id
                    return album_id
                },
                max_upload: function () {
                    return $scope.variables.user.max_upload;
                }
            }
        });
        modalInstance.result.then(function (reload) {
            if(!reload) return;
            $scope.variables.selectedAlbum = $scope.variables.albums[index];
            $scope.loadAlbums($scope.variables.user.id),
              $scope.loadUserProfile($scope.variables.user.id)
        });
    }

    //Create video
    $scope.createVideo = function(){
        var modalInstance = $modal.open({
            templateUrl:'./views/mypage/newVideoPopup.html',
            controller: 'NewVideoModalInstanceCtrl',
            windowClass: 'app-modal-small',
            resolve: {
                userId: function () {
                    return $scope.variables.user.user_id
                },
                videoType: function () {
                    return $scope.userVideoType = 'upload';
                }
            }
        });
        modalInstance.result.then(function (reload) {
            if(!reload) return;
            $scope.loadVideos($scope.variables.user.id),
            $scope.loadUserProfile($scope.variables.user.id)
        });
    }

    $scope.playVideo = function(ind){
        var modalInstance = $modal.open({
            templateUrl:'./views/mypage/playVideo.html',
            controller: 'VideoPlayModalInstanceCtrl',
            windowClass: 'app-modal-small',
            resolve: {
                video: function () {
                    return $scope.variables.videos[ind]
                }
            }
        });

        modalInstance.result.then(function (reload) {
            if(!reload) return;
            $scope.loadVideos($scope.variables.user.id),
            $scope.loadUserProfile($scope.variables.user.id)
        });
    }

    //Create embed video
    $scope.createEmbedVideo = function(){
        var modalInstance = $modal.open({
            templateUrl:'./views/mypage/newVideoPopup.html',
            controller: 'NewVideoModalInstanceCtrl',
            windowClass: 'app-modal-small',
            resolve: {
                userId: function () {
                    return $scope.variables.user.user_id
                },
                videoType: function () {
                    return $scope.userVideoType = 'embed';
                }
            }
        });
        modalInstance.result.then(function (reload) {
            if(!reload) return;
            $scope.loadVideos($scope.variables.user.id)
        });
    }

    //Edit video
    $scope.editVideo = function(ind){
        var getParam = "id=" + $scope.variables.videos[ind].id;
        httpRequestService.sendAjaxRequest(ACTION_GET_VIDEO_INFO_ALL_LANGUAGES,getParam,null,false,function(data){
            //if(data.type == 0){
            var modalInst = $modal.open({
                templateUrl:'./views/mypage/videoInfoPopup.html',
                controller: 'VideoEditModalInstanceCtrl',
                windowClass: 'app-modal-small',
                resolve: {
                    video: function () {
                        return data.value
                    }
                }
            });
            modalInst.result.then(function (reload) {
                if(!reload) return;
                $scope.loadVideos($scope.variables.user.id);
            });
            //}
        });
    }

    //Remove Video
    $scope.removeVideo = function(ind){
        var getParam = "id=" + $scope.variables.videos[ind].id;
        var modalInstance = $modal.open({
            templateUrl:'./views/mypage/confirmPopup.html',
            controller: 'RemoveVideoConfirmModalInstanceCtrl',
            windowClass: 'app-modal-small',
            resolve: {
                videoId: function () {
                    return getParam;
                }

            }
        });
        modalInstance.result.then(function (reload) {
            if(!reload) return;
            $scope.loadVideos($scope.variables.user.id);
        });
        /*httpRequestService.sendAjaxRequest(ACTION_REMOVE_VIDEO,getParam,null,false,function(data){
            if(data.type == 0){
                $scope.loadVideos($scope.variables.user.id);
            }
        });*/
    }

    $scope.showPictureFullInfo = function(ind){
        $scope.variables.user_details = {};
        $scope.variables.user_details.userId = $scope.variables.user.id;
        $scope.variables.user_details.currentUser = $scope.variables.currentUser;
        //$scope.variables.user_details.currentUserRole = $scope.variables.currentUser.role;
       
        //$scope.variables.pictures[ind].active = true;
        var modalInstance = $modal.open({
            //templateUrl:'./views/mypage/albumPictureInfoPopup.html',
            animation: true,
            templateUrl:'./views/mypage/carousel.html',
            controller: 'AlbumPictureInfoModalInstanceCtrl',
            windowClass: 'app-modal-large',
            size: 'lg',
            scope: $scope,
            resolve: {
                picture: function () {
                    return $scope.variables.pictures[ind]
                },
                album: function () {
                    return $scope.variables.pictures
                },
                userDetails: function () {
                    return $scope.variables.user_details
                }
            }
        });

        modalInstance.result.then(function (reload) {
            if(!reload) return;
            $scope.loadAlbums($scope.variables.user.id),
            $scope.loadUserProfile($scope.variables.user.id)
        });
    }

    $scope.getTimeStamp = function(){
        return new Date().getTime();
    }

    $scope.addUserToTree = function(){
        var usr = $scope.variables.user;

        if($scope.variables.currentUser.role == 1){
            var getParam = "id=" + $scope.variables.user.user_id;
            httpRequestService.sendAjaxRequest(ACTION_GET_TREE_ALL_USERS_LIST,getParam,null,false,function(data){
                if(data.type == 0){
                    var modalInstance = $modal.open({
                        templateUrl:'./views/mypage/addToTree.html',
                        controller: 'AddUserToTRee',
                        windowClass: 'app-modal-small',
                        resolve: {
                            user: function () {
                                return usr;
                            },
                            relation_users: function () {
                                return data.value;
                            }
                        }
                    });
                    modalInstance.result.then(function (reload) {
                        if(!reload) return;
                        $scope.loadUserProfile($scope.variables.user.id)
                    });
                }
            });
        }
    }

    $scope.mergeUserToTree = function(){
        var usr = $scope.variables.user;
        if($scope.variables.currentUser.role == 1){
            var getParam = "id=" + $scope.variables.user.user_id;
            httpRequestService.sendAjaxRequest(ACTION_GET_TREE_ALL_USERS_LIST,getParam,null,false,function(data){
                if(data.type == 0){
                    var modalInstance = $modal.open({
                        templateUrl:'./views/mypage/mergeUserWithTree.html',
                        controller: 'AddUserToTRee',
                        windowClass: 'app-modal-small',
                        resolve: {
                            user: function () {
                                return usr;
                            },
                            relation_users: function () {
                                return data.value;
                            }
                        }
                    });
                    modalInstance.result.then(function (reload) {
                        if(!reload) return;
                        $scope.loadUserProfile($scope.variables.user.id)
                    });
                }
            });
        }
    }

    $scope.deleteUserFromTree = function(){
        if($scope.variables.currentUser.role == 1){
            var modalInstance = $modal.open({
                templateUrl:'./views/mypage/deleteFromTree.html',
                controller: 'DeleteUserFromTRee',
                windowClass: 'app-modal-small',
                resolve: {
                    user: function () {
                        return $scope.variables.user;
                    }
                }
            });
            modalInstance.result.then(function (reload) {
                if(!reload) return;
                $scope.loadUserProfile($scope.variables.user.id)
            });
        }
    }

    $scope.openChat = function(){
        var usr = $scope.variables.user;
        var modalInstance = $modal.open({
            templateUrl:'./views/mypage/chatPopup.html',
            controller: 'openChat',
            windowClass: 'app-modal-small',
            resolve: {
                user: function () {
                    return usr;
                },
            }
        });
    };
    
    $scope.addCouple = function(ind){
        var usr = $scope.variables.user;
        var modalInstance = $modal.open({
            templateUrl:'./views/register.html',
            controller: 'NewCoupleModalInstanceCtrl',
            windowClass: 'app-modal-large',
            size: 'lg',
            resolve: {
                user: function () {
                    return usr;
                },
                userType: function () {
                    $scope.userType = 'sexSensitiveForm';
                    return $scope.userType
                }
            }
        });
        modalInstance.result.then(function (reload) {
            if(!reload) return;
            $scope.loadUserProfile($scope.variables.user.id)
        });
    };

    $scope.addChild = function(ind){
        var usr = $scope.variables.user;
        var modalInstance = $modal.open({
            templateUrl:'./views/register.html',
            controller: 'NewChildModalInstanceCtrl',
            windowClass: 'app-modal-large',
            size: 'lg',
            resolve: {
                user: function () {
                    return usr;
                },
                userType: function () {
                    $scope.userType = 'briefForm';
                    return $scope.userType
                }
            }
        });
        modalInstance.result.then(function (reload) {
            if(!reload) return;
            $scope.loadUserProfile($scope.variables.user.id)
        });
    };

    $scope.showUserFullInfo = function(user_id){
        var user = $scope.variables.user.children[user_id];
        var getParam = "id=" + user.rel_user_id;
        httpRequestService.sendAjaxRequest(ACTION_GET_USER_FULL_INFO_ALL_LANG,getParam,null,false,function(data){
            if(data.type == 0){
                var modalInstance = $modal.open({
                    templateUrl:'./views/register.html',
                    controller: 'UserChildModalInstanceCtrl',
                    windowClass: 'app-modal-large',
                    resolve: {
                        user: function () {
                            data.value.birth_date = data.value.birth_date != 0?new Date(data.value.birth_date*1000):undefined;
                            return data.value;
                        }
                    }
                });
                modalInstance.result.then(function (obj) {
                    if(!obj || !obj.id) return;
                    if(obj.approved)
                        me.approveReject(obj.id,ACTION_APPROVE_USER)
                    else
                        me.approveReject(obj.id,ACTION_REJECT_USER)
                });

            }
        });
    }


    $scope.editChildProfile = function(user_id){
        var user = $scope.variables.user.children[user_id];
        var getParam = "id=" + user.rel_user_id;
        httpRequestService.sendAjaxRequest(ACTION_GET_USER_FULL_INFO_ALL_LANG,getParam,null,false,function(data){
            if(data.type == 0){
                var modalInstance = $modal.open({
                    templateUrl:'./views/register.html',
                    controller: 'UserEditInstanceCtrl',
                    windowClass: 'app-modal-large',
                    size: 'lg',
                    resolve: {
                        user: function () {
                            data.value.birth_date = data.value.birth_date != 0 ? new Date(data.value.birth_date*1000):undefined;
                            data.value.formType = 'briefForm';
                            return data.value;
                        }
                    }
                });
                modalInstance.result.then(function (reload) {
                    if(!reload) return;
                    $scope.loadUserProfile($scope.variables.user.id)
                });
            }
        });
    }

    $scope.editCouple = function(user_id){
        var user = $scope.variables.user.couple[user_id];
        var getParam = "id=" + user.rel_user_id;
        httpRequestService.sendAjaxRequest(ACTION_GET_USER_FULL_INFO_ALL_LANG,getParam,null,false,function(data){
            if(data.type == 0){
                var modalInstance = $modal.open({
                    templateUrl:'./views/register.html',
                    controller: 'UserEditInstanceCtrl',
                    windowClass: 'app-modal-large',
                    size: 'lg',
                    resolve: {
                        user: function () {
                            data.value.birth_date = data.value.birth_date != 0 ? new Date(data.value.birth_date*1000):undefined;
                            data.value.formType = 'sexSensitiveForm';
                            return data.value;
                        }
                    }
                });
                modalInstance.result.then(function (reload) {
                    if(!reload) return;
                    $scope.loadUserProfile($scope.variables.user.id)
                });
            }
        });
    }

    $scope.removeChild = function(user_id){
        var usrParent = $scope.variables.user;
        var user = $scope.variables.user.children[user_id];
        var getParam = "rel_user_id=" + user.rel_user_id+"&user_id="+user.user_id;
        var modalInstance = $modal.open({
            templateUrl:'./views/mypage/confirmPopup.html',
            controller: 'UserRemoveChildConfirmModalInstanceCtrl',
            windowClass: 'app-modal-small',
            resolve: {
                user: function () {
                    return usrParent;
                },
                userParam: function () {
                    return getParam;
                }

            }
        });
        modalInstance.result.then(function (reload) {
            if(!reload) return;
            $scope.loadUserProfile($scope.variables.user.id);
        });
    }

    $scope.removeCouple = function(user_id){
        var usrParent = $scope.variables.user;
        var user = $scope.variables.user.couple[user_id];
        var getParam = "rel_user_id=" + user.rel_user_id+"&user_id="+user.user_id;
        var modalInstance = $modal.open({
            templateUrl:'./views/mypage/confirmPopup.html',
            controller: 'UserRemoveChildConfirmModalInstanceCtrl',
            windowClass: 'app-modal-small',
            resolve: {
                user: function () {
                    return usrParent;
                },
                userParam: function () {
                    return getParam;
                }
            }
        });
        modalInstance.result.then(function (reload) {
            if(!reload) return;
            $scope.loadUserProfile($scope.variables.user.id)
        });
    }

    $scope.showCarousel = function(){
        //var getParam = "id=" + $scope.variables.selectedAlbum.id;
        //httpRequestService.sendAjaxRequest(ACTION_GET_ALBUM_ALL_LANGUAGES,getParam,null,false,function(data){
        //if(data.type == 0){
        var modalInstance = $modal.open({
            templateUrl:'./views/mypage/carousel.html',
            controller: 'showCarouselInstanceCtrl',
            windowClass: 'app-modal-small',
            resolve: {
                album: function () {
                    return $scope.variables.pictures
                }
            }
        });
        modalInstance.result.then(function (reload) {
            //if(!reload) return;
            //$scope.loadAlbums($scope.variables.user.id)
        });
        //}
        //});
    }
    $scope.checkLoadPhotos = function($event, $index, action, album_id, type){
        //action = 0 load photos
        //action = 1 create photos
        //action = 2 edit album
        //action = 3 remove album
        //action = 4 play video
        //action = 5 edit video
        //action = 6 remove video
        var parentToggle = $('.' + type + '-elem-' + album_id +'-' + action).closest('.img-layout');
        switch (action) {
            case 0:
                if(angular.element(parentToggle).hasClass('more-options')){
                    $scope.loadAlbumPhotos($index);
                    $event.stopPropagation();
                }
                //(angular.element(parentToggle).hasClass('more-options')) ? $scope.loadAlbumPhotos($index) : '';
                break;
            case 1:
              if(angular.element(parentToggle).hasClass('more-options')){
                $scope.createAlbumPicture($index);
                $event.stopPropagation();
              }
                break;
            case 2:
                if(angular.element(parentToggle).hasClass('more-options')){
                    $scope.editAlbum($index);
                    $event.stopPropagation();
                }
                break;
            case 3:
                if(angular.element(parentToggle).hasClass('more-options')){
                    $scope.removeAlbum($index);
                    $event.stopPropagation();
                }
                break;
            case 4:
                if(angular.element(parentToggle).hasClass('more-options')){
                    $scope.playVideo($index);
                    $event.stopPropagation();
                }
                break;
            case 5:
                if(angular.element(parentToggle).hasClass('more-options')){
                    $scope.editVideo($index);
                    $event.stopPropagation();
                }
                break;
            case 6:
                if(angular.element(parentToggle).hasClass('more-options')){
                    $scope.removeVideo($index);
                    $event.stopPropagation();
                }
                break;
            default:
                /*if(angular.element(parentToggle).hasClass('more-options')){
                    $scope.loadAlbumPhotos($index);
                    $event.stopPropagation();
                }*/
        }
    }
    $scope.checkLoadPhotosDesktop = function($event, $index, action){
         //action = 1 create photos
         //action = 2 edit album
         //action = 3 remove album
        //action = 5 edit video
        //action = 6 remove video
        switch (action) {
            case 1:
                $scope.createAlbumPicture($index);
                $event.stopPropagation();
                break;
            case 2:
                $scope.editAlbum($index);
                $event.stopPropagation();
                break;
            case 3:
                $scope.removeAlbum($index);
                $event.stopPropagation();
                break;
            case 5:
                $scope.editVideo($index);
                $event.stopPropagation();
                break;
            case 6:
                $scope.removeVideo($index);
                $event.stopPropagation();
                break;
            default:
        }
    }
});

familyApp.directive('toggleClass', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            scope.toggleClass = true;
            scope.ios = false;
            if((scope.new_width <= scope.mobileWidth)){
                scope.toggleClass = false;
                element.bind('click', function() {
                    $(".memory-bg .img-layout").removeClass(attrs.toggleClass);
                    element.toggleClass(attrs.toggleClass);
                    (element.hasClass(attrs.toggleClass)) ? scope.toggleClass=true : scope.toggleClass=false;
                });
            }
            else {
                if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) {
                    scope.ios = true;
                    scope.toggleClass = false;
                    element.bind("click", function(event) {
                        $(".memory-bg .img-layout").removeClass(attrs.toggleClass);
                        element.toggleClass(attrs.toggleClass);
                        (element.hasClass(attrs.toggleClass)) ? scope.toggleClass=true : scope.toggleClass=false;
                    });
                }
            }
        }
    };
});
familyApp.directive('isolateClick', function() {
    return {
        link: function(scope, elem) {
            elem.on('click', function(e){
                e.stopPropagation();
            });
        }
    };
});