familyApp.controller('UserEditInstanceCtrl', function ($scope, $modalInstance,$rootScope,$location, httpRequestService, user) {

    $scope.variables = {};

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate', 'yyyy-MM-dd'];
    $scope.format = $scope.formats[4];


    $scope.variables.user = user;
    $scope.variables.mode = 'edit';
    $scope.variables.formType = $scope.variables.user.formType;
    //$scope.variables.user.dead_date = user.dead_date*1000;
    //$scope.variables.user.birth_date = user.birth_date*1000;

    var ACTION_GET_COUNTRIES_WITH_ALL_LANG = "getCountriesWithAllLang";
    var ACTION_UPDATE_USER = "updateUser";

    $scope.variables.currentUser = $rootScope.currentUser;

    this.loadCountryList = function(){
        httpRequestService.sendAjaxRequest(ACTION_GET_COUNTRIES_WITH_ALL_LANG,null,null,false,function(data){
            if(data.type == 0){
                $scope.variables.coutries = data.value;
            }
        });
    }

    this.loadCountryList();

    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

    $scope.openDead = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        
        $scope.deadOpened = true;
    };

    $scope.setFile = function(el){
        $scope.variables.fileObject = el.files[0];
    }

    $scope.variables.errors = [];

    $scope.submitForm = function(){
        var formData = new FormData();
        
        if (typeof $scope.variables.user.birth_date == 'object'){
            if($scope.variables.user.birth_date instanceof Date) {

            }
        }
        else if (typeof $scope.variables.user.birth_date == 'string' || typeof $scope.variables.user.birth_date == 'number') {
            $scope.variables.user.birth_date = new Date($scope.variables.user.birth_date);
        }
        else {

        }
       
        //if isset dead date or not
        if($scope.variables.user.dead_date == undefined || $scope.variables.user.dead_date == null){
            $scope.variables.user.dead_date = null;
        }
        else {
            // define dead date type
            if (typeof $scope.variables.user.dead_date == 'object'){
                if($scope.variables.user.dead_date instanceof Date) {

                }
            }
            else if (typeof $scope.variables.user.dead_date == 'string' || typeof $scope.variables.user.dead_date == 'number') {
                $scope.variables.user.dead_date = new Date($scope.variables.user.dead_date);
            }
            else {

            }
            //$scope.variables.user.dead_date = $scope.variables.user.dead_date.getTime()/1000;
        }



        //$scope.variables.user.birth_date = $scope.variables.user.birth_date.getTime()/1000;
        formData.append("data", JSON.stringify($scope.variables.user));
        if($scope.variables.fileObject && typeof $scope.variables.fileObject.name !== 'undefined'){
            formData.append('profile_picture', $scope.variables.fileObject);
        }

        httpRequestService.sendAjaxRequest(ACTION_UPDATE_USER,null,formData,true,function(data){
            $scope.variables.errors = [];
            if(data.type == 0){
                $modalInstance.close(data.value);
            }
            else if (data.type == 1 ){
                $.each(data.error, function (i, v){
                    $scope.variables.errors.push(v);
                });

            }
        });
    }
    $scope.scopeCheckRequired = function (form_values) {
      var required = 1;
      if (typeof form_values == 'undefined' || form_values.length == 0) {
        return required;
      }
      var multilang_req_fields = [
        'birth_place',
        'city',
        'first_name',
        'last_name',
      ];

      $.each(form_values, function (lang, value) {
        var all_fields_exist = true;
        $.each(multilang_req_fields, function (i, v) {
          if (value[v] == undefined || value[v] == '') {
            all_fields_exist = false;
          }
        });
        if (all_fields_exist) {
          required = 0;
          return required;
        }
      });
      return required;
    };
});

familyApp.controller('NewAlbumModalInstanceCtrl', function ($scope, $modalInstance,httpRequestService) {
    var ACTION_CREATE_ALBUM = "createAlbum";

    $scope.variables = {};
    $scope.variables.album  = {};
    $scope.variables.album.is_public = 1;
    $scope.variables.mode == 'add';

    $scope.submitNewAlbumForm =  function(){
        httpRequestService.sendAjaxRequest(ACTION_CREATE_ALBUM,null, ("data=" + JSON.stringify($scope.variables.album)),true,function(data){
            if(data.type == 0) {
                $modalInstance.close(true);
            }
        },null,true);
    }
});

familyApp.controller('AlbumEditInstanceCtrl', function ($scope, $modalInstance,httpRequestService,album, checkRequiredService) {
    var ACTION_EDIT_ALBUM = "editAlbum";

    $scope.variables = {};
    $scope.variables.album = album;
    $scope.variables.mode = 'edit';


    $scope.submitNewAlbumForm =  function(){
        httpRequestService.sendAjaxRequest(ACTION_EDIT_ALBUM,null, ("data=" + JSON.stringify($scope.variables.album)),true,function(data){
            if(data.type == 0) {
                $modalInstance.close(true);
            }
        });
    }
    $scope.scopeModalCheckRequired = function (form_values, fields) {
      var checked = checkRequiredService.check(form_values, fields);
      console.log(checked);
      return checked;
    }
});

familyApp.controller('NewAlbumPictureModalInstanceCtrl', function ($scope, $modalInstance,$timeout,httpRequestService,albumId,max_upload, checkRequiredService) {
    var ACTION_CREATE_ALBUM_PICTURE = "createAlbumPicture";

    $scope.variables = {};
    $scope.variables.albumPicture  = {};
    $scope.variables.albumPicture.album_id = albumId;
    $scope.allowFileUpload = false;
    $scope.variables.max_upload = max_upload;

    $scope.setFile = function(el){
        $scope.variables.fileObject = el.files[0];
    }

    $scope.submitNewAlbumPictureForm =  function(){
        var formData = new FormData();
        formData.append("data", JSON.stringify($scope.variables.albumPicture));
        if($scope.variables.fileObject && typeof $scope.variables.fileObject.name !== 'undefined'){
            if ($scope.variables.fileObject.type == 'image/jpeg' || $scope.variables.fileObject.type == 'image/png' || $scope.variables.fileObject.type == 'image/gif' || $scope.variables.fileObject.type == 'image/jpg' || $scope.variables.fileObject.type == 'image/bmp') {
                formData.append('file', $scope.variables.fileObject);
            }
        }
        httpRequestService.sendAjaxRequest(ACTION_CREATE_ALBUM_PICTURE,null, formData,true,function(data){
            if(data.type == 0) {
                $modalInstance.close(true);
            }
        },null,true);
    }
    $scope.scopeModalCheckRequired = function (form_values, fields) {
      return checkRequiredService.check(form_values, fields);
    }
});

familyApp.controller('NewVideoModalInstanceCtrl', function ($scope, $modalInstance,httpRequestService,userId,videoType, checkRequiredService) {
    var ACTION_CREATE_VIDEO = "createNewVideo";

    $scope.variables = {};
    $scope.variables.video  = {};
    $scope.variables.video.user_id = userId;
    $scope.variables.video.type = 0;
    if(videoType == 'embed') {
        $scope.variables.video.type = 1;
    }
    $scope.variables.userVideoType = videoType;

    $scope.setFile = function(el){
        $scope.$apply(function($scope) {
            $scope.variables.fileObject = el.files[0];
        });
    }

    $scope.submitNewVideoForm =  function(){
        var formData = new FormData();
        $scope.is_video = false;
        if($scope.variables.fileObject && typeof $scope.variables.fileObject.name !== 'undefined'){
            if ($scope.variables.fileObject.type == 'video/mp4' || $scope.variables.fileObject.type == 'video/mpeg' || $scope.variables.fileObject.type == 'video/x-flv' || $scope.variables.fileObject.type == 'video/avi' || $scope.variables.fileObject.type == 'video/wmv') {
                formData.append('file', $scope.variables.fileObject);
                $scope.variables.video.file_format = $scope.variables.fileObject.type;
                $scope.is_video = true;
            }
        }
        formData.append("data", JSON.stringify($scope.variables.video));
        if ($scope.is_video || $scope.variables.video.embed_code) {
            httpRequestService.sendAjaxRequest(ACTION_CREATE_VIDEO,null, formData,true,function(data){
             if(data.type == 0) {
             $modalInstance.close(true);
             }
             },null,true);
        }
    }
    $scope.scopeModalCheckRequired = function (form_values, fields) {
      return checkRequiredService.check(form_values, fields);
    }
});

familyApp.controller('VideoPlayModalInstanceCtrl', function ($scope, $modalInstance,$modal,httpRequestService,video,$sce) {
    var ACTION_REMOVE_VIDEO = "removeVideoItem";
    var ACTION_GET_VIDEO_INFO_ALL_LANGUAGES = "getVideo_allLang";

    $scope.variables = {};
    $scope.variables.video  = video;
    //$scope.variables.video.embed_code = $sce.trustAsHtml($scope.variables.video.embed_code);
    $scope.toTrustedHTML = function( html ){
        return $sce.trustAsHtml( html );
    }
});

familyApp.controller('VideoEditModalInstanceCtrl', function ($scope, $modalInstance,httpRequestService,video,$sce, checkRequiredService) {
    var ACTION_EDIT_VIDEO_INFO = "editVideoInfo";
    $scope.variables = {};
    $scope.variables.video  = video;
    $scope.variables.mode  = "edit";

    $scope.submitNewVideoForm =  function(){

        httpRequestService.sendAjaxRequest(ACTION_EDIT_VIDEO_INFO,null, ("data=" + JSON.stringify($scope.variables.video)),true,function(data){
            if(data.type == 0) {
                $modalInstance.close(true);
            }
        });
    }
    $scope.scopeModalCheckRequired = function (form_values, fields) {
      return checkRequiredService.check(form_values, fields);
    }
});

familyApp.controller('AlbumPictureInfoModalInstanceCtrl', function ($scope, $modalInstance,$modal,httpRequestService,picture,album,userDetails,$filter) {
    //var ACTION_REMOVE_ALBUM_PICTURE = "removeAlbumPicture";
    var ACTION_GET_ALBUM_PICTURE_ALL_LANGUAGES = "getAlbumPicture_allLang";

    $scope.variables = {};
    $scope.variables.picture  = picture;
    $scope.variables.albumPictures  = album;
    $scope.variables.user_details = userDetails;

    //filter the array
    var foundItem = $filter('filter')($scope.variables.albumPictures, { id: $scope.variables.picture.id  }, true)[0];
    //get the index
    var index =  $scope.variables.albumPictures.indexOf(foundItem );


    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.direction = 'left';
    $scope.currentIndex = index;

    $scope.setCurrentSlideIndex = function (index) {
        $scope.direction = (index > $scope.currentIndex) ? 'left' : 'right';
        $scope.currentIndex = index;
    };

    $scope.isCurrentSlideIndex = function (index) {
        return $scope.currentIndex === index;
    };

    $scope.prevSlide = function () {
        $scope.direction = 'left';
        $scope.currentIndex = ($scope.currentIndex < $scope.variables.albumPictures.length - 1) ? ++$scope.currentIndex : 0;
    };

    $scope.nextSlide = function () {
        $scope.direction = 'right';
        $scope.currentIndex = ($scope.currentIndex > 0) ? --$scope.currentIndex : $scope.variables.albumPictures.length - 1;
    };


    $scope.editPicture = function(ind){
        //$scope.variables.albumPictures[ind].active = true;
        //var getParam = "id=" + $scope.variables.picture.id;
        var getParam = "id=" + $scope.variables.albumPictures[ind].id;
        httpRequestService.sendAjaxRequest(ACTION_GET_ALBUM_PICTURE_ALL_LANGUAGES,getParam,null,false,function(data){
            if(data.type == 0){
                var modalInst = $modal.open({
                    templateUrl:'./views/mypage/albumPicturePopup.html',
                    controller: 'PictureEditModalInstanceCtrl',
                    windowClass: 'app-modal-small',
                    animation: true,
                    resolve: {
                        picture: function () {
                            return data.value
                        }
                    }
                });
                modalInst.result.then(function (reload) {
                    if(!reload) return;
                    $modalInstance.close(true)
                });
            }
        });
    }

    $scope.removePicture = function(ind){
        //var getParam = "id=" + $scope.variables.picture.id;
        var getParam = "id=" + $scope.variables.albumPictures[ind].id;
        var modalInstance = $modal.open({
            templateUrl:'./views/mypage/confirmPopup.html',
            controller: 'PictureRemoveConfirmModalInstanceCtrl',
            windowClass: 'app-modal-small',
            animation: true,
            resolve: {
                pictureId: function () {
                    return getParam
                }
            }
        });
        modalInstance.result.then(function (reload) {
            if(!reload) return;
            $modalInstance.close(true);
        });
    }
});

familyApp.controller('PictureRemoveConfirmModalInstanceCtrl', function ($scope, $modalInstance,httpRequestService,pictureId) {
    var ACTION_REMOVE_ALBUM_PICTURE = "removeAlbumPicture";
    $scope.variables = {};
    $scope.variables.confirmText = "picDelete";
    var getParam = pictureId;

    $scope.close = function(){
        $modalInstance.close();
    }

    $scope.confirm = function(){
        httpRequestService.sendAjaxRequest(ACTION_REMOVE_ALBUM_PICTURE,getParam,null,false,function(data){
            if(data.type == 0){
                $modalInstance.close(true);
            }
        });
    }


});

familyApp.controller('PictureEditModalInstanceCtrl', function ($scope, $modalInstance,httpRequestService,picture, checkRequiredService) {
    var ACTION_EDIT_ALBUM_PICTURE = "editAlbumPicture";

    $scope.variables = {};
    $scope.variables.albumPicture  = picture;
    $scope.variables.mode  = "edit";

    $scope.submitNewAlbumPictureForm =  function(){

        httpRequestService.sendAjaxRequest(ACTION_EDIT_ALBUM_PICTURE,null, ("data=" + JSON.stringify($scope.variables.albumPicture)),true,function(data){
            if(data.type == 0) {
                $modalInstance.close(true);
            }
        });
    }
    $scope.scopeModalCheckRequired = function (form_values, fields) {
      return checkRequiredService.check(form_values, fields);
    }
});

familyApp.controller('AddUserToTRee', function ($scope, $modalInstance,httpRequestService,$modal, user, relation_users) {
    var ACTION_ADD_TO_TREE = "addUserToTreeSubmit";

    $scope.variables = {};
    $scope.variables.user = user;
    $scope.variables.relationUsers = [];

    for (var i in relation_users){
        $scope.variables.relationUsers.push({id:relation_users[i].user_id,name:relation_users[i].first_name + " " + relation_users[i].last_name});
    }

    $scope.submitAddToTree =  function(){
        var getParam = "current_user_id=" + $scope.variables.user.user_id + "&related_user_id=" + $scope.variables.related_selected_user_id;

        httpRequestService.sendAjaxRequest(ACTION_ADD_TO_TREE, null, getParam, true,function(data){
            if(data.type == 0) {
                var responseModalInstance = $modal.open({
                    templateUrl:'./views/request/generalPopup.html',
                    controller: 'GeneralPopupController',
                    windowClass: 'app-modal-small',
                    resolve: {
                        message: function () {
                            return "tree_user_added_success"
                        },
                        icon_path: function () {
                            return "./img/success.png"
                        }
                    }
                });
                $modalInstance.close(true);
            }
            else if(data.type == 2){
                var responseModalInstance = $modal.open({
                    templateUrl:'./views/request/generalPopup.html',
                    controller: 'GeneralPopupController',
                    windowClass: 'app-modal-small',
                    resolve: {
                        message: function () {
                            return "tree_user_exist_error"
                        },
                        icon_path: function () {
                            return "./img/warning.png"
                        }
                    }
                });
            }
        });
    };

    $scope.submitMergeWithTree =  function(){
        var ACTION_MERGE_WITH_TREE = "mergeUserWithTreeUserSubmit";

        var getParam = "current_user_id=" + $scope.variables.user.user_id + "&related_user_id=" + $scope.variables.related_selected_user_id;
        httpRequestService.sendAjaxRequest(ACTION_MERGE_WITH_TREE, null, getParam, true,function(data){
            if(data.type == 0) {
                var responseModalInstance = $modal.open({
                    templateUrl:'./views/request/generalPopup.html',
                    controller: 'GeneralPopupController',
                    windowClass: 'app-modal-small',
                    resolve: {
                        message: function () {
                            return "tree_user_merged_success"
                        },
                        icon_path: function () {
                            return "./img/success.png"
                        }
                    }
                });
                $modalInstance.close(true);
            }
            else if(data.type == 2){
                var responseModalInstance = $modal.open({
                    templateUrl:'./views/request/generalPopup.html',
                    controller: 'GeneralPopupController',
                    windowClass: 'app-modal-small',
                    resolve: {
                        message: function () {
                            return "tree_user_exist_error"
                        },
                        icon_path: function () {
                            return "./img/warning.png"
                        }
                    }
                });
            }
        });
    }
});
familyApp.controller('openChat', function ($scope, $modalInstance,httpRequestService,$modal, user) {
  var ACTION_SEND_MESSAGE_TO_USER = "chatSendFirstMessage";
  
  $scope.variables = {};
  $scope.variables.recipient_first_name = user.first_name;
  $scope.variables.form_status = '';
  
  $scope.cancelFormSubmt = function(){
    $modalInstance.close(true);
  };

  $scope.sendMessage = function(){
    $scope.variables.form_status = 'active';
    $scope.form.sender = $scope.currentUser.id;  
    $scope.form.recipient = user.id;
    $scope.form.timestamp = $.now();
    
    var params = $.param($scope.form);
    httpRequestService.sendAjaxRequest(ACTION_SEND_MESSAGE_TO_USER, null, params, true, function(data){
        if(data.status == 1) {
          $scope.variables.form_status = 'submitted';
          $modalInstance.close();
        }
    },function(){}, false, true, 'chat_message_sent_success');
  }
});
familyApp.controller('DeleteUserFromTRee', function ($scope, $modalInstance,httpRequestService,$modal, user) {
    var ACTION_DELETE_FROM_TREE ="deleteUserFromTree";
    $scope.variables = {};
    $scope.variables.user = user;

    $scope.cancelFormSubmt = function(){
        $modalInstance.close(true);
    }

    $scope.deleteFromTree = function(){
        var getParam = "current_user_id=" + $scope.variables.user.user_id;
        httpRequestService.sendAjaxRequest(ACTION_DELETE_FROM_TREE, null, getParam, true,function(data){
            if(data.type == 0) {
                var responseModalInstance = $modal.open({
                    templateUrl:'./views/request/generalPopup.html',
                    controller: 'GeneralPopupController',
                    windowClass: 'app-modal-small',
                    resolve: {
                        message: function () {
                            return "tree_user_delete_success"
                        },
                        icon_path: function () {
                            return "./img/success.png"
                        }
                    }
                });
                $modalInstance.close(true);
            }
            else if(data.type == 2){
                var responseModalInstance = $modal.open({
                    templateUrl:'./views/request/generalPopup.html',
                    controller: 'GeneralPopupController',
                    windowClass: 'app-modal-small',
                    resolve: {
                        message: function () {
                            return "tree_user_delete_error_has_child"
                        },
                        icon_path: function () {
                            return "./img/warning.png"
                        }
                    }
                });
            }
            else if(data.type == 3){
                var responseModalInstance = $modal.open({
                    templateUrl:'./views/request/generalPopup.html',
                    controller: 'GeneralPopupController',
                    windowClass: 'app-modal-small',
                    resolve: {
                        message: function () {
                            return "tree_user_not_exist_error"
                        },
                        icon_path: function () {
                            return "./img/warning.png"
                        }
                    }
                });
            }
        });
    }
});

familyApp.controller('GeneralPopupController', function ($scope, $modalInstance,httpRequestService, $modal, message, icon_path) {
    $scope.variables = {};
    $scope.variables.message = message;
    $scope.variables.icon_path = icon_path;
});


familyApp.controller('NewCoupleModalInstanceCtrl', function ($scope, $modalInstance,httpRequestService,userType,user) {
    var ACTION_CREATE_NEW_USER = "createNewMember";
    var ACTION_GET_COUNTRIES_WITH_ALL_LANG = "getCountriesWithAllLang";

    $scope.variables = {};
    $scope.variables.mode  = "register";
    $scope.variables.formType = userType;

    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

    $scope.setFile = function(el){
        $scope.variables.fileObject = el.files[0];
    }

    this.loadCountryList = function(){
        httpRequestService.sendAjaxRequest(ACTION_GET_COUNTRIES_WITH_ALL_LANG,null,null,false,function(data){
            if(data.type == 0){
                $scope.variables.coutries = data.value;
            }
        });
    }

    this.loadCountryList();

    $scope.submitForm = function(){
        var formData = new FormData();
        $scope.variables.user.role = 2;
        $scope.variables.user.rel_type = "couple";
        $scope.variables.user.relative_id  = user.id;
        $scope.variables.user.dead_date = null;
        $scope.variables.user.sex = 0;
        if (angular.isUndefined($scope.variables.user.country)) {
            $scope.variables.user.country = 0;
        }
        //$scope.variables.user.birth_date = $scope.variables.user.birth_date.getTime()/1000;
        formData.append("data", JSON.stringify($scope.variables.user));
        if($scope.variables.fileObject && typeof $scope.variables.fileObject.name !== 'undefined'){
            formData.append('profile_picture', $scope.variables.fileObject);
        }
        httpRequestService.sendAjaxRequest(ACTION_CREATE_NEW_USER,null,formData,true,function(data){
            if(data.type == 0){
                $modalInstance.close(data.value);
            }
        });
    }

});

familyApp.controller('NewChildModalInstanceCtrl', function ($scope, $modalInstance,httpRequestService,userType, user) {
    var ACTION_CREATE_NEW_USER = "createNewMember";
    var ACTION_GET_COUNTRIES_WITH_ALL_LANG = "getCountriesWithAllLang";

    $scope.variables = {};
    $scope.variables.mode  = "register";
    $scope.variables.formType = userType;

    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

    $scope.setFile = function(el){
        $scope.variables.fileObject = el.files[0];
    }

    this.loadCountryList = function(){
        httpRequestService.sendAjaxRequest(ACTION_GET_COUNTRIES_WITH_ALL_LANG,null,null,false,function(data){
            if(data.type == 0){
                $scope.variables.coutries = data.value;
            }
        });
    }

    this.loadCountryList();

    $scope.submitForm = function(){
        var formData = new FormData();
        $scope.variables.user.role = 2;
        $scope.variables.user.rel_type = "child";
        $scope.variables.user.relative_id  = user.id;
        $scope.variables.user.dead_date = null;
        if (angular.isUndefined($scope.variables.user.country)) {
            $scope.variables.user.country = 0;
        }
        //$scope.variables.user.birth_date = $scope.variables.user.birth_date.getTime()/1000;
        formData.append("data", JSON.stringify($scope.variables.user));
        if($scope.variables.fileObject && typeof $scope.variables.fileObject.name !== 'undefined'){
            formData.append('profile_picture', $scope.variables.fileObject);
        }

        httpRequestService.sendAjaxRequest(ACTION_CREATE_NEW_USER,null,formData,true,function(data){
         if(data.type == 0){
             $modalInstance.close(data.value);
         }
         });
    }
});

familyApp.controller('UserChildModalInstanceCtrl', function ($scope, $modalInstance,$location, httpRequestService,user) {
    $scope.variables = {};
    $scope.variables.user = user;
    $scope.variables.mode = 'view';

    var ACTION_GET_COUNTRIES_WITH_ALL_LANG = "getCountriesWithAllLang";


    this.loadCountryList = function(){
        httpRequestService.sendAjaxRequest(ACTION_GET_COUNTRIES_WITH_ALL_LANG,null,null,false,function(data){
            if(data.type == 0){
                $scope.variables.coutries = data.value;
            }
        });
    }

    this.loadCountryList();

    $scope.viewProfile = function(){
        $modalInstance.close();
        $location.path("/profile/"+ $scope.variables.user.id);
    }

    $scope.approveUser = function(){
        $modalInstance.close({id:$scope.variables.user.id,approved:true});
    }

    $scope.rejectUser = function(){
        $modalInstance.close({id:$scope.variables.user.id,approved:false});
    }
});

familyApp.controller('UserRemoveChildConfirmModalInstanceCtrl', function ($scope, $modalInstance,$location, httpRequestService,user,userParam) {
    $scope.variables = {};
    $scope.variables.confirmText = "relDelete";
    var getParam = userParam;
    var ACTION_REMOVE_RELATIVES = "removeRelatives";

    $scope.close = function(){
        $modalInstance.close();
    }

    $scope.confirm = function(){
        
        httpRequestService.sendAjaxRequest(ACTION_REMOVE_RELATIVES,getParam,null,false,function(data){
         if(data.type == 0){
             $modalInstance.close(data.value);
         }
         });
    }

});

familyApp.controller('showCarouselInstanceCtrl', function ($scope, $modalInstance,httpRequestService,album) {
    var ACTION_EDIT_ALBUM_PICTURE = "editAlbumPicture";

    $scope.variables = {};
    $scope.variables.albumPictures  = album;
    $scope.ok = function () {
        $scope.modalInstance.close();
    };
});

familyApp.controller('RemoveAlbumConfirmModalInstanceCtrl', function ($scope, $modalInstance,$location, httpRequestService,albumId) {
    $scope.variables = {};
    $scope.variables.confirmText = "albumDelete";
    var getParam = albumId;
    var ACTION_REMOVE_ALBUM = "removeAlbum";

    $scope.close = function(){
        $modalInstance.close();
    }

    $scope.confirm = function(){

        httpRequestService.sendAjaxRequest(ACTION_REMOVE_ALBUM,getParam,null,false,function(data){
            if(data.type == 0){
                $modalInstance.close(true);
            }
        });
    }

});

familyApp.controller('RemoveVideoConfirmModalInstanceCtrl', function ($scope, $modalInstance,$location, httpRequestService,videoId) {
    $scope.variables = {};
    $scope.variables.confirmText = "videoDelete";
    var getParam = videoId;
    var ACTION_REMOVE_VIDEO = "removeVideoItem";

    $scope.close = function(){
        $modalInstance.close();
    }

    $scope.confirm = function(){

        httpRequestService.sendAjaxRequest(ACTION_REMOVE_VIDEO,getParam,null,false,function(data){
            if(data.type == 0){
                $modalInstance.close(true);
            }
        });
    }

});

familyApp.directive('validImageFile', function () {
    return {
        require: 'ngModel',
        link: function (scope, elem, attrs, ngModel) {
            var validFormats = ['jpg','jpeg','png', 'gif', 'bmp'];
            elem.bind('change', function () {
                validImage(false);
                scope.$apply(function () {
                    ngModel.$render();
                });
            });
            ngModel.$render = function () {
                ngModel.$setViewValue(elem.val());
            };
            function validImage(bool) {
                ngModel.$setValidity('extension', bool);
            }
            ngModel.$parsers.push(function(value) {
                var ext = value.substr(value.lastIndexOf('.')+1);
                ext = ext.toLowerCase();
                if(ext=='') return;
                if(validFormats.indexOf(ext) == -1){
                    return value;
                }
                validImage(true);
                return value;
            });
        }
    };
});
familyApp.animation('.slide-animation', function ($animateCss) {
    return {
        beforeAddClass: function (element, className, done) {
            var scope = element.scope();

            if (className == 'ng-hide') {
                var finishPoint = element.parent().width();
                if(scope.direction !== 'right') {
                    finishPoint = -finishPoint;
                }
                TweenMax.to(element, 0, {left: finishPoint, onComplete: done });
            }
            else {
                done();
            }
        },
        removeClass: function (element, className, done) {
            var scope = element.scope();

            if (className == 'ng-hide') {
                element.removeClass('ng-hide');

                var startPoint = element.parent().width();
                if(scope.direction === 'right') {
                    startPoint = -startPoint;
                }

                TweenMax.fromTo(element, 0, { left: startPoint }, {left: 0, onComplete: done });
            }
            else {
                done();
            }
        }
    };
});
familyApp.directive('uibDatepickerPopup', function (dateFilter, uibDateParser, uibDatepickerPopupConfig) {
    return {
        restrict: 'A',
        priority: 1,
        require: 'ngModel',
        link: function (scope, element, attr, ngModel) {
            var dateFormat = attr.uibDatepickerPopup || uibDatepickerPopupConfig.datepickerPopup;
            ngModel.$validators.date = function(modelValue, viewValue) {
                var value = viewValue || modelValue;

                if (!attr.ngRequired && !value) {
                    return true;
                }

                if (angular.isNumber(value)) {
                    value = new Date(value);
                }
                if (!value) {
                    return true;
                } else if (angular.isDate(value) && !isNaN(value)) {
                    return true;
                } else if (angular.isString(value)) {
                    var date = uibDateParser.parse(value, dateFormat);
                    return !isNaN(date);
                } else {
                    return false;
                }
            };
        }
    };
});


