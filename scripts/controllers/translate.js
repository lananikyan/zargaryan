familyApp.controller('TranslateController', function($translate,$rootScope, $scope,httpRequestService) {
    var ACTION_GET_CURRENT_LANGUAGE = "getCurrentLangauge";
    var ACTION_CHANGE_CURRENT_LANGUAGE = "changeCurrentLanguage";

    httpRequestService.sendAjaxRequest(ACTION_GET_CURRENT_LANGUAGE,null,null,false,function(data){
        $translate.use(data.value);
        $rootScope.language = data.value;

    });

    $scope.changeLanguage = function (langKey) {
        httpRequestService.sendAjaxRequest(ACTION_CHANGE_CURRENT_LANGUAGE,null,'lang='+langKey,true,function(data){
            $rootScope.language = langKey;
        });
        $translate.use(langKey);
    };
});