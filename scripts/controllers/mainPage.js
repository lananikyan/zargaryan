familyApp.controller('MainPageController', function($rootScope,$scope,$routeParams,httpRequestService,$location,$timeout) {
    var me = this;
   
    $rootScope.page = $routeParams.page;
    $rootScope.tab = null;

    $scope.variables = {};
    $scope.variables.parts = [];
    $scope.pagesInfo = [
        {bg:'#87ff94', detail:'#daffde'},
        {bg:'#fcbe62', detail:'#fcead2'},
        {bg:'#66c3dc', detail:'#d9eeff'},
        {bg:'#ffff57', detail:'#ffff9f'},
        {bg:'#c0c0c0', detail:'#e8e8e8'}
    ];

    var ACTION_GET_SHORT_MAIN_INFO = "getShortMainInfo";
    var ACTION_GET_LONG_MAIN_INFO = "getLongMainInfo";
    var ACTION_GET_SHORT_RANDOM_USER = "getShortRandomUser";

    $scope.$watch(function(){ return $rootScope.language},function(){
        me.loadMainPageInfo();
    })

    $scope.cutString = function(s, n){
        var cut= s.indexOf(' ', n);
        if(cut== -1) return s;
        var ellipsis = '';
        if (cut > n){
            ellipsis = '...';
        }
        return s.substring(0, cut)+ellipsis;
    }

    if($scope.windowWidth < 768) {
        $scope.charLimit =  100;
        $scope.charLimitLogout =  100;
    }
    else {
        $scope.charLimit =  200;
        $scope.charLimitLogout =  220;
    }

    this.loadMainPageInfo = function(){
        httpRequestService.sendAjaxRequest(ACTION_GET_SHORT_MAIN_INFO,null,null,false,function(data){
            $scope.variables.parts = data.value;

            //if($routeParams.type && $routeParams.type != 6){
            if($routeParams.type){
                httpRequestService.sendAjaxRequest(ACTION_GET_LONG_MAIN_INFO,"id=" +$routeParams.type,null,false,function(data){
                    if( data.type == 0 ){
                        $rootScope.tab = $routeParams.type;
                        $scope.variables.selectedPart = data.value;
                        $scope.variables.selectedPageInfo = $scope.pagesInfo[$routeParams.type -1];
                        $scope.variables.currentPageIndex = $routeParams.type -1;
                        var imgExtension = '_inner.jpg';
                        $scope.variables.selectedPart.headerImg = $scope.variables.currentPageIndex+imgExtension;
                    }
                });
            }
            /*else if(!$rootScope.currentUser){
                $scope.tab =  $scope.variables.parts[0].id;
                $location.path("/mainPage/"+ $scope.tab);
            }*/
        });
      /*  if($routeParams.id){
            $location.path("/mainPage/info/" + $scope.variables.randomUser.id);
        }else{
            httpRequestService.sendAjaxRequest(ACTION_GET_SHORT_RANDOM_USER,null,null,false,function(data){
                $scope.variables.randomUser = data.value;
            });
        }*/

    }
})


// Filter below found here: http://jsfiddle.net/tuyyx/

familyApp.filter('truncate', function() {
    return function(text, length, end) {
        if (isNaN(length))
            length = 10;

        if (end === undefined)
            end = "...";

        if (text.length <= length || text.length - end.length <= length) {
            return text;
        } else {
            return String(text).substring(0, length - end.length) + end;
        }

    };
});
familyApp.directive('resize', function ($window) {
    return function (scope, element) {
        var w = angular.element($window);
        scope.getWindowDimensions = function () {
            return {
                'h': w.height(),
                'w': w.width()
            };
        };
        scope.$watch(scope.getWindowDimensions, function (newValue, oldValue) {
            scope.windowHeight = newValue.h;
            scope.windowWidth = newValue.w;

            scope.style = function () {
                return {
                    'height': (newValue.h - 100) + 'px',
                    'width': (newValue.w - 100) + 'px'
                };
            };

        }, true);

        w.bind('resize', function () {
            scope.$apply();
        });
    }
})