familyApp.controller('MessageModalInstanceCtrl', function ($scope, $modalInstance, message) {
    $scope.variables = {};
    $scope.variables.message = message;

    $scope.close = function () {
        $modalInstance.close();
    };
});

familyApp.controller('ArticleModalInstanceCtrl', function ($scope, $modalInstance, article) {
    $scope.variables = {};
    $scope.variables.article = article;

    $scope.approveArticle = function () {
        $modalInstance.close({id:$scope.variables.article.id,approved:true});
    };
    $scope.rejectArticle = function () {
        $modalInstance.close({id:$scope.variables.article.id,approved:false});
    };

});

familyApp.controller('ForumModalInstanceCtrl', function ($scope, $modalInstance, forum) {
    $scope.variables = {};
    $scope.variables.forum = forum;

    $scope.updateForum = function () {

    };

    $scope.approveForum = function () {
        $modalInstance.close({id:$scope.variables.forum.id,approved:true});
    };
    $scope.rejectForum = function () {
        $modalInstance.close({id:$scope.variables.forum.id,approved:false});
    };

});

familyApp.controller('CommentModalInstanceCtrl', function ($scope, $modalInstance, comment) {
    $scope.variables = {};
    $scope.variables.comment = comment;

    $scope.approveComment = function () {
        $modalInstance.close({id:$scope.variables.comment.id,approved:true});
    };
    $scope.rejectComment = function () {
        $modalInstance.close({id:$scope.variables.comment.id,approved:false});
    };

});

familyApp.controller('UserModalInstanceCtrl', function ($scope, $rootScope, $modalInstance,$location, httpRequestService,user) {
    $scope.variables = {};
  $scope.variables.currentUser = {};
    $scope.variables.user = user;
    $scope.variables.mode = 'view&change';
    $scope.variables.currentUser.role = $rootScope.currentUser.role;

    var ACTION_GET_COUNTRIES_WITH_ALL_LANG = "getCountriesWithAllLang";
    var ACTION_UPDATE_USER = "updateUser";

    this.loadCountryList = function(){
        httpRequestService.sendAjaxRequest(ACTION_GET_COUNTRIES_WITH_ALL_LANG,null,null,false,function(data){
            if(data.type == 0){
                $scope.variables.coutries = data.value;
            }
        });
    }

    this.loadCountryList();

  $scope.open = function($event) {
    $event.preventDefault();
    $event.stopPropagation();

    $scope.opened = true;
  };

  $scope.openDead = function($event) {
    $event.preventDefault();
    $event.stopPropagation();

    $scope.deadOpened = true;
  };

    $scope.viewProfile = function(){
        $modalInstance.close();
        $location.path("/profile/"+ $scope.variables.user.id);
    }

    $scope.approveUser = function(){
        $modalInstance.close({id:$scope.variables.user.id,approved:true});
    }

    $scope.rejectUser = function(){
        $modalInstance.close({id:$scope.variables.user.id,approved:false});
    }

    $scope.variables.errors = [];
  
    $scope.submitForm = function () {
      var formData = new FormData();
     
      var formData = new FormData();
        //$scope.variables.user.dead_date = null;
        
      if (typeof $scope.variables.user.birth_date == 'object'){
        if($scope.variables.user.birth_date instanceof Date) {}
      }
      else if (typeof $scope.variables.user.birth_date == 'string') {
        $scope.variables.user.birth_date = new Date($scope.variables.user.birth_date);
      }

      //if isset dead date or not
      if($scope.variables.user.dead_date == undefined || $scope.variables.user.dead_date == null){
        $scope.variables.user.dead_date = null;
      }
      else {
        // define dead date type
        if (typeof $scope.variables.user.dead_date == 'object'){
          if($scope.variables.user.dead_date instanceof Date) {}
        }
        else if (typeof $scope.variables.user.dead_date == 'string') {
          $scope.variables.user.dead_date = new Date($scope.variables.user.dead_date);
        }
        //$scope.variables.user.dead_date = $scope.variables.user.dead_date.getTime()/1000;
      }
      //$scope.variables.user.birth_date = $scope.variables.user.birth_date.getTime()/1000;
      
      formData.append("data", JSON.stringify($scope.variables.user));
      if($scope.variables.fileObject && typeof $scope.variables.fileObject.name !== 'undefined'){
          formData.append('profile_picture', $scope.variables.fileObject);
      }
      httpRequestService.sendAjaxRequest(ACTION_UPDATE_USER,null,formData,true,function(data){
          $scope.variables.errors = [];
          if(data.type == 0){
              $modalInstance.close(data.value);
          }
          else if (data.type == 1 ){
            $.each(data.error, function (i, v){
              $scope.variables.errors.push(v);
            });

          }
      });
    }
    $scope.scopeCheckRequired = function (form_values) {
      var required = 1;
      if (typeof form_values == 'undefined' || form_values.length == 0) {
        return required;
      }
      var multilang_req_fields = [
        'birth_place',
        'city',
        'first_name',
        'last_name',
      ];

      $.each(form_values, function (lang, value) {
        var all_fields_exist = true;
        $.each(multilang_req_fields, function (i, v) {
          if(value[v] == undefined || value[v] == '') {
            all_fields_exist = false;
          }
        });
        if (all_fields_exist) {
          required = 0;
          return required;
        }
      });
      return required;
    };
});

familyApp.controller('UserRejectConfirmModalInstanceCtrl', function ($scope, $modalInstance,httpRequestService,action,userId) {
  $scope.variables = {};
  $scope.variables.confirmText = "rejectUser";
  var getParam = userId;
  var action = action;

  $scope.close = function(){
    $modalInstance.close();
  }

  $scope.confirm = function(){
    httpRequestService.sendAjaxRequest(action,getParam,null,false,function(data){
      if(data.type == 0){
        $modalInstance.close(true);
      }
    });
  }


});