familyApp.controller('AdminController', function($rootScope,$scope,$routeParams,$location,$http,httpRequestService ,$modal,$timeout) {
    var me = this;
    var ACTION_GET_VISIT_COUNT = "getVisitCount";
    var ACTION_GET_SHORT_MAIN_INFO = "getShortMainInfo";
    var ACTION_GET_LONG_MAIN_INFO_ALL_LANG = "getLongMainInfo_allLang";
    var ACTION_SET_LONG_MAIN_INFO_ALL_LANG = "setLongMainInfo_allLang";

    var ACTION_GET_NEW_MESSAGES = "getNewMessages";
    var ACTION_SET_MESSAGE_OLD = "setMessageOld";

    var ACTION_GET_USERS_SHORT_LIST = "getUserList";
    var ACTION_APPROVE_USER = "approveUser";
    var ACTION_REJECT_USER = "rejectUser";
    var ACTION_GET_USER_FULL_INFO_ALL_LANG = "getUserData_allLang";

    var ACTION_GET_FORUM_NOTIFICATION_COUNT = "forumCountAdmin";
    var ACTION_GET_FORUM_COMMENT_NOTIFICATION_COUNT = "forumCommentCountAdmin";

    var ACTION_GET_USERS_WAITING_APPROVE_COUNT = "getUsersWaitingApproveCount";
    var ACTION_GET_NEW_MESSAGES_COUNT = "getNewMessagesCount";
    var ACTION_GET_NEW_ARTICLE_COUNT = "getNewArticleCount";

    var ACTION_GET_ARTICLES_SHORT_LIST = "getArticlesShortListForAdmin";
    var ACTION_APPROVE_ARTICLE = "approveArticle";
    var ACTION_REJECT_ARTICLE = "rejectArticle";
    var ACTION_ARTICLE_FULL_INFO_ALL_LANG = "getArticlesDetail_allLang";

    var ACTION_CREATE_NEW_USER = "createNewUser";
    var ACTION_GET_COUNTRIES_WITH_ALL_LANG = "getCountriesWithAllLang";

    var ACTION_GET_USERS_LIST_FOR_TREE = "getUserListForTree";

    var ACTION_GET_FORUMS_LIST ="getForumAdmin";
    var ACTION_APPROVE_FORUM = "approveForum";
    var ACTION_REJECT_FORUM = "rejectForum";

    var ACTION_GET_COMMENTS_LIST ="getForumCommentAdmin ";
    var ACTION_APPROVE_COMMENT = "approveComment";
    var ACTION_REJECT_COMMENT = "rejectComment";

    var ACTION_CHANGE_USER_RELATION = "changeUserRelation";
    $scope.Math=window.Math;


    $rootScope.page = $routeParams.page;

    $scope.variables = {};

    $scope.variables.type =  $routeParams.type;

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

    $scope.$watch(function(){ return $rootScope.language},function(){
        me.loadAdminPageInfo();
    })

    if($routeParams.part){
        $scope.variables.mode = 1;
        var getParam = "id=" + $routeParams.part;
        httpRequestService.sendAjaxRequest(ACTION_GET_LONG_MAIN_INFO_ALL_LANG,getParam,null,false,function(data){
            if(data.type == 0)
                $scope.variables.selectedPart = data.value;
            else
                $location.path('/admin/mainPage');
        });
    }else{
        $scope.variables.mode =  0;
    }

    $scope.showMessageFullInfo = function(ind){
        var message = $scope.variables.messages[ind];
        var getParam = "id=" + message.id;
        var modalInstance = $modal.open({
            templateUrl:'./views/admin/messagesPopup.html',
            controller: 'MessageModalInstanceCtrl',
            resolve: {
                message: function () {
                    return message;
                }
            }
        });
        httpRequestService.sendAjaxRequest(ACTION_SET_MESSAGE_OLD,getParam,null,false,function(data){
            if(data.type == 0){
                $scope.variables.messages[ind].is_new = 0;
            }
        });
    }

    $scope.editMainPageContent = function(ind){
        $location.path('/admin/mainPage/'+$scope.variables.mainPageParts[ind].id);
    }

    $scope.submitMainPageForm = function(){
        httpRequestService.sendAjaxRequest(ACTION_SET_LONG_MAIN_INFO_ALL_LANG,null, ("data=" + JSON.stringify($scope.variables.selectedPart)),true,function(data){
            if(data.type == 0){
                $location.path('/admin/mainPage');
            }
        });
    }

    $scope.showArticleFullInfo = function(ind){
        var article = $scope.variables.articles[ind];
        var getParam = "id=" + article.id;
        httpRequestService.sendAjaxRequest(ACTION_ARTICLE_FULL_INFO_ALL_LANG,getParam,null,false,function(data){
            if(data.type == 0){
                var modalInstance = $modal.open({
                    templateUrl:'./views/admin/articlePopup.html',
                    controller: 'ArticleModalInstanceCtrl',
                    resolve: {
                        article: function () {
                            return data.value;
                        }
                    }
                });
                modalInstance.result.then(function (obj) {
                    if(!obj.id) return;
                    if(obj.approved)
                        me.approveReject(obj.id,ACTION_APPROVE_ARTICLE)
                    else
                        me.approveReject(obj.id,ACTION_REJECT_ARTICLE)
                });
            }
        });
    }


    $scope.showUserFullInfo = function(ind){
        //var user = $scope.variables.users[ind];
        //var getParam = "id=" + user.user_id;
        var getParam = "id=" + ind;
        httpRequestService.sendAjaxRequest(ACTION_GET_USER_FULL_INFO_ALL_LANG,getParam,null,false,function(data){
            if(data.type == 0){
                var modalInstance = $modal.open({
                    templateUrl:'./views/register.html',
                    controller: 'UserModalInstanceCtrl',
                    windowClass: 'app-modal-large',
                    size: 'lg',
                    resolve: {
                        user: function () {
                            birth_date_obj = data.value.birth_date != 0 ? new Date(data.value.birth_date*1000) : undefined;
                            if (birth_date_obj != undefined) {
                                birth_date_obj.setHours(0, -birth_date_obj.getTimezoneOffset(), 0, 0);
                                data.value.birth_date = birth_date_obj.toISOString().slice(0,10);
                            }
                            else {
                                //data.value.birth_date = '';
                            }

                            dead_date_obj = data.value.dead_date != null ? new Date(data.value.dead_date*1000):undefined;
                            if(dead_date_obj != undefined) {
                                dead_date_obj.setHours(0, -dead_date_obj.getTimezoneOffset(), 0, 0);
                                data.value.dead_date = dead_date_obj.toISOString().slice(0,10);
                            }
                            //data.value.birth_date = data.value.birth_date != null ?new Date(data.value.birth_date*1000):undefined;
                            return data.value;
                        }
                    }
                });
                modalInstance.result.then(function (obj) {
                    if(!obj || !obj.id) return;
                    if(obj.approved)
                        me.approveReject(obj.id,ACTION_APPROVE_USER)
                    else
                        me.approveReject(obj.id,ACTION_REJECT_USER)
                });

            }
        });
    }

    $scope.showForumFullInfo = function(ind) {
        var forum = $scope.variables.forums[ind];
        var modalInstance = $modal.open({
            templateUrl: './views/admin/forumPopup.html',
            controller: 'ForumModalInstanceCtrl',
            resolve: {
                forum: function () {
                    return forum;
                }
            }
        });
        modalInstance.result.then(function (obj) {
            if (!obj.id) return;
            if (obj.approved)
                me.approveReject(obj.id, ACTION_APPROVE_FORUM)
            else
                me.approveReject(obj.id, ACTION_REJECT_FORUM)
        });
    }

    $scope.showCommentFullInfo = function(ind){
        var comment = $scope.variables.comments[ind];
        var modalInstance = $modal.open({
            templateUrl: './views/admin/commentPopup.html',
            controller: 'CommentModalInstanceCtrl',
            resolve: {
                comment: function () {
                    return comment;
                }
            }
        });
        modalInstance.result.then(function (obj) {
            if (!obj.id) return;
            if (obj.approved)
                me.approveReject(obj.id, ACTION_APPROVE_COMMENT)
            else
                me.approveReject(obj.id, ACTION_REJECT_COMMENT)
        });
    }

    $scope.approveArticle = function(ind,event){
        event.stopPropagation();
        var article = $scope.variables.articles[ind];
        me.approveReject(article.id,ACTION_APPROVE_ARTICLE);
    }

    $scope.rejectArticle = function(ind,event){
        event.stopPropagation();
        var article = $scope.variables.articles[ind];
        me.approveReject(article.id,ACTION_REJECT_ARTICLE);
    }

    $scope.approveUser = function(ind,event){
        event.stopPropagation();
        var user = $scope.variables.users[ind];
        //me.approveReject(user.user_id,ACTION_APPROVE_USER);
        me.approveReject(ind,ACTION_APPROVE_USER);
    }

    $scope.rejectUser = function(ind,event){
        event.stopPropagation();
        var user = $scope.variables.users[ind];
        //me.approveReject(user.user_id,ACTION_REJECT_USER);
        me.approveReject(ind,ACTION_REJECT_USER);
    }

    $scope.approveForum = function(ind,event){
        event.stopPropagation();
        var forum = $scope.variables.forums[ind];
        me.approveReject(forum.id,ACTION_APPROVE_FORUM);
    }

    $scope.rejectForum = function(ind,event){
        event.stopPropagation();
        var forum = $scope.variables.forums[ind];
        me.approveReject(forum.id,ACTION_REJECT_FORUM);
    }

    $scope.approveComment = function(ind,event){
        event.stopPropagation();
        var comment = $scope.variables.comments[ind];
        me.approveReject(comment.id,ACTION_APPROVE_COMMENT);
    }

    $scope.rejectComment = function(ind,event){
        event.stopPropagation();
        var comment = $scope.variables.comments[ind];
        me.approveReject(comment.id,ACTION_REJECT_COMMENT);
    }

    this.getNotifications = function(){
        httpRequestService.sendAjaxRequest(ACTION_GET_FORUM_NOTIFICATION_COUNT,null,null,false,function(data){
            $scope.variables.forumNotificationCount = data.value[0].count;
        });

        httpRequestService.sendAjaxRequest(ACTION_GET_FORUM_COMMENT_NOTIFICATION_COUNT,null,null,false,function(data){
            $scope.variables.forumCommentNotificationCount = data.value[0].count;
        });

        httpRequestService.sendAjaxRequest(ACTION_GET_USERS_WAITING_APPROVE_COUNT,null,null,false,function(data){
            $scope.variables.usersWaitingApproveCount = data.value;
        });

        httpRequestService.sendAjaxRequest(ACTION_GET_NEW_MESSAGES_COUNT,null,null,false,function(data){
            $scope.variables.newMessagesCount = data.value;
        });

        httpRequestService.sendAjaxRequest(ACTION_GET_NEW_ARTICLE_COUNT,null,null,false,function(data){
            $scope.variables.newArticlesCount = data.value;
        });
        httpRequestService.sendAjaxRequest(ACTION_GET_VISIT_COUNT,null,null,false,function(data){
            $scope.variables.visitCount = data.value;
        });
    }

    this.loadTableData = function(){
        switch ($routeParams.type){
            case 'users':
                httpRequestService.sendAjaxRequest(ACTION_GET_USERS_SHORT_LIST,null,null,false,function(data){
                    $scope.variables.users = data.value;

                    $scope.totalItems = $scope.variables.users.length;
                    $scope.currentPage = 1;
                    $scope.itemsPerPage = 20;
                    $scope.numPerPage = 20;
                    $scope.maxSize = 3;
                    $scope.changeSort = function (item) {
                        $scope.reverse = $scope.reverse =! $scope.reverse;
                        $scope.sort = item;
                    };
                    //$scope.totalPages = Math.ceil($scope.totalItems/$scope.itemsPerPage);
                });
              $scope.currLang = $rootScope.language;

                break;
            case 'articles':
                httpRequestService.sendAjaxRequest(ACTION_GET_ARTICLES_SHORT_LIST,null,null,false,function(data){
                    $scope.variables.articles = data.value;
                });
                break;
            case 'messages':
                httpRequestService.sendAjaxRequest(ACTION_GET_NEW_MESSAGES,null,null,false,function(data){
                    $scope.variables.messages = data.value;
                });
                break;
            case 'forum':
                httpRequestService.sendAjaxRequest(ACTION_GET_FORUMS_LIST,null,null,false,function(data){
                    $scope.variables.forums = data.value;
                });
                httpRequestService.sendAjaxRequest(ACTION_GET_COMMENTS_LIST,null,null,false,function(data){
                    $scope.variables.comments = data.value;
                });
                break;
            case 'mainPage':
                httpRequestService.sendAjaxRequest(ACTION_GET_SHORT_MAIN_INFO,null,null,false,function(data){
                    $scope.variables.mainPageParts = data.value;
                });
                break;
            case 'geneticTree':
                $scope.variables.gPersonType = 0;
                $scope.variables.relation = {};
                $scope.variables.relation.type  ="father";
                $scope.variables.user = {};
                $scope.variables.user.sex = 1;
                this.loadCountryList();
                this.loadRelationUsers();
                break;
            default:
                $location.path('/admin/users');
                break;
        }
    }

    this.loadAdminPageInfo = function(){
        this.getNotifications();
        this.loadTableData();
    }

    this.approveReject = function(id,action){
        var getParam = "id=" + id;
        if (action == 'rejectUser'){
            var modalInstance = $modal.open({
                templateUrl:'./views/mypage/confirmPopup.html',
                controller: 'UserRejectConfirmModalInstanceCtrl',
                windowClass: 'app-modal-small',
                resolve: {
                    action: function () {
                        return action;
                    },
                    userId: function () {
                        return getParam;
                    }

                }
            });
            modalInstance.result.then(function (reload) {
                if(!reload) return;
                me.loadAdminPageInfo();
            });
        }
        else {
            httpRequestService.sendAjaxRequest(action,getParam,null,false,function(data){
                if(data.type == 0){
                    me.loadAdminPageInfo();
                }
            });
        }
    }

    $scope.setFile = function(el){
        $scope.$apply(function($scope) {
            $scope.variables.fileObject = el.files[0];
        });
    }
    $scope.variables.errors = [];
    $scope.submitUserForm = function(){
        if($scope.variables.gPersonType == 0){
            var formData = new FormData();
            $scope.variables.user.dead_date = null;
            //$scope.variables.user.birth_date = $scope.variables.user.birth_date?$scope.variables.user.birth_date.getTime()/1000:null;
			if (angular.isUndefined($scope.variables.user.country)) {
				$scope.variables.user.country = 0;
			}
            formData.append("data", JSON.stringify($scope.variables.user));
            if($scope.variables.fileObject && typeof $scope.variables.fileObject.name !== 'undefined'){
                formData.append('profile_picture', $scope.variables.fileObject);
            }

            httpRequestService.sendAjaxRequest(ACTION_CREATE_NEW_USER,null,formData,true,function(data){
                $scope.variables.errors = [];
                if(data.type == 0){
                   // $scope.variables.user.birth_date = $scope.variables.user.birth_date != 0 ?new Date($scope.variables.user.birth_date *1000):undefined;
                    $scope.variables.relation.user_id = data.value;
                    $scope.changeRelationType();
                }
                else if (data.type == 1 ){
                    $.each(data.error, function (i, v){
                        $scope.variables.errors.push(v);
                    });

                }
            });
        }else {
            $scope.changeRelationType();
        }
    }

    /*this.loadCountryList = function(){
        httpRequestService.sendAjaxRequest(ACTION_GET_COUNTRIES_WITH_ALL_LANG,null,null,false,function(data){
            if(data.type == 0){
                $scope.variables.coutries = data.value;
            }
        });
    }*/


    this.loadRelationUsers = function(){
        httpRequestService.sendAjaxRequest(ACTION_GET_USERS_LIST_FOR_TREE,null,null,false,function(data){
            if(data.type == 0){
                $scope.variables.relationUsers = [];
                for (var i in data.value){
                    $scope.variables.relationUsers.push({id:data.value[i].id,name:data.value[i].first_name + " " + data.value[i].last_name});
                }
            }
        });
    }

    $scope.changeRelationType = function() {
        var getParam = "user_id=" + $scope.variables.relation.user_id + "&rel_user_id=" + $scope.variables.relation.rel_user_id + "&type=" + $scope.variables.relation.type;
        httpRequestService.sendAjaxRequest(ACTION_CHANGE_USER_RELATION, getParam, null, false, function (data) {
            if (data.type == 0) {
                $scope.variables.user = {};
                $scope.variables.user.sex = 1;
                $scope.variables.relation = {};
                $scope.variables.relation.type = "father";
            }
        });
    }

    this.loadCountryList = function(){
        httpRequestService.sendAjaxRequest(ACTION_GET_COUNTRIES_WITH_ALL_LANG,null,null,false,function(data){
            if(data.type == 0){
                $scope.variables.coutries = data.value;
            }
        });
    }
    this.loadCountryList();
});
familyApp.filter('pagination', function() {
    return function(input, currentPage, pageSize) {
        if(angular.isArray(input)) {
            var start = (currentPage-1)*pageSize;
            var end = currentPage*pageSize;
            return input.slice(start, end);
        }
    };
});