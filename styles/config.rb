# Compass plugins
require 'sass-globbing'

http_path = "/"
css_dir = "css"
sass_dir = "custom_scss"
images_dir = "images"
javascripts_dir = "js"
relative_assets = true
output_style = :compact
line_comments = false
