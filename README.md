# README #

The web site dedicated to Zargaryans family.

### What is this repository for? ###

* Provides family tree of Zargaryans
* Version 1.0
* DEV site - http://dev.zargaryan.com.findarmenian.com/
* STAGE site - http://stage.zargaryan.com.findarmenian.com/
* Live site - http://www.zargaryan.com

### Mockups ###

[Confirmed mockups](http://stage.zargaryan.com.findarmenian.com/_data_/confirmed_designs/)

### Local setup ###

* Clone the repo
* Checkout to **dev** branch
* Create PHP settings file: api/ConfigLocal.php. See below
  
```
#!php

<?php
/*
 * Sets environment dependent settings
 */
Config::setDBhost('localhost');
Config::setDBusername('zargaryan');
Config::setDBpassword('zargaryan123');
Config::setDBdatabaseName('zargaryan_1');
Config::setBaseURL('http://local.zargaryan/');
Config::setNodejsHost('http://zargaryan.herokuapp.com:80/');
Config::setNodejsKey('myservicekey');

```

* Download and export to your local the latest database from downloads section [here](https://bitbucket.org/lananikyan/zargaryan/downloads)

## Initial database dump is in Downloads section. ##
### Database structure deployments is not available. Should be discussed. ###