<?php
require_once 'api/Config.php';
require_once 'api/ConfigLocal.php';
?>
<!DOCTYPE html>
<html ng-app="familyApp">
<head >
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <title>Genetic</title>

    <!---- stylesheets -->


    <base href="/" />

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,700,700i,800&amp;subset=cyrillic" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Oswald:400,600&amp;subset=cyrillic" rel="stylesheet">
    <link rel="stylesheet" href="styles/css/ng-animation.css">
    <link rel="stylesheet" href="scripts/vendor/angular-smilies/dist/angular-smilies-embed.css">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <link rel="stylesheet" href="styles/css/style.css">
    <link rel="stylesheet" href="styles/css/loader.css">
    <link rel="stylesheet" href="styles/css/familyTree.css">
    <link rel="stylesheet" href="styles/css/chosen.min.css">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="styles/css/textAngular.css">
    <link rel="stylesheet" href="styles/css/slideout.css">
    <!--favicon-->
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">
    
    
    <!-- js files -->
    <script src="scripts/vendor/jquery-2.0.3.min.js"></script>
    <script src="scripts/vendor/angular.js"></script>
    <script src="scripts/vendor/angular-route.min.js"></script>
    <script src="scripts/vendor/angular-translate.min.js"></script>
    <script src="scripts/vendor/angular-translate-loader-static-files.min.js"></script>
    <script src="https://use.fontawesome.com/cbd4653301.js"></script>
    <!-- carousel-->
    <!--<script src="https://code.angularjs.org/1.3.15/angular-touch.min.js"></script>-->
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular-touch.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.0-rc.2/angular-animate.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.10.3/TweenMax.min.js"></script>

    <script src="scripts/vendor/ui-bootstrap.min.js"></script>
    <script src="scripts/vendor/ui-bootstrap-tpls.min.js"></script>
    <script src="scripts/vendor/bootstrap.min.js"></script>
    <script src="scripts/vendor/textAngular-sanitize.js"></script>
    <script src="scripts/vendor/angular-smilies/dist/angular-smilies.js"></script>
    <script src="scripts/vendor/chosen.jquery.min.js"></script>
    <script src="scripts/vendor/angular-chosen.min.js"></script>

    <script type="text/javascript" src="scripts/vendor/socket.io.js"></script>

    <script src="scripts/vendor/scrollglue.js"></script>
    <script src="scripts/vendor/angular-filter.min.js"></script>
    <script src="scripts/vendor/angular-input-match.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/rangy/1.3.0/rangy-core.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/rangy/1.3.0/rangy-selectionsaverestore.js"></script>
    <script src="scripts/vendor/textAngular.js"></script>
    <script src="scripts/vendor/textAngularSetup.js"></script>
    <script src="scripts/vendor/underscore.js"></script>
    <script src="scripts/vendor/angular.tree.js"></script>
    <script src="scripts/vendor/angular-cookies.js"></script>
    <script src="//twemoji.maxcdn.com/2/twemoji.min.js?2.2.3"></script>

    <script>
        nodejsHost = "<?php print Config::getNodejsHost(); ?>";
    </script>
    <script src="scripts/app.js"></script>
    <script src="scripts/controllers/mainPage.js"></script>
    <script src="scripts/controllers/passwordChange.js"></script>
    <script src="scripts/controllers/contactUs.js"></script>
    <script src="scripts/controllers/entertainingBook/entertainingBook.js"></script>
    <script src="scripts/controllers/familyTree/familyTree.js"></script>
    <script src="scripts/controllers/familyTree/modal.js"></script>
    <script src="scripts/controllers/guessBook/guessBook.js"></script>
    <script src="scripts/controllers/guessBook/modal.js"></script>
    <script src="scripts/controllers/chat/chat.js"></script>
    <script src="scripts/controllers/chat/modal.js"></script>

    <script src="scripts/controllers/mypage/myPage.js"></script>
    <script src="scripts/controllers/mypage/modal.js"></script>
    <script src="scripts/controllers/admin/admin.js"></script>
    <script src="scripts/controllers/translate.js"></script>
    <script src="scripts/controllers/login.js"></script>
    <script src="scripts/controllers/register.js"></script>
    <script src="scripts/controllers/admin/modal.js"></script>
    <script src="scripts/controllers/entertainingBook/modal.js"></script>
    <script src="scripts/controllers/fixtop.js"></script>

    <script src="scripts/services/httpRequestService.js"></script>
    <script src="scripts/services/rememberMeService.js"></script>
    <script src="scripts/services/socketService.js"></script>
    <script src="scripts/services/checkRequiredService.js"></script>
    <script src="scripts/directives/iframe.js"></script>
    <script src="scripts/directives/sglclick.js"></script>
    <script src="scripts/directives/on_top_scroll.js"></script>
    <script src="scripts/directives/prevent_enter.js"></script>
    <script src="scripts/directives/resize.js"></script>
    <script src="scripts/directives/file_thumbnail.js"></script>
    <script src="scripts/directives/ios_emoji.js"></script>
    
    
    <!--video-->
    <link href="//vjs.zencdn.net/5.8/video-js.min.css" rel="stylesheet">
    <script src="//vjs.zencdn.net/5.8/video.min.js"></script>
    <script src="https://content.jwplatform.com/libraries/RgSgfNbD.js"> </script>

    <script src="scripts/vendor/moment.js"></script>
    <script src="scripts/vendor/moment-timezone-with-data-2010-2020.js"></script>
    <script src="scripts/vendor/slideout.js"></script>

    <script src="https://use.fontawesome.com/7d7a482c46.js"></script>


    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-73056975-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body class="{{language}}" resize>
<div class="page">
    <div class="header">
        <!--New design-->
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 logo-main sa">
                    <img src="img/family_logo.png" class="logo" ng-click="changeTab('mainPage')">
                </div>
                <div class="col-xs-6">
                    <div class="row author visible-xs">
                        <label>{{'website_founders_title' | translate}}:</label>
                        <div ng-if="currentUser">
                            <a href="/profile/599">{{'website_founder1' | translate}}</a>
                            <a href="/profile/557">{{'website_founder2' | translate}}</a>
                        </div>
                        <div ng-if="!currentUser">
                            <span>{{'website_founder1' | translate}}</span>
                            <span>{{'website_founder2' | translate}}</span>
                        </div>

                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8 middle">
                    <div class="row">
                        <div class="secondary-items">
                            <div class="inline-items">
                                <i class="fa fa-globe" aria-hidden="true"></i>
                                <select id="languageBar"  ng-controller="TranslateController" ng-model="language" ng-change="changeLanguage(this.language)">
                                    <option ng-class="{active: language == 'am'}" value="am"><span>Հայերեն</span></option>
                                    <option ng-class="{active: language == 'ru'}" value="ru"><span>Русский</span></option>
                                    <option ng-class="{active: language == 'en'}" value="en"><span>English</span></option>
                                </select>
                            </div>
                            <div class="inline-items hidden-xs">
                                <div ng-class="{active: page == 'contactUs'}" ><i class="fa fa-envelope" aria-hidden="true"></i><a ng-click="changeTab('contactUs')" >{{ 'menu_contact_us' | translate }}</a></div>
                            </div>
                            <div class="inline-items no-border">
                                <ul class="loginBar">
                                    <li ng-if="!currentUser" ng-class="{active: page == 'login'}"><a href="/login"><i class="fa fa-user" aria-hidden="true"></i>{{ 'menu_login' | translate}}</a> </li>
                                    <li ng-if="currentUser" ng-controller="LoginController"><a class="visible-lg btn btn-primary" href="#" ng-click="logoutUser()" >{{ 'menu_logout' | translate}}</a></li>
                                    <li ng-if="currentUser" ng-controller="LoginController"><a class="visible-md" href="#" ng-click="logoutUser()" title="{{ 'menu_logout' | translate}}"><i class="fa fa-sign-out" aria-hidden="true"></i></a></li>
                                    <li ng-if="!currentUser" ng-class="{active: page == 'register'}"><a class="btn btn-primary visible-lg visible-md register-lg" href="/register"  >{{ 'menu_register' | translate}}</a></li>
                                    <li ng-if="currentUser" ng-class="{active: page == 'register'}">{{ currentUser.first_name}} {{ currentUser.last_name}}</li>
                                </ul>
                            </div>
                            <!--<div ng-if="currentUser" class="inline-items no-border user-name">
                                <ul class="loginBar">
                                    <li>{{ currentUser.first_name}} {{ currentUser.last_name}}</li>
                                </ul>
                            </div>-->
                        </div>

                    </div>
                    <div class="row author hidden-xs">
                        {{'website_founders_title' | translate}}:
                        <div ng-if="currentUser">
                            <a href="/profile/559">{{'website_founder1' | translate}}</a>
                            <a href="/profile/557">{{'website_founder2' | translate}}</a>
                        </div>
                        <div ng-if="!currentUser">
                            <span>{{'website_founder1' | translate}}</span>
                            <span>{{'website_founder2' | translate}}</span>
                        </div>

                    </div>
                </div>
                <div class="col-lg-4 col-md-4 visible-lg visible-md">
                    <div class="hero-layout">
                        <img ng-src="img/hero/{{heroIndex}}.png" class="">
                    </div>
                </div>
                <div class="col-sm-2 hidden-lg visible-sm duplicate-regBtn">
                    <div class="inline-items">
                        <ul class="loginBar">
                            <li ng-if="currentUser" ng-controller="LoginController"><a href="#" class="btn btn-primary" ng-click="logoutUser()" >{{ 'menu_logout' | translate}}</a></li>
                            <li ng-if="!currentUser" ng-class="{active: page == 'register'}"><a class="btn btn-primary register-tablet"  href="/register"  >{{ 'menu_register' | translate}}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <nav class="navbar" set-class-when-at-top="fix-to-top">
            <div class="container">
                <div class="logo-mainmenu">
                    <img src="img/family_logo.png" class="logo" ng-click="changeTab('mainPage')">
                </div>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" ng-if="!currentUser" data-toggle="collapse" data-target="#mainMenu">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <button type="button" class="navbar-toggle collapsed" ng-if="currentUser" data-toggle="collapse" data-target="#loginMainMenu">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>


                <div class="navigationBar" ng-if="currentUser">
                    <div class="collapse navbar-collapse" id="loginMainMenu">
                        <ul class="nav navbar-nav nav-tabs">
                            <li ng-class="{active: page == 'mainPage'}" ><a ng-click="changeTab('mainPage')">{{ 'menu_home' | translate }}</a></li>
                            <li ng-class="{active: page == 'familyTree'}"><a ng-click="changeTab('familyTree')" >{{ 'menu_family_tree' | translate }}</a></li>
                            <li ng-class="{active: page == 'guessBook'}"><a ng-click="changeTab('guessBook')">{{ 'menu_guest_book' | translate }}</a></li>
                            <li ng-class="{active: page == 'chat'}"><span id="msg-count"  ng-if="currentUser.msg_unseen > 0 && page != 'chat'">{{currentUser.msg_unseen}}</span><a ng-click="changeTab('chat')">{{ 'chat' | translate }}</a></li>
                            <li ng-class="{active: page == 'entertainingBook'}"><a ng-click="changeTab('entertainingBook')" > {{ 'menu_entertaining_book' | translate }}</a></li>
                            <li ng-class="{active: page == 'contactUs'}" class="visible-xs"><a ng-click="changeTab('contactUs')" >{{ 'menu_contact_us' | translate }}</a></li>
                            <li ng-class="{active: page == 'myPage'}"><a  ng-click="changeTab('profile')" >{{ 'menu_my_page' | translate }}</a></li>
                            <li ng-class="{active: page == 'admin'}" ng-if="currentUser.role == 1"><a  ng-click="changeTab('admin/users')">{{ 'menu_admin_page' | translate }}</a></li>
                            <li ng-controller="LoginController" class="visible-xs"><a href="#" ng-click="logoutUser()" >{{ 'menu_logout' | translate}}</a></li>
                        </ul>
                    </div>
                </div>


                <div class="navigationBar" ng-if="!currentUser" ng-controller="MainPageController">
                    <div class="collapse navbar-collapse" id="mainMenu">
                        <ul class="nav navbar-nav nav-tabs ">
                            <li ng-class="{active: page == 'mainPage' && tab == null}" >
                                <a ng-click="changeTab('mainPage')">{{ 'menu_home' | translate }}</a>
                            </li>
                            <li ng-repeat="part in variables.parts" ng-class="{active: page == 'mainPage' && tab == part.id}">
                                <a ng-click="changeTab('mainPage/' +part.id)">{{part.title}}</a>
                            </li>
                            <li ng-class="{active: page == 'contactUs'}" class="visible-xs"><a ng-click="changeTab('contactUs')">{{ 'menu_contact_us' | translate }}</a></li>

                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>

    <div class="content" >
        <div ng-view >

        </div>
    </div>

    <div class="footer">
        <div class="socialContent">
            <div class="container">
                <ul class="socialIcons">
                    <li>
                        <a href="https://facebook.com/" target="_blank">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://twitter.com/" target="_blank">
                            <i class="fa fa-twitter" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li>
                        <a href="http://vk.com/" target="_blank" >
                            <i class="fa fa-vk" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li>
                        <a href="http://ok.ru/" target="_blank" >
                            <i class="fa fa-odnoklassniki" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/" target="_blank">
                            <i class="fa fa-instagram" aria-hidden="true"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="footer-menu">
            <!--footer menu-->
            <nav class="navbar">
                <div class="container">

                    <div class="navigationBar row-fluid" ng-if="currentUser">
                        <div class="" id="footerLoginMainMenu">
                            <ul class="nav navbar-nav nav-tabs">
                                <li ng-class="{active: page == 'mainPage'}" ><a ng-click="changeTab('mainPage')">{{ 'menu_home' | translate }}</a></li>
                                <li ng-class="{active: page == 'familyTree'}"><a ng-click="changeTab('familyTree')" >{{ 'menu_family_tree' | translate }}</a></li>
                                <li ng-class="{active: page == 'guessBook'}"><a ng-click="changeTab('guessBook')">{{ 'menu_guest_book' | translate }}</a></li>
                                <li ng-class="{active: page == 'chat'}"><a ng-click="changeTab('chat')">{{ 'chat' | translate }}</a></li>
                                <li ng-class="{active: page == 'entertainingBook'}"><a ng-click="changeTab('entertainingBook')" > {{ 'menu_entertaining_book' | translate }}</a></li>
                                <li ng-class="{active: page == 'contactUs'}" ><a ng-click="changeTab('contactUs')" >{{ 'menu_contact_us' | translate }}</a></li>
                                <li ng-class="{active: page == 'myPage'}"><a  ng-click="changeTab('profile')" >{{ 'menu_my_page' | translate }}</a></li>
                                <li ng-class="{active: page == 'admin'}" ng-if="currentUser.role == 1"><a  ng-click="changeTab('admin/users')">{{ 'menu_admin_page' | translate }}</a></li>
                                <li ng-controller="LoginController"><a href="#" ng-click="logoutUser()" >{{ 'menu_logout' | translate}}</a></li>
                            </ul>
                        </div>
                    </div>


                    <div class="navigationBar" ng-if="!currentUser" ng-controller="MainPageController">
                        <div class="" id="footerLmainMenu">
                            <ul class="nav navbar-nav nav-tabs ">
                                <li ng-class="{active: page == 'mainPage' && tab == null}" >
                                    <a ng-click="changeTab('mainPage')">{{ 'menu_home' | translate }}</a>
                                </li>
                                <li ng-repeat="part in variables.parts" ng-class="{active: page == 'mainPage' && tab== part.id}">
                                    <a ng-click="changeTab('mainPage/' +part.id)">{{part.title}}</a>
                                </li>
                                <li ng-class="{active: page == 'login'}"><a href="/login">{{ 'menu_login' | translate}}</a> </li>
                                <li ng-class="{active: page == 'register'}"><a href="/register"  >{{ 'menu_register' | translate}}</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
            <div class="container copyright">
                <div>
                    <p> © 2017   Zargaryan.com.  All Rights Reserved</p>
                </div>
            </div>
        </div>
    </div>

</div>
</body>
</html>